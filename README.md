# Serveur - CreaTIKZ - Groupe 4

## Configuration
Les librairies nécessaires se trouvent dans le dossier **Creatikz_lib/** présent à la racine du projet. 

Les informations de connexion au serveur Creatikz peuvent être modifiées dans le fichier
de configuration **properties.ini** (présent à la racine du projet).

## Compilation
La compilation se fait à l'aide de l'outil **ant**.
#### Compilation simple
Executer la commande suivante à la racine du projet: 
 `# ant `
### Creation du jar
Executer la commande suivante à la racine du projet: 
 `# ant create_run_jar `
### Execution
Après avoir créé le jar, lancez le à l'aide de la commande: 
 `# java -jar Creatikz.jar`

Il faudra toutefois un serveur CreaTIKZ 
(https://gitlab.com/CreaTIKZ-groupe4/CreaTIKZ-Server) fonctionnel pour accèder à 
toutes les fonctionnalités de la plateforme.

package main;

import controller.LoginController;
import controller.creatikzwindow.CreaTikzWindowController;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import util.Config;
import util.CustomUtil;

public class Main extends Application {

	/**
	 * Launch the login window if the server is accessible, start offline mode otherwise
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {

		try {
			CustomUtil.createDefaultRep();
			if (!Config.offlineMode()) {
				LoginController.setUp();
			} else {
				CreaTikzWindowController.setupTikz();
			}
		} catch (Exception e) {
			new Alert(AlertType.ERROR, "Erreur au lancement du programme").show();
		}

	}

	public static void main(String... args) {
		launch(args);
	}

}
package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.services.Server;
import model.services.User;
import util.CustomUtil;

public class EditController extends Stage {

	@FXML
	private TextField txtUsernameEdit;
	@FXML
	private TextField txtNomEdit;
	@FXML
	private TextField txtPrenomEdit;
	@FXML
	private TextField txtMailEdit;
	@FXML
	private PasswordField txtCurrentPasswordEdit;
	@FXML
	private PasswordField txtPasswordEdit;
	@FXML
	private PasswordField txtConfPasswordEdit;
	@FXML
	private Button btnEdit;
	@FXML
	private Button btnCancel;
	@FXML
	private Label lblMessageEdit;

	/**
	 * Initialize edit dialog
	 * @throws IOException
	 */
	public static void setUp() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLLoader.getDefaultClassLoader().getResource("view/edit.fxml"));
		try {
			GridPane root = loader.load();
			EditController edit = loader.getController();
			edit.setScene(new Scene(root));
			edit.show();
		} catch (IOException ex) {
			new Alert(AlertType.ERROR, "Erreur au chargement de la fenêtre").show();
		}
	}

	/**
	 * Set up this window's controller
	 */
	@FXML
	private void initialize() {
		this.setTitle("Edit profile");
		btnEdit.setOnAction(e -> edit());
		btnCancel.setOnAction(e -> this.close());
		setFields();
	}

	/**
	 * Set up this window's field with the active user's information
	 */
	private void setFields() {
		txtUsernameEdit.setText(Server.getActiveUser().getUsername());
		txtMailEdit.setText(Server.getActiveUser().getEmail());
		txtNomEdit.setText(Server.getActiveUser().getLast_name());
		txtPrenomEdit.setText(Server.getActiveUser().getFirst_name());
	}

	/**
	 * Saves the modified user information on the server
	 */
	private void edit() {

		if (!txtPasswordEdit.getText().isEmpty() && txtPasswordEdit.getText().equals(txtConfPasswordEdit.getText())) {
			errorMessage("Mots de passes pas identiques");
			return;
		}

		String username = txtUsernameEdit.getText();
		String mail = txtMailEdit.getText();
		String nom = txtNomEdit.getText();
		String prenom = txtPrenomEdit.getText();
		String mdp = txtCurrentPasswordEdit.getText();
		String newPwd = txtPasswordEdit.getText();
		
		newPwd = newPwd.isEmpty() ? mdp : newPwd;

		if (mdp.isEmpty()) {
			errorMessage("Veuillez entrer votre mdp");
			return;
		}
		if (!CustomUtil.isValidEmail(mail)) {
			errorMessage("Adresse mail invalide");
			return;
		}

		User updateUser = new User(username, prenom, nom, mail, mdp);
		updateUser.setId(Server.getActiveUser().getId());

		if (prenom.isEmpty()) {
			updateUser.setFirst_name(Server.getActiveUser().getFirst_name());
		}
		if (nom.isEmpty()) {
			updateUser.setLast_name(Server.getActiveUser().getLast_name());
		}

		updateUser.setPassword(mdp);
		try { 
			Server.update(updateUser, newPwd);
		 	errorMessage("Compte modifié");
		} catch(Exception ex) {
			errorMessage("Echec à la mise à jour" + System.lineSeparator() + ex.getCause());
		}
	}

	/**
	 * Sets this window's error message and displays an alert dialog
	 * @param message the message to be displayed
	 */
	public void errorMessage(String message) {
		lblMessageEdit.setText(message);
		new Alert(AlertType.INFORMATION, message).show();
	}
}

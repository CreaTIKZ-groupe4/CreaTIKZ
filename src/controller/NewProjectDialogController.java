package controller;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;

import javax.ws.rs.ProcessingException;

/**
 * Created by J on 26/03/2016.
 */

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Project;
import util.CustomUtil;
import util.StringUtils;

public class NewProjectDialogController extends Dialog {

	@FXML
	private TextField projectName;
	@FXML
	private TextField projectLocation;
	@FXML
	private Button changeRep;

	private String rootRep;

	private Stage primaryStage;
	private Project project;

	/**
	 * Set up a new project dialog which allows the user to enter a new project name.
	 * @return the created ProjectDialog's controller
	 * @throws IOException
	 */
	public static NewProjectDialogController setUp() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLLoader.getDefaultClassLoader().getResource("view/newProjectDialog.fxml"));
		GridPane pane = loader.load();
		NewProjectDialogController projectDialogController = loader.getController();
		projectDialogController.setContent(pane);
		projectDialogController.setTitle("Create new project");
		return projectDialogController;
	}

	/**
	 * Set up this controller's action event handlers
	 */
	@FXML
	private void initialize() {
		getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
		getDialogPane().getButtonTypes().add(ButtonType.OK);
		Button okButton = (Button) getDialogPane().lookupButton(ButtonType.OK);
		okButton.setDisable(true);

		rootRep = CustomUtil.getWorkingDirPath();

		projectLocation.setText(rootRep);

		projectName.textProperty().addListener((observable, oldValue, newValue) -> {
			okButton.setDisable(StringUtils.isNullOrEmpty(newValue));
		});

		changeRep.setOnAction(event -> {
			getProjectDirectory();
		});

		okButton.setOnAction(event -> {
			createProject();
		});

	}

	/**
	 * Prompt the user for a project location
	 */
	private void getProjectDirectory() {
		File selectedDirectory = CustomUtil.selectDirectory(rootRep, primaryStage);
		if (selectedDirectory != null) {
			try {
				projectLocation.setText(selectedDirectory.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Create a new project using the entered project name
	 */
	private void createProject() {
		String projName = projectName.getText().trim();
		try {
			this.project = Project.createNewProject(projName);
		} catch (ConnectException | ProcessingException ce) {
			new Alert(AlertType.ERROR, "Erreur lors de la sauvegarde du projet sur le serveur").showAndWait();
			ce.printStackTrace();
		} catch (SecurityException se) {
			se.printStackTrace();
		} catch (IOException e) {
			new Alert(AlertType.ERROR, "Erreur lors de la sauvegarde du projet").show();

			e.printStackTrace();
		} catch (Exception e) {
			new Alert(AlertType.ERROR, "Erreur lors de la création du projet:\n" + e.getMessage()).show();
			e.printStackTrace();
		}
	}

	/**
	 * Set this dialog's content
	 * @param gridPane
	 */
	public void setContent(GridPane gridPane) {
		this.getDialogPane().setContent(gridPane);
	}

	/**
	 * @return the dialog's project
	 */
	public Project getProject() {
		return this.project;
	}

}

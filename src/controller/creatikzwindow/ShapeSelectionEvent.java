package controller.creatikzwindow;

import javafx.event.Event;
import javafx.event.EventType;

public class ShapeSelectionEvent extends Event {
	private static final long serialVersionUID = 1L;
	public static EventType<ShapeSelectionEvent> OPTIONS_ALL = new EventType<>("OPTIONS_ALL");
	private int lineNumber;

	public ShapeSelectionEvent(EventType<? extends Event> eventType) {
		super(eventType);
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int newLineNumber) {
		lineNumber = newLineNumber;
	}

}

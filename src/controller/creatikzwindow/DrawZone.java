package controller.creatikzwindow;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import model.DrawOptions;
import model.Model;
import model.shapes.CustomEllipse;
import model.shapes.CustomLine;
import model.shapes.CustomMatrix;
import model.shapes.CustomMindMap;
import model.shapes.CustomNode;
import model.shapes.CustomRectangle;
import model.shapes.CustomShape;
import model.shapes.CustomTriangle;
import model.shapes.DrawingGenerator;
import model.styles.ShapeStyle;
import util.TikzParser;

/**
 * Created by J on 01/03/2016.
 */
public class DrawZone extends Canvas implements Observer {

	private Shape selectedFigure = null;
	private Group figuresGroup;
	private boolean newFigureIsDrawn = false;
	private double startingPointX, startingPointY;
	private CustomShape newFigure;
	private DrawOptions drawOptions;
	private Model tikzModel;
	private double dragStartX, dragStartY;
	private CustomLine drawnLink = null;

	ShapeSelectionEvent selectionEvent = new ShapeSelectionEvent(ShapeSelectionEvent.OPTIONS_ALL);

	/**
	 * Default constructor
	 */
	public DrawZone() {
		super(800, 900);
	}

	/**
	 * Constructs a new drawzone with the specified Group and drawoptions
	 * 
	 * @param groupForFigures
	 *            Group which will contain all drawn shapes
	 * @param drawOptions
	 *            DrawOptions to be applied to drawn shapes
	 */
	public DrawZone(Group groupForFigures, DrawOptions drawOptions) {
		this();
		this.figuresGroup = groupForFigures;
		this.drawOptions = drawOptions;
	}

	/**
	 * Constructs a new drawzone with the specified Group and drawoptions
	 * 
	 * @param groupForFigures
	 *            Group which will contain all drawn shapes
	 * @param drawOptions
	 *            DrawOptions to be applied to drawn shapes
	 * @param figuresModel
	 *            Model to be displayed in this drawZone
	 */
	public DrawZone(Group groupForFigures, DrawOptions drawOptions, Model figuresModel) {
		this(groupForFigures, drawOptions);
		this.tikzModel = figuresModel;
	}

	/**
	 * Initialize this DrawZone
	 */
	public void initialize() {
		this.setOnMousePressed((MouseEvent event) -> {
			this.selectedFigure = null;
			startDrawingFigure(event);
		});

		this.setOnMouseDragged((MouseEvent event) -> {
			if (!newFigureIsDrawn) {
				newFigureIsDrawn = true;
				figuresGroup.getChildren().add(newFigure.getFigure());
			}
			newFigure.setCoordsFromTwoPoints(startingPointX, startingPointY, event.getX(), event.getY());
		});

		this.setOnMouseReleased((MouseEvent event) -> {
			if (newFigureIsDrawn) {
				finishDrawingFigure();
			}
		});

		this.drawOptions.addObserver(this);
		this.setOnDragOver(onDragOverEventHandler);
		this.setOnDragDropped(onDragDroppedEventHandler);
	}

	EventHandler<DragEvent> onDragOverEventHandler = new EventHandler<DragEvent>() {
		@Override
		public void handle(DragEvent event) {
			if (event.getGestureSource() != this && event.getDragboard().hasString()) {
				event.acceptTransferModes(TransferMode.MOVE);
			}
			event.consume();
		}
	};

	EventHandler<DragEvent> onDragDroppedEventHandler = new EventHandler<DragEvent>() {
		@Override
		public void handle(DragEvent event) {

			Dragboard db = event.getDragboard();
			CustomShape newCustomShape = null;
			if (db.hasString()) {
				switch (db.getString()) {
				case "ellipseButton":
					newCustomShape = new CustomEllipse(event.getX(), event.getY(), 60.0, 60.0);
					break;
				case "rectangleButton":
					newCustomShape = new CustomRectangle(event.getX() - 50, event.getY() - 40, event.getX() + 50.0,
							event.getY() + 40.0);
					break;
				case "triangleButton":
					newCustomShape = new CustomTriangle(event.getX(), event.getY() - 30, event.getX() - 40,
							event.getY() + 30, event.getX() + 40, event.getY() + 30);
					break;
				case "lineButton":
					newCustomShape = new CustomLine(event.getX() - 100, event.getY(), event.getX() + 100, event.getY());
					break;
				case "pentagonDragArea":
					DrawingGenerator.addPentagon(event.getX(), event.getY(), tikzModel, true);
					return;
				case "diamondDragArea":
					DrawingGenerator.addDiamond(event.getX(), event.getY(), tikzModel, true);
					return;
				case "cycleDragArea":
					DrawingGenerator.addCycleDrawing(event.getX(), event.getY(), 64, 16, 0, 4, tikzModel, true);
					return;
				case "matrixDragArea":
					DrawingGenerator.addMatrixDrawing(event.getX(), event.getY(), tikzModel, drawOptions);
					return;
				case "treeDragArea":
					DrawingGenerator.addTreeDrawing(event.getX(), event.getY(), tikzModel, drawOptions);
					return;
				case "mindmapDragArea":
					DrawingGenerator.addMindMapDrawing(event.getX(), event.getY(), tikzModel, drawOptions);
					return;
				case "cakeDragArea":
					DrawingGenerator.addCakeDrawing(event.getX(), event.getY(), tikzModel);
					return;
				}
				drawOptions.applyStyleToShape(newCustomShape.getFigure());
				tikzModel.addComponent(newCustomShape);
			}
		}

	};

	/**
	 * Set this DrawZone's DrawOptions
	 * 
	 * @param options
	 */
	public void setOptions(DrawOptions options) {
		this.drawOptions = options;
	}

	/**
	 * Initialise drawing mode
	 * 
	 * @param event
	 *            Starting event of the drawing
	 */
	private void startDrawingFigure(MouseEvent event) {
		startingPointX = event.getX();
		startingPointY = event.getY();
		try {
			Class<? extends CustomShape> custom = drawOptions.getShapeStyle().getCustomClass();
			newFigure = custom.newInstance();

			drawOptions.applyStyleToShape(newFigure.getFigure());
			addListenersToShape(newFigure);
			newFigureIsDrawn = false;
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add all expected listeners to the passed CustomShape
	 * 
	 * @param customShape
	 *            the shape on which listeners will be applied
	 */
	private void addListenersToShape(CustomShape customShape) {
		customShape.getFigure().setOnMouseDragged(event -> shapeOnMouseDraggedEventHandler(event, customShape));

		customShape.getFigure().setOnMouseReleased(event -> shapeOnMouseReleasedHandler(event, customShape));

		customShape.getFigure().setOnMousePressed(event -> shapeOnMousePressedEventHandler(event));

		customShape.getFigure().setOnKeyPressed(event -> shapeOnKeyPressedEventHandler(event, customShape));
	}

	/**
	 * Selects the clicked shape and applies its style to the drawoptions
	 * control panel.\n Initializes shape movement
	 * 
	 * @param event
	 *            MouseEvent
	 */
	public void shapeOnMousePressedEventHandler(MouseEvent t) {
		dragStartX = t.getSceneX();
		dragStartY = t.getSceneY();

		selectedFigure = (Shape) (t.getSource());
		drawOptions.setStyleFromShape((Shape) (t.getSource()));
		selectedFigure.requestFocus();

	}

	/**
	 * Moves the CustomShape to follow the drag event
	 * 
	 * @param event
	 *            Mouse dragged event
	 * @param customShape
	 *            CustomShape to be moved
	 */
	public void shapeOnMouseDraggedEventHandler(MouseEvent event, CustomShape customShape) {
		double offsetX = event.getSceneX() - dragStartX;
		double offsetY = event.getSceneY() - dragStartY;
		dragStartX = event.getSceneX();
		dragStartY = event.getSceneY();
		customShape.translate(offsetX, offsetY);
	}

	/**
	 * Deletes the customShape if the delete key was pressed
	 * 
	 * @param event
	 *            KeyEvent, only DELETE Keycode is handled
	 * @param customShape
	 *            shape to be deleted
	 */
	private void shapeOnKeyPressedEventHandler(KeyEvent event, CustomShape customShape) {
		if (event.getCode() == KeyCode.DELETE) {
			tikzModel.removeComponent(customShape);
			selectedFigure = null;
		}
	}

	/**
	 * Attaches the link the user is drawing to the pressed shape.
	 * 
	 * @param event
	 *            MouseReleased event
	 * @param customShape
	 *            pressed shape
	 */
	public void shapeOnMouseReleasedHandler(MouseEvent event, CustomShape customShape) {
		if (drawOptions.getShapeStyle() == ShapeStyle.Link) {
			String id = customShape.getOrGenerateId();
			if (drawnLink == null) {
				drawnLink = new CustomLine();
				drawnLink.setStartId(id);
			} else {
				drawnLink.setEndId(id);
				drawOptions.applyStyleToShape(drawnLink.getFigure());
				tikzModel.addComponent(drawnLink);
				drawOptions.setShapeStyle(ShapeStyle.None);
				drawnLink = null;
			}
		}
		tikzModel.update();
	}

	/**
	 * Adds the drawnshape to the model and resets the drawing mode
	 */
	private void finishDrawingFigure() {
		tikzModel.addComponent(newFigure);
		newFigure = null;
		newFigureIsDrawn = false;
	}

	/**
	 * Applies the drawoptions to the selected figure and updates the model
	 */
	@Override
	public void update(Observable obs, Object obj) {
		if (this.selectedFigure != null) {
			drawOptions.applyStyleToShape(this.selectedFigure);
			tikzModel.update();
		}
	}

	/**
	 * Draws the shapes of this DrawZone's tikzModel
	 */
	public void drawShapes() {
		int lineIndex = TikzParser.LINE_BEGIN;

		ObservableList<Node> children = figuresGroup.getChildren();
		children.remove(1, children.size());

		selectionEvent.setLineNumber(0);
		for (CustomShape shape : tikzModel.getComponents()) {
			shape.updateFigureCoordinates();
			switch (shape.getClass().getSimpleName()) {
			case "CustomMindMap":
			case "CustomNode":
				CustomNode drawnNode = (CustomNode) shape;
				drawnNode.updateFigureCoordinates();
				List<Shape> figures = drawnNode.getAllFigures();
				children.addAll(figures);
				drawShape(lineIndex, drawnNode);
				lineIndex+=(figures.size()+1)/3; 
				break;

			case "CustomMatrix":
				CustomMatrix drawnMatrix = (CustomMatrix) shape;
				drawnMatrix.updateFigureCoordinates();
				lineIndex++;
				ArrayList<ArrayList<CustomShape>> rows = drawnMatrix.getRows();
				for (int i = 0; i < rows.size(); i++) {
					for (int j = 0; j < rows.get(i).size(); j++) {
						drawShape(lineIndex, children, rows.get(i).get(j));
						lineIndex++;
					}
				}
				lineIndex++;
				break;

			default:
				drawShape(lineIndex, children, shape);

				lineIndex++;
				break;
			}
		}
		this.fireEvent(selectionEvent);
	}

	private void drawShape(int lineIndex, ObservableList<Node> children, CustomShape shape) {
		Shape drawnShape = shape.getFigure();
		checkSelectedShape(lineIndex, drawnShape);
		addShapeToGroup(shape);
		addListenersToShape(shape);
	}
	
	private void drawShape(int lineIndex, CustomShape shape) {
		Shape drawnShape = shape.getFigure();
		checkSelectedShape(lineIndex, drawnShape);
		addListenersToShape(shape);
	}
	
	/**
	 * Verifies if this shape is the selectedFigure, and if it is, applies a
	 * specific style to it\n and marks its line index
	 * 
	 * @param lineIndex
	 *            the index of this shape
	 * @param drawnShape
	 *            the shape to be checked
	 */
	private void checkSelectedShape(int lineIndex, Shape drawnShape) {
		if (selectedFigure != null && drawnShape.toString().equals(selectedFigure.toString())) {
			selectedFigure = drawnShape;
			drawnShape.setStyle("-fx-effect: dropshadow(gaussian, #039ed3, 5, 0.5, 0, 0);");
			selectionEvent.setLineNumber(lineIndex);
		} else {
			drawnShape.setStyle("-fx-effect: none;");
		}
	}

	/**
	 * Add a CustomShape to this drawzone's figuresGroup
	 * 
	 * @param customShape
	 *            CustomShape to be added
	 */
	private void addShapeToGroup(CustomShape customShape) {
		if (customShape.getClass() == CustomLine.class) {
			figuresGroup.getChildren().add(1, customShape.getFigure());
		} else {
			figuresGroup.getChildren().add(customShape.getFigure());
		}

		figuresGroup.getChildren().add(generateShapeLabel(customShape));
	}

	/**
	 * Generates a Text node with this CustomShape's label
	 * 
	 * @param customShape
	 *            CustomShape for which a Text node will be created
	 * @return the generated Text Node
	 */
	private Text generateShapeLabel(CustomShape customShape) {
		Text label = new Text();
		label.setText(customShape.getLabel());
		label.setX(customShape.getAnchorX());
		label.setY(customShape.getAnchorY());
		label.setLayoutX(-label.getLayoutBounds().getWidth() / 2);
		return label;
	}

	/**
	 * Set the model and draw its shapes
	 * 
	 * @param model
	 *            the model to be displayed
	 */
	public void setTikzModel(Model model) {
		this.tikzModel = model;
		drawShapes();
	}

	/**
	 * 
	 * @return the currently focused shape
	 */
	public Shape getSelectedShape() {
		return selectedFigure;
	}

	/**
	 * 
	 * @param shape
	 *            the currently focused shape
	 */
	public void setSelectedShape(Shape shape) {
		selectedFigure = shape;
	}

	/**
	 * 
	 * @return the Group that contains all drawn shapes
	 */
	public Group getFiguresGroup() {
		return figuresGroup;
	}

	/**
	 * 
	 * @param figuresGroup
	 *            the Group that will contain all the drawn shapes
	 */
	public void setFiguresGroup(Group figuresGroup) {
		this.figuresGroup = figuresGroup;
	}

}
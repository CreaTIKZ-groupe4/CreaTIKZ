package controller.creatikzwindow;

import org.fxmisc.richtext.CodeArea;

import controller.TikzSyntaxHighlighting;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Model;
import util.TikzGenerator;
import util.TikzParser;

public class TikzTextPanelController extends Stage {

	private Model tikzModel;

	private CodeArea tikzCodeArea; 

	@FXML
	private Button parseButton;
	@FXML
	private Pane paneArea;
	@FXML
	private Button openTikz;

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		tikzCodeArea = TikzSyntaxHighlighting.createCodeArea();
		tikzCodeArea.setPrefSize(600, 200);
		paneArea.getChildren().add(tikzCodeArea);
		paneArea.getStylesheets().add("/resources/tikz-keywords.css");
		parseButton.setOnAction(event -> parseCode());
	}

	/**
	 * Sets the TikzTextPanel's model and applies its text to the codearea
	 * @param model the Model to be set
	 */
	public void setModel(Model model) {
		this.tikzModel = model;
		setText(TikzGenerator.convertToTikz(model));
	}
	
	/**
	 * Parses the tikzCodeArea's text into the model and updates it
	 */
	public void parseCode() {
		try {
			TikzParser.parse(tikzCodeArea.getText(), tikzModel, false, true);
		} catch (Exception e) {
			e.printStackTrace();
			new Alert(AlertType.ERROR, "Erreur lors du parsage:\n"+ e.getCause()).show();
		}
		tikzModel.update();
	}

	/**
	 * 
	 * @param tikzText the TikzTextPanel's CodeArea text content
	 */
	public void setText(String tikzText) {
		tikzCodeArea.replaceText(tikzText);
	}

	/**
	 * Marks a line in this TikzTextPanel's CodeArea
	 * @param lineNumber the line to be marked
	 */
	public void highlightSelectedLine(int lineNumber) {
		tikzCodeArea.positionCaret(tikzCodeArea.position(lineNumber, 0).toOffset());
	}

}
package controller.creatikzwindow;

import java.util.Observable;
import java.util.Observer;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import model.DrawOptions;
import model.styles.ColorStyle;
import model.styles.LineStyle;
import model.styles.ShapeStyle;
import model.styles.ThicknessStyle;

public class DrawOptionsPanelController implements Observer {

	@FXML
	private GridPane drawOptionsPanel;
	@FXML
	private Button rectangleButton;
	@FXML
	private Button ellipseButton;
	@FXML
	private Button triangleButton;
	@FXML
	private Button lineButton;
	@FXML
	private Button linkButton;

	@FXML
	private Label pentagonDragArea;
	@FXML
	private Label diamondDragArea;
	@FXML
	private Label cycleDragArea;
	@FXML
	private Label matrixDragArea;
	@FXML
	private Label treeDragArea;
	@FXML
	private Label mindmapDragArea;
	@FXML
	private Label cakeDragArea;

	@FXML
	private ComboBox<ColorStyle> fillColor;
	@FXML
	private ComboBox<LineStyle> lineType;
	@FXML
	private ComboBox<ThicknessStyle> lineThickness;
	@FXML
	private ComboBox<ColorStyle> lineColor;
	@FXML
	private CheckBox doFill;
	private Button selectedButton;
	private DrawOptions shapeOptions;

	/**
	 * Set the disable property of the draw options
	 * @param disable True to disable 
	 */
	public void setDisable(boolean disable) {
		drawOptionsPanel.setDisable(disable);
	}

	private EventHandler<MouseEvent> onDragDetectedEventHandler = event -> {
		Node source = ((Node) event.getSource());
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		ClipboardContent content = new ClipboardContent();
		content.putString(source.getId());
		db.setContent(content);

		event.consume();
	};

	/**
	 * Initialize this drawoptions panel
	 */
	@FXML
	private void initialize() {
		fillColor.getItems().setAll(ColorStyle.values());
		lineColor.getItems().setAll(ColorStyle.values());
		lineType.getItems().setAll(LineStyle.values());
		lineThickness.getItems().setAll(ThicknessStyle.values());
		bindBoxes();
		addShapeButtonsEventHandlers();
		setImagesOnButton();
	}

	/**
	 * Bind actions to this option panel's comboboxes
	 */
	private void bindBoxes() {

		// Modifie la couleur de remplissage
		fillColor.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			shapeOptions.setFillColorStyle(newValue);
		});

		// Modifie la couleur de trait
		lineColor.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			shapeOptions.setLineColorStyle(newValue);
		});

		// Modifie le type de trait
		lineType.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			shapeOptions.setLineStyle(newValue);
		});

		// Met à jour la largeur de trait
		lineThickness.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			shapeOptions.setThickness(newValue);
		});

	}

	/**
	 * Add event handlers to this drawoption panel's shape buttons
	 */
	private void addShapeButtonsEventHandlers() {

		// Selection de la forme à dessiner
		rectangleButton.setOnAction(event -> shapeOptions.setShapeStyle(ShapeStyle.Rectangle));
		ellipseButton.setOnAction(event -> shapeOptions.setShapeStyle(ShapeStyle.Ellipse));
		triangleButton.setOnAction(event -> shapeOptions.setShapeStyle(ShapeStyle.Triangle));
		lineButton.setOnAction(event -> shapeOptions.setShapeStyle(ShapeStyle.Line));
		linkButton.setOnAction(event -> shapeOptions.setShapeStyle(ShapeStyle.Link));

		// Début du drag&drop
		ellipseButton.setOnDragDetected(onDragDetectedEventHandler);
		rectangleButton.setOnDragDetected(onDragDetectedEventHandler);
		triangleButton.setOnDragDetected(onDragDetectedEventHandler);
		lineButton.setOnDragDetected(onDragDetectedEventHandler);
		pentagonDragArea.setOnDragDetected(onDragDetectedEventHandler);
		diamondDragArea.setOnDragDetected(onDragDetectedEventHandler);
		cycleDragArea.setOnDragDetected(onDragDetectedEventHandler);
		matrixDragArea.setOnDragDetected(onDragDetectedEventHandler);
		treeDragArea.setOnDragDetected(onDragDetectedEventHandler);
		mindmapDragArea.setOnDragDetected(onDragDetectedEventHandler);
		cakeDragArea.setOnDragDetected(onDragDetectedEventHandler);

	}

	/**
	 * Apply the specified drawoptions to this drawoptions panel
	 * @param options DrawOptions to be applied
	 */
	public void setShapeOptions(DrawOptions options) {
		shapeOptions = options;
		shapeOptions.addObserver(this);
		updateDrawStyles();
	}

	/**
	 * Update displayed drawstyle options
	 */
	public void updateDrawStyles() {
		fillColor.setValue(shapeOptions.getFillColorStyle());
		lineColor.setValue(shapeOptions.getLineColorStyle());
		lineType.setValue(shapeOptions.getLineStyle());
		lineThickness.setValue(shapeOptions.getThickness());
		markSelectedShape();
	}

	/**
	 * Update this window's controls
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (selectedButton != null) {
			selectedButton.setEffect(null);
		}
		updateDrawStyles();
	}

	/**
	 * Get the selected shape button and apply a shadow to it
	 */
	private void markSelectedShape() {
		switch (shapeOptions.getShapeStyle()) {
		case Ellipse:
			selectedButton = ellipseButton;
			break;
		case Rectangle:
			selectedButton = rectangleButton;
			break;
		case Triangle:
			selectedButton = triangleButton;
			break;
		case Line:
			selectedButton = lineButton;
			break;
		default:
			break;
		}
		selectedButton.setEffect(new DropShadow());
	}

	/**
	 * Add images to shape buttons
	 */
	private void setImagesOnButton() {
		triangleButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/triangle.png"))));
		lineButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/line.png"))));
		linkButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/link.png"))));
		ellipseButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/circle.png"))));
		rectangleButton
				.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/rectangle.png"))));
		pentagonDragArea
				.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/pentagon.png"))));
		cycleDragArea.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/cycle.png"))));
		diamondDragArea.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/diamond.png"))));
		matrixDragArea.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/matrix.png"))));
		treeDragArea.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/tree.png"))));
		mindmapDragArea.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/resources/mindmap.png"))));

	}
}

package controller.creatikzwindow;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.text.ParseException;
import java.util.Observable;
import java.util.Observer;

import javax.ws.rs.ProcessingException;

import controller.ConflictViewController;
import controller.EditController;
import controller.FileViewController;
import controller.NewProjectDialogController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import model.DrawOptions;
import model.Model;
import model.ModelHistory;
import model.Project;
import model.services.Conflict;
import model.services.Server;
import util.LatexCompiler;

/**
 * Created by J on 23/03/2016.
 */
public class CreaTikzWindowController extends Stage implements Observer {

	@FXML
	private MenuItem openFileNewProjectItem;
	@FXML
	private MenuItem openFileItem;
	@FXML
	private MenuItem setRootItem;
	@FXML
	private MenuItem syncUpItem;
	@FXML
	private MenuItem syncDownItem;
	@FXML
	private MenuItem exitAppItem;
	@FXML
	private MenuItem infoItem;
	@FXML
	private MenuItem editUserDetailsItem;
	@FXML
	private MenuItem createProjectItem;
	@FXML
	private MenuItem uploadProject;
	@FXML
	private VBox topContainer;
	@FXML
	private BorderPane allContainer;
	@FXML
	private Button undoButton;
	@FXML
	private Button redoButton;
	@FXML
	private Button previewButton;
	@FXML
	private ToolBar toolbar;
	@FXML
	private DrawOptionsPanelController drawOptionsPanelController;
	@FXML
	private TikzTextPanelController tikzTextPanelController;
	@FXML
	private DrawZone drawZone;
	@FXML
	private Group groupForFigures;
	
	private ModelHistory history;
	private DrawOptions myOptions = new DrawOptions();

	/**
	 * Initializes the main tikz editor window
	 */
	@FXML
	private void initialize() {
		addMenuEvents();

		this.addEventHandler(ShapeSelectionEvent.OPTIONS_ALL, event -> {
			tikzTextPanelController.highlightSelectedLine(event.getLineNumber());
		});

		Scene scene = new Scene(allContainer, 800, 730);
		this.setScene(scene);
		this.setTitle("Creatikz Editor");
		this.show();

		drawZone.setOptions(myOptions);
		drawZone.setFiguresGroup(groupForFigures);
		drawZone.initialize();

		drawOptionsPanelController.setShapeOptions(myOptions);
	}

	/**
	 * Enables all controls
	 */
	private void enableEditing() {
		history = new ModelHistory(this);
		setDisableAll(false);
	}

	/**
	 * Disable or enable all diagram-related controls
	 * @param disable true to disable all diagram-related controls
	 */
	private void setDisableAll(boolean disable) {
		previewButton.setDisable(disable);
		drawOptionsPanelController.setDisable(disable);
		allContainer.getCenter().setDisable(disable);
		allContainer.getBottom().setDisable(disable);
		drawZone.setDisable(disable);
	}

	/**
	 * Updates the interface and adds current model to history if there has been a change of content
	 */
	@Override
	public void update(Observable o, Object arg) {

		Model previousModel = history.getPreviousModel();
		Model newModel = (Model) o;

		if (!history.getComeFromUndo() && !previousModel.equals(newModel)) {
			history.addNewModel(newModel);
		}

		updateInterface();

		history.setComeFromUndo(false);
	}

	/**
	 * Apply the current model to all controls
	 */
	private void updateInterface() {
		tikzTextPanelController.setModel(history.getCurrentModel());
		drawZone.setTikzModel(history.getCurrentModel());
		undoButton.setDisable(!history.hasPreviousModel());
		redoButton.setDisable(!history.hasNextModel());
		previewButton.setDisable(!history.hasCurrentModel());
	}
	
	/**
	 * Go back to previous model
	 */
	private void undoAction() {
		history.undo();
	}

	/**
	 * Go back to the cancelled model
	 */
	private void redoAction() {
		history.redo();
	}

	public static void setupTikz() throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(
				FXMLLoader.getDefaultClassLoader().getResource("view/creatikzwindow/creaTikzWindowLayout.fxml"));
		loader.load();
	}

	/**
	 * Configures actions on menu buttons
	 */
	private void addMenuEvents() {
		
		openFileItem.setOnAction(event -> openProjectFromInfo());
		openFileNewProjectItem.setOnAction(event -> createProjectFromTikz());
		exitAppItem.setOnAction(event -> logOut());
		createProjectItem.setOnAction(event -> createProject());
		infoItem.setOnAction(event -> FileViewController.setUp());
		uploadProject.setOnAction(event -> updateOnServer());
		editUserDetailsItem.setOnAction(event -> EditController.setUp());
		undoButton.setOnAction(event -> undoAction());
		redoButton.setOnAction(event -> redoAction());
		previewButton.setOnAction(event -> openPreview());
		
	}

	/**
	 * Logs the current users out and closes the tikz editor window
	 */
	private void logOut() {
		this.close();
		
		try {
			Server.logOut();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Create and set a new project by asking the user for a project name
	 */
	private void createProject() {
		try {
			Project proj = newProjectDialog();
			setProject(proj);
		} catch (IOException e) {
			new Alert(AlertType.ERROR, "Erreur lors de la création du projet").showAndWait();
		}
	}

	/**
	 * Displays a prompt asking the user for the new project's name
	 * @return Project initialized using the entered name
	 * @throws IOException if an error occurs while saving the project on disk
	 */
	private Project newProjectDialog() throws IOException {
		NewProjectDialogController projDialog = NewProjectDialogController.setUp();
		projDialog.showAndWait();
		return projDialog.getProject();
	}

	/**
	 * Attemps an update of the project on the server.\n
	 * If a conflict occurs, an interactive window lets the user decide which version to keep
	 */
	private void updateOnServer() {
		try {
			Conflict conf = Server.updateProject(Server.getCurrentProject());
			if (!conf.isDetected()) {

				
				String message = "";

				if(conf.getMine().equals(conf.getTheirs())){
					message+="Serveur à jour";
				}
				else{
					message+="Merge réussi";
				}
				if(conf.getMine().equals(Server.getCurrentProject().getPrj_data())){
					message+="\n(Aucune modification locale)";
				}
				
			
				

				Server.getCurrentProject().setPrj_data(conf.getMine());
				Server.getCurrentProject().setPrj_origin(conf.getMine());
				Model cmodel = Server.getCurrentProject().buildModel();
				history.addNewModel(cmodel); 
				Server.getCurrentProject().saveToDisk();
				new Alert(AlertType.CONFIRMATION, message).showAndWait();
			} else {
				ConflictViewController.setUp(conf);
				history.addNewModel(Server.getCurrentProject().buildModel());
				
			}
		} catch (ConnectException | ProcessingException ex) {
			new Alert(AlertType.ERROR, "Connexion avec le serveur impossible").showAndWait();
		} catch (IOException e) {
			new Alert(AlertType.ERROR, "Echec lors du lancement du solveur de conflit").showAndWait();
			e.printStackTrace();
		} catch (Exception ex) {
			new Alert(AlertType.ERROR, "Echec lors du traitement sur le serveur impossible").showAndWait();
		}
	}

	
	/**
	 * Creates a new project from a tikz file on disk selected by the user
	 */
	private void createProjectFromTikz() {
		try {
			File tikzFile = openFile(new FileChooser.ExtensionFilter("Diagram Tikz", "*.tikz"), "Select .tikz file");
			Project proj = newProjectDialog();
			proj.readTikzFromFile(tikzFile);
			setProject(proj);
		} catch (IOException ex) {
			new Alert(AlertType.ERROR, "Impossible d'importer ce fichier tikz dans un nouveau projet").showAndWait();
		}
	}

	/**
	 * Set the program's active project
	 * @param project the new active project
	 */
	private void setProject(Project project) {
		Server.setCurrentProject(project);
		this.setTitle(project.getBaseFolder());
		enableEditing();
		try {
			history.addNewModel(project.buildModel());
		} catch (ParseException e) {
			new Alert(AlertType.ERROR, e.getMessage()).show();
		}
	}

	/**
	 * Open a project from .info file
	 */
	private void openProjectFromInfo() {
		File infoFile = openFile(new FileChooser.ExtensionFilter("Project info", "*.info"),
				"Select project .info file");
		String projectName = infoFile.getParentFile().getName();
		setProject(new Project(projectName));
	}

	/**
	 * Opens a filechooser dialog with specified filter and title
	 * @param filter the filter to be applied to displayed files
	 * @param title the title of the filechooser dialog
	 * @return the chosen file
	 */
	private File openFile(ExtensionFilter filter, String title) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(Server.getActiveUser().getPersonalDir()));
		fileChooser.setTitle(title);
		fileChooser.getExtensionFilters().setAll(filter);
		return fileChooser.showOpenDialog(this);
	}

	/**
	 * Compiles the current project and opens the compiled file in the system's default reader
	 */
	private void openPreview() {
		String generatedPdf = "";
		try {
			generatedPdf = LatexCompiler.compile(Server.getCurrentProject());
			try {
				Desktop.getDesktop().open(new File(generatedPdf));
			} catch (Exception e) {
				new Alert(AlertType.ERROR, "Erreur lors de l'ouverture du fichier pdf generé").showAndWait();
			}
		} catch (IOException | InterruptedException e) {
			new Alert(AlertType.ERROR, "Erreur lors de la compilation").showAndWait();
		}

	}

}

package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.services.Conflict;

public class ConflictViewController extends Stage {
	@FXML
	private Text lblMine;
	@FXML
	private Label Mine;
	@FXML
	private Label Theirs;
	@FXML
	private Text lblTheirs;
	@FXML
	private Button btnMine;
	@FXML
	private Button btnTheirs;

	private Conflict conflict;

	/**
	 * 
	 * @return this ConflictViewer's Conflict instance
	 */
	public Conflict getConflict() {
		return conflict;
	}

	/**
	 * Set the ConflictViewer's Conflict instance and apply its content to the appropriate Text fields
	 * @param conflict the Conflict to be applied
	 */
	public void setConflict(Conflict conflict) {
		this.conflict = conflict;
		lblMine.setText(conflict.getMine());
		lblTheirs.setText(conflict.getTheirs());
	}

	/**
	 * Set up a new ConflictViewer window
	 * @param conflict conflict associated with this window
	 * @throws IOException If loading this window fails
	 */
	public static void setUp(Conflict conflict) throws IOException {
		FXMLLoader loader = new FXMLLoader(FXMLLoader.getDefaultClassLoader().getResource("view/ConflictView.fxml"));
		Parent root = loader.load();
		ConflictViewController conflictController = loader.getController();

		Scene scene = new Scene(root);
		conflictController.setScene(scene);
		conflictController.setTitle("Connexion");
		conflictController.setConflict(conflict);
		conflictController.showAndWait();
	}

	/**
	 * Add actions on this window's buttons
	 */
	@FXML
	public void initialize() {
		btnMine.setOnAction(event -> choose(Conflict.MINE));
		btnTheirs.setOnAction(event -> choose(Conflict.THEIRS));
	}

	/**
	 * Apply the user's choice
	 * @param decision Either Conflict.MINE, Conflict.THEIRS or Conflict.ORIGIN
	 */
	public void choose(int decision) {
		try {
			conflict.choose(decision);
			this.close();
		} catch (Exception ex) {
			new Alert(Alert.AlertType.ERROR, "Erreur lors de l'application du choix");
		}
	}
}

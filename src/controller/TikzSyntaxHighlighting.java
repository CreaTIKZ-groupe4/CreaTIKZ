package controller;

import java.util.function.IntFunction;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

public class TikzSyntaxHighlighting {
	
	/**
	 * Create a a new syntactically highlighted codeArea
	 * @return the created CodeArea
	 */
	public static CodeArea createCodeArea() {
		CodeArea tikzArea = new CodeArea();
		IntFunction<Node> lineNumberFactory = LineNumberFactory.get(tikzArea);
		IntFunction<Node> breakpointFactory = new TikzBreakpointFactory(tikzArea.currentParagraphProperty());
		IntFunction<Node> graphicFactory = line -> {
			HBox hbox = new HBox(lineNumberFactory.apply(line), breakpointFactory.apply(line));
			hbox.setAlignment(Pos.CENTER_LEFT);
			return hbox;
		};

		tikzArea.setParagraphGraphicFactory(graphicFactory);

		tikzArea.richChanges().filter(ch -> !ch.getInserted().equals(ch.getRemoved())) // XXX
				.subscribe(change -> {
					tikzArea.setStyleSpans(0, TikzKeyWords.computeHighlighting(tikzArea.getText()));
				});

		Scene scene = new Scene(new StackPane(tikzArea), 600, 200);
		scene.getStylesheets().add("Ressource/tikz-keywords.css");
		return tikzArea;
	}
}

package controller;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;

import javax.ws.rs.ProcessingException;

import controller.creatikzwindow.CreaTikzWindowController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.services.Server;
import model.services.User;
import util.CustomUtil;
import util.StringUtils;

public class LoginController extends Stage {

	@FXML
	private TextField txtUsername;
	@FXML
	private Button btnLogin;
	@FXML
	private Button btnRegister;
	@FXML
	private TextField txtNom;
	@FXML
	private TextField txtPrenom;
	@FXML
	private TextField txtMailCreation;
	@FXML 
	private PasswordField txtPasswordCreation;
	@FXML
	private PasswordField txtConfPasswordCreation;
	@FXML 
	private TextField txtMailConnection;
	@FXML
	private PasswordField txtPassword;
	@FXML
	private Label lblMessageCreation;
	@FXML
	private Label lblMessageConnexion;
	@FXML
	private CheckBox conditionsCheckbox;
	@FXML
	private Hyperlink conditionsUtilisation;

	/**
	 * Setup this Login Controller
	 * @throws IOException
	 */
	public static void setUp() throws IOException {
		FXMLLoader loader = new FXMLLoader(FXMLLoader.getDefaultClassLoader().getResource("view/login.fxml"));
		Parent root = loader.load();
		LoginController loginController = loader.getController();

		Scene scene = new Scene(root);
		loginController.setScene(scene);
		loginController.setTitle("Connexion");
		loginController.setResizable(false);
		loginController.show();

	}

	/**
	 * Initializes this controller's action listeners.
	 */
	@FXML
	public void initialize() {
		btnLogin.setOnAction(event -> login());
		btnRegister.setOnAction(event -> register());
		conditionsUtilisation.setOnAction(event -> showConditions());
	}

	/**
	 * Display the terms of service in an alert windows
	 */
	public void showConditions() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Conditions d'utilisation");
		alert.setHeaderText("Veuillez agréer avec les conditions d'utilisations suivantes:");

		String conditions;
		try {
			conditions = StringUtils.readStringFromFile(new File("src/resources/ConditionsUtilisation.txt"));
		} catch (IOException e) {
			e.printStackTrace();
			conditions = "Erreur: Veuillez lire les conditions générales dans resources/ConditionsUtilisation.txt";
		}

		TextArea textArea = new TextArea(conditions);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		alert.getDialogPane().setContent(textArea);

		alert.showAndWait();
	}

	/**
	 * Attempt a login using the provided fields
	 */
	public void login() {
		User user = new User();
		String username = txtMailConnection.getText();
		String password = txtPassword.getText();

		if (CustomUtil.isValidEmail(username)) {
			user.setEmail(username);
		} else {
			user.setUsername(username);
		}

		user.setPassword(password);
		try {
			Server.authenticate(user);
			try {
				user = Server.getActiveUser();
				CustomUtil.createUserRep();
				user.downloadProjects();
				CreaTikzWindowController.setupTikz();
				this.close();
			} catch (Exception e) {
				new Alert(AlertType.INFORMATION, "Erreur lors de l'initialisation de l'environnement de travail\n")
						.show();
				e.printStackTrace();
			}
		} catch (ProcessingException | ConnectException ex) {
			errorMessage("Impossible de se connecter au serveur");
		} catch (Exception ex) {
			errorMessage("Connexion echouée, verifiez vos identifiants");
		}
	}

	/**
	 * Set and display the passed error message
	 * @param message the message to be displayed
	 */
	public void errorMessage(String message) {
		lblMessageConnexion.setText(message);
		new Alert(AlertType.ERROR, message).show();
	}

	
	/**
	 * Register a new user using the provided fields
	 */
	public void register() {

		
		if (!conditionsCheckbox.isSelected()) {
			lblMessageCreation.setText("Acceptez les conditions d'utilisation");
			return;
		}

		if (!txtPasswordCreation.getText().equals(txtConfPasswordCreation.getText())) {
			lblMessageCreation.setText("Mots de passes différents");
			return;
		}

		if (!CustomUtil.isValidEmail(txtMailCreation.getText())) {
			lblMessageCreation.setText("Adresse mail invalide");
			return;
		}

		lblMessageCreation.setText("Echec à la création");

		String username = txtUsername.getText();
		String prenom = txtPrenom.getText();
		String nom = txtNom.getText();
		String mail = txtMailCreation.getText();
		String mdp = txtPasswordCreation.getText();

		User user = new User(username, prenom, nom, mail, mdp);
		try {
		Server.register(user);
		lblMessageCreation.setText("Compte créé");
		} catch (ConnectException ex) {
			new Alert(AlertType.ERROR,"Serveur injoignable").show();
		} catch (Exception ex) {
			new Alert(AlertType.ERROR,"Erreur lors du traitement sur le serveur").show();
		}
		

	}

}

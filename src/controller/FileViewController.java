package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import model.Project;
import model.services.Server;
import util.CustomUtil;

import javax.ws.rs.ProcessingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileViewController extends Dialog<Object> {

	@FXML
	private TreeView<File> filesTree;
	@FXML
	private Label prjName;
	@FXML
	private Label prjOwner;
	@FXML
	private Label prjLastModif;
	@FXML
	private Button btnPartager;

	private String pathOfFileToCopy = null;

	private TextField textField;

	/**
	 * Initializes project view dialog
	 * @throws IOException
	 */
	public static void setUp() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(FXMLLoader.getDefaultClassLoader().getResource("view/FileView.fxml"));
		try {
			BorderPane fileView = loader.load();
			FileViewController fileViewController = loader.getController();
			fileViewController.setContent(fileView);

			fileViewController.getDialogPane().getButtonTypes().add(ButtonType.FINISH);
			fileViewController.show();
		} catch(IOException ex) {
			new Alert(AlertType.ERROR,"Erreur à l'ouverture de la vue projets").show();
		}
	}

	/**
	 * Handles delete key press on project
	 */
	private final EventHandler<KeyEvent> deleteButtonEvent = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			if (event.getCode() == KeyCode.DELETE) {
				File file = filesTree.getSelectionModel().getSelectedItem().getValue();
				if(isEditable(file)) {
					try {
						if(isProjectDirectory(file)) {
							Project proj = new Project(file);
							proj.delete();
						} else {
							CustomUtil.deleteFolderOrFile(file);
							Server.deleteFileRequest(file.getPath());
						}
					} catch (IOException e) {
						new Alert(AlertType.ERROR,"Erreur à la suppression du projet").show();
						e.printStackTrace();
					} catch (Exception e) {
						new Alert(AlertType.ERROR,"Erreur à la suppression du projet coté serveur").show();
						e.printStackTrace();
					}
				} else {
					new Alert(AlertType.ERROR,"Ce fichier ne peut pas être modifié").show();
				}
				initialize();
			}
		}
	};

	/**
	 * Handles file movement between projects
	 */
	private final EventHandler<DragEvent> onDragDroppedEventHandler = event -> {
		Dragboard db = event.getDragboard();
		File sourceFile = new File(db.getString());
		TreeItem<File> parent = ((TreeCell<File>) event.getGestureTarget()).getTreeItem();
		File destinationFile = parent.getValue();
		try {
			Project proj = null;
			if(isProjectDirectory(sourceFile)) { //On récupére le projet avant de le déplacer sur le disque
				proj = new Project(sourceFile.getName()); 
			}
			
			if(!destinationFile.isDirectory()) {
				destinationFile = destinationFile.getParentFile();
			}
			
			Path sourcePath = sourceFile.toPath();
			Path targetPath = destinationFile.toPath().resolve(sourceFile.getName());

			if(!sourcePath.equals(targetPath)) {
				Files.move(sourcePath, targetPath, StandardCopyOption.ATOMIC_MOVE);
				Server.moveFileRequest(sourcePath.toString(), targetPath.toString());
	
				if(isRootDirectory(destinationFile)) { //Si un dossier est déplacé dans la racine, ce dossier devient un projet
					proj = new Project(targetPath.toFile());
					Server.createProject(proj);
				} else if(proj != null) { //Si  au contraire,un projet est déplacé dans un autre projet, il n'est plus considéré comme un projet et doit donc être supprimé
					proj.delete();
				}
			}
			
		} catch (IOException e) {
			new Alert(AlertType.ERROR,"Erreur au déplacement du fichier sur le disque").show();
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		initialize();
	};

	/**
	 * Handles dragover event from file movement
	 */
	private final EventHandler<DragEvent> onDragOverEventHandler = new EventHandler<DragEvent>() {
		@Override
		public void handle(DragEvent event) {
			if (event.getGestureSource() != this && event.getDragboard().hasString()) {
				event.acceptTransferModes(TransferMode.MOVE);
			}
			event.consume();
		}
	};

	/**
	 * Shares the selected project
	 */
	private void share() {
		TreeItem<File> pathEnd = filesTree.getSelectionModel().getSelectedItem();
		try {
			String name = pathEnd.getValue().getName();
			Project proj = new Project(name);
			if(Server.isSharedProject(proj)){
				new Alert(AlertType.INFORMATION, "Ce projet est déjà partagé").showAndWait();
			}
			else{
				Server.shareProject(proj);
				new Alert(AlertType.INFORMATION, "Partage du projet réussi").showAndWait();
			}
		} catch (NullPointerException e) {
			new Alert(AlertType.ERROR, "Aucun projet sélectionné").showAndWait();
		} catch (Exception ex) {
			new Alert(AlertType.ERROR, ex.getMessage()).showAndWait();
		}

	}

	/**
	 * Handles treeview cells creation, edit and update.\n
	 * Adds required listeners.
	 */
	private Callback<TreeView<File>, TreeCell<File>> cellFactory = new Callback<TreeView<File>, TreeCell<File>>() {
		@Override
		public TreeCell<File> call(TreeView<File> fileTreeView) {

			TreeCell<File> treeCell = new TreeCell<File>() {
				public File currentFile = null;

				@Override
				protected void updateItem(File item, boolean empty) {
					super.updateItem(item, empty);

					if (empty || item == null) {
						setGraphic(null);
						setText(null);
					} else if (isEditing()) {
						if (textField != null) {
							textField.setText(currentFile.getName());
						}
						setText(null);
						if (FileViewController.isEditable(item) ) {
							setGraphic(textField);
						}
					} else {
						setText(item.getName());
						setGraphic(getTreeItem().getGraphic());
					}

				}

				@Override
				public void startEdit() {

					super.startEdit();
					setCurrentFile();

					if (FileViewController.isEditable(currentFile)) {
						if (textField == null)
							createTextField();
						textField.setText(currentFile.getName());
						setText(null);
						setGraphic(textField);
						textField.selectAll();
					}
				}

				@Override
				public void cancelEdit() {
					super.cancelEdit();
					setText(getItem().getName());
					setGraphic(getTreeItem().getGraphic());
				}

				/**
				 * Create a textfield to edit the name of the file
				 */
				private void createTextField() {
					textField = new TextField();
					textField.setOnKeyReleased(e -> {

 						String oldPath = currentFile.getPath();
 						String newPath = currentFile.toPath().resolveSibling(textField.getText()).toString();
 					
						if (e.getCode() == KeyCode.ENTER) {
							try {
								if(isProjectDirectory(currentFile)) {
									Project proj = new Project(currentFile);
									proj.move(textField.getText());
								} else {
		 							Server.renameFileRequest(oldPath, newPath);
		 							currentFile.renameTo(new File(newPath));
								}
								commitEdit(currentFile);
								textField = null;
							} catch (ProcessingException ex) {
								new Alert(AlertType.ERROR, "Mise à jour du serveur impossible").showAndWait();
							} catch (IOException ex) {
								new Alert(AlertType.ERROR, "Erreur lors de la modification du nom").showAndWait();
							} catch (Exception ex ){
								new Alert(AlertType.ERROR, "Erreur lors de la modification du nom coté serveur" ).showAndWait();
								ex.printStackTrace();
							}
							initialize();

						} else if (e.getCode() == KeyCode.ESCAPE) {
							cancelEdit();
						}
					});

				}

				/**
				 * Set the selected file
				 */
				private void setCurrentFile() {
					currentFile = new File(getString());
				}

				/**
				 * Get the name of the selected element
				 * @return
                 */
				private String getString() {
					return getItem() == null ? "" : getItem().toString();
				}
			};
			
			treeCell.setOnDragDetected(onDragDetectedEventHandler);
			treeCell.setOnDragOver(onDragOverEventHandler);
			treeCell.setOnDragDropped(onDragDroppedEventHandler);
			treeCell.setOnMouseClicked(e -> setInfo());
			return treeCell;
		}
	};

	/**
	 * Initializes the TreeView using the active user's personal directory as root
	 */
	private void initializeFilesTree() {
		filesTree.setCellFactory(cellFactory);

		File file = new File(Server.getActiveUser().getPersonalDir());
		TreeItem<File> root = new TreeItem<>(file);
		filesTree.setRoot(root);
		root.setExpanded(true);
		populateNode(root, file.listFiles());
	}

	/**
	 * Initialize this FileView
	 */
	@FXML
	private void initialize() {
		btnPartager.setOnAction(event -> share());
		initializeFilesTree();
		addKeyListener();
	}

	/**
	 * Stores the dragged file in the dragboard
	 */
	private final EventHandler<MouseEvent> onDragDetectedEventHandler = event -> {
		TreeCell<File> treeCell = (TreeCell<File>) event.getSource();
		if(isEditable(treeCell.getItem())) {
			Dragboard db = treeCell.startDragAndDrop(TransferMode.ANY);
			ClipboardContent content = new ClipboardContent();
			File file = treeCell.getTreeItem().getValue();
			content.putString(file.getPath());
			db.setContent(content);
		} else {
			new Alert(AlertType.ERROR,"Ce fichier ne peut pas être déplacé").show();
		}
	};

	/**
	 * Adds copy(ctrl+c)/paste(ctrl+v) and delete key listeners
	 */
	private void addKeyListener() {
		this.getDialogPane().getScene().getAccelerators()
				.put(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY), () -> copyFile());

		this.getDialogPane().getScene().getAccelerators()
				.put(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_ANY), () -> pasteFile());

		filesTree.setOnKeyPressed(deleteButtonEvent);
	}

	/**
	 * Stores the file to be pasted
	 */
	private void copyFile() {
		pathOfFileToCopy = filesTree.getSelectionModel().getSelectedItem().getValue().getPath();
	}

	/**
	 * Determine whether or not a file is the tree's root
	 * @param file
	 * @return true or false
	 */
	private boolean isProjectDirectory(File file) {
		return file.getParentFile().equals(new File(Server.getActiveUser().getPersonalDir()));
	}

	/**
	 * Determine whether or not a file is the tree's root
	 * @param file
	 * @return true or false
     */
	private boolean isRootDirectory(File file) {
		return file.equals(new File(Server.getActiveUser().getPersonalDir()));
	}

	/**
	 * Paste file in selected location
	 * @throws IOException 
	 */
	private void pasteFile()  {
		if (pathOfFileToCopy != null) {
			File source = new File(pathOfFileToCopy);
			File target = filesTree.getSelectionModel().getSelectedItem().getValue();

			if (!source.equals(target)) {
				if(isEditable(source)) {
					try {	
						if(isProjectDirectory(source) && isRootDirectory(target)) {
								Project proj = new Project(source);
								proj.copy(proj.getPrj_name()+"(copy)");
						} else {
			 				copyFolderOrFile(source, target);
			 				Server.copyFile(source.getPath(), target.getPath());
						}
					} catch (IOException e) {
						new Alert(AlertType.ERROR,"Erreur lors de la copie du fichier").show();
						e.printStackTrace();
					} catch(Exception e) {
						new Alert(AlertType.ERROR,"Erreur lors de la copie du fichier coté serveur").show();
					}
					pathOfFileToCopy = null;
					initialize();
				} else {
					new Alert(AlertType.ERROR,"Ce fichier ne peut pas être copié collé").show();
				}
			}
		}
	}

	/**
	 * Recursively copies a folder or file into another folder
	 * @param source file to be copied
	 * @param target directory to copy into
	 */
	public static void copyFolderOrFile(File source, File target) {
		Path targetPath = target.toPath().resolve(source.getName());
		try {
			Files.copy(source.toPath(), targetPath, StandardCopyOption.REPLACE_EXISTING,
					StandardCopyOption.COPY_ATTRIBUTES);
			if (source.isDirectory()) {
				for (File file : source.listFiles()) {
					copyFolderOrFile(file, new File(targetPath.toString()));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Populate a node with a file
	 * @param node the node to be populated
	 * @param files the files to add as a child of this node
	 */
	private void populateNode(TreeItem<File> node, File... files) {

		for (File file : files) {
			TreeItem<File> currentFile = new TreeItem<>(file);
			if (file.isDirectory()) {
				populateNode(currentFile, file.listFiles());
				if (!currentFile.getChildren().isEmpty()) {
					node.getChildren().add(currentFile);
				}
			} else {
				node.getChildren().add(currentFile);
			}
		}

	}

	/**
	 * Load the selected project's info from its info file
	 */
	private void setInfo() {
		TreeItem<File> pathEnd = filesTree.getSelectionModel().getSelectedItem();
		try {
			if(!isProjectDirectory(pathEnd.getValue())) {
				return;
			}
			Project proj = new Project(pathEnd.getValue());
			proj.readInfoFromDisk();
			prjName.setText(proj.getPrj_name());
			prjOwner.setText(proj.getPrj_owner_name());
			prjLastModif.setText(proj.getPrj_timestamp().toString());
		} catch (IOException e) {
			new Alert(AlertType.ERROR, "Erreur à la récupération des infos du projet").showAndWait();
		} catch (NullPointerException e) {
			new Alert(AlertType.ERROR, "Aucun projet sélectionné").showAndWait();
		}
	}

	/**
	 * 
	 * @param pane sets this FileView's content
	 */
	private void setContent(BorderPane pane) {
		this.getDialogPane().setContent(pane);
	}
	
	/**
	 * Checks wether this file can be edited (renamed and/or deleted).
	 * All files but main.tikz and .info files can be edited
	 * @param file the file to be checked
	 * @return True if the file can be edited
	 */
	private static boolean isEditable(File file) {
		return file != null && !(file.getName().equals("main.tikz") || file.getName().equals(".info")) ;
		
	}
}

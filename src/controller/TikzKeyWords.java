package controller;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;

public class TikzKeyWords {
	private static final String[] KEYWORDS = new String[] { "rectangle", "documentclass", "usepackage",
			"usetikzlibrary", "begin", "ellipse", "draw", "and", "cycle", "width", "dotted", "fill", "end", "line", "node","matrix","path" };
	private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
	private static final String PAREN_PATTERN = "\\(|\\)";
	private static final String BRACE_PATTERN = "\\{|\\}";
	private static final String BRACKET_PATTERN = "\\[|\\]";

	private static final String[] MATCH_KEYWORDS = new String[] { "keyword", "paren", "brace", "bracket", "semicolon",
			"string", "comment" };
	
	private static final Pattern PATTERN = Pattern.compile("(?<KEYWORD>" + KEYWORD_PATTERN + ")" + "|(?<PAREN>"
			+ PAREN_PATTERN + ")" + "|(?<BRACE>" + BRACE_PATTERN + ")" + "|(?<BRACKET>" + BRACKET_PATTERN + ")");

	/**
	 * Calculate the text's style spans by this classes keywords
	 * @param text the text for which to extract the StyleSpans
	 * @return the initialised StyleSpans for this text
	 */
	public static StyleSpans<Collection<String>> computeHighlighting(String text) {
		Matcher matcher = PATTERN.matcher(text);
		int lastKeywordEnd = 0;
		StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
		while (matcher.find()) {
			String styleClass = null;
			for (String keyword : MATCH_KEYWORDS) {
				if (matcher.group(keyword.toUpperCase()) != null) {
					styleClass = keyword;
					break;
				}
			}

			spansBuilder.add(Collections.emptyList(), matcher.start() - lastKeywordEnd);
			spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
			lastKeywordEnd = matcher.end();
		}
		spansBuilder.add(Collections.emptyList(), text.length() - lastKeywordEnd);
		return spansBuilder.create();
	}
}
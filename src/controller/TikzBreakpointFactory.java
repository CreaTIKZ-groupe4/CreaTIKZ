package controller;

import java.util.function.IntFunction;

import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import util.TikzParser;

public class TikzBreakpointFactory implements IntFunction<Node> {
	private final ObservableValue<Integer> shownLine;

	TikzBreakpointFactory(ObservableValue<Integer> shownLine) {
		this.shownLine = shownLine;
	}

	/**
	 * Display the breakpoint at the appropriate line
	 * @param lineNumber the line to be marked
	 */
	@Override
	public Node apply(int lineNumber) {
		Circle circle = new Circle(5);
		circle.setFill(Color.BLUE);
		circle.setVisible(lineNumber >= TikzParser.LINE_BEGIN && shownLine.getValue() == lineNumber);

		return circle;
	}
}

Conditions générales d'utilisation du programme CreaTikz

Accès au service:
En utilisant ce programme (Éditeur de code CreaTikz) vous (utilisateur) acceptez nos (groupe 4) termes d'utilisation du service (ce programme).
Dans le cas où vous n'acceptez pas tout ou partie de nos conditions, nous vous demandons de renoncer à utiliser ce programme.
Nous nous réservons le droit de modifier les conditions d'utilisation à tout moment sans notification.

Mentions légales:
Ce programme est créé par le groupe 4 pour le projet CreaTikz du cours de Génie Logiciel de l'ULB
Les programmeurs sont:
Julien Durieux
Dansy Efila
Zakaria Ahr
Gabriel Ortega
Jey Toucourt
Axel Abels
Kaio Icaro Lopes
Akira Baes

Gestion des données personnelles:
Nous ne revendrons pas vos informations personnelles à des tiers pour profit.
Nous ne pouvons donner aucune garanties de sécurité pour la protection de vos données personnelles.

Utilisation du programme:
Nous déclinons toute responsabilité pour tout dégât causé à tout appareil dû à l'utilisation de ce programme.
Vous utilisez ce programme à vos propres risques.

Propriété intellectuelle:
Ce programme a été créé dans un cadre scolaire.
Tout diagramme créé par vous (utilisateur) dans le cadre de ce programme est votre propriété, 
mais nous nous gardons le droit de modifier, supprimer, dupliquer, transférer, distribuer, afficher, revendre, utiliser à des fins promotionelles et archiver vos diagrammes et toute information produite via ce programme.
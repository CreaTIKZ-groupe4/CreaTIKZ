package util;

import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import model.services.Server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by J on 26/03/2016.
 */
public class CustomUtil {

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	/**
	 * Selects a directory
	 * @param initDirectoryPath the given directory
	 * @param primaryStage JavaFx's primary stage
	 */
	public static File selectDirectory(String initDirectoryPath, Stage primaryStage) {
		File defaultDirectory = new File(initDirectoryPath);
		DirectoryChooser rootDirectoryChooser = new DirectoryChooser();
		rootDirectoryChooser.setInitialDirectory(defaultDirectory);
		rootDirectoryChooser.setTitle("Select folder");
		File selectedDirectory = rootDirectoryChooser.showDialog(primaryStage);
		return selectedDirectory;
	}

	/**
	 * Creates the user's directory based on his id
	 */
	public static void createUserRep() throws IOException {
		if (Server.getActiveUser() != null) {
			Files.createDirectories(Paths.get(Server.getActiveUser().getPersonalDir()));
		}
	}
	/**
	 * Creates the default directory for the Creatikz projects
	 */
	public static void createDefaultRep() throws IOException {
		Files.createDirectories(Paths.get(System.getProperty("user.home") + "/CreaTIKZ/Default"));

		Files.createDirectories(Paths.get(Server.getActiveUser().getPersonalDir()));
	}
	/**
	 * Finds the current working directory
	 * @return the current  user's working directory
	 */
	public static String getWorkingDirPath() {

		return Server.getActiveUser() != null ? Server.getActiveUser().getPersonalDir()
				: System.getProperty("user.home") + "/CreaTIKZ/Default";

	}
	/**
	 * Does an email verification
	 * @param email The email to check
	 * @return the boolean state for the validity of the email
	 */
	public static Boolean isValidEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.find();

	}
	/**
	 * Deletes a given file or folder
	 * @param source the file/folder to delete
	 */
	public static void deleteFolderOrFile(File source) {
		try {
			if (source.isDirectory()) {
				for (File file : source.listFiles()) {
					deleteFolderOrFile(file);
				}
			}
			Files.deleteIfExists(Paths.get(source.getPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** 
	 * Copies a given file to a given destination
	 * @param source The file to copy
	 * @param destination The destination where to Copy the file
	 * 
	 */
	public static void recursiveCopy(File source, File destination) throws IOException {
		Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES);
		if (source.isDirectory()) {
			for (File file : source.listFiles()) {
				recursiveCopy(file, destination.toPath().resolve(file.getName()).toFile());
			}
		}
	}

	/**
	 * Gets the minimum of an array
	 * @param values
	 * @return
     */
	public static Double getMin(Double... values){
		double min = Double.MAX_VALUE;
		for (Double value : values) {
			min = Math.min(min, value);
		}
		return min;

	}

	/**
	 * Gets the maximum value of an array
	 * @param values
	 * @return
	 */
	public static Double getMax(Double... values){
		double max = 0;
		for (Double  value : values) {
			max = Math.max(max, value);
		}
		return max;
	}
}

package util;

import java.util.List;

import model.Model;
import model.shapes.CustomShape;

public class TikzGenerator {

	public final static String LATEX_HEADER = "\\documentclass{article}" + System.lineSeparator() + "\\usepackage{tikz,pdflscape}"
			+ System.lineSeparator() + "\\usetikzlibrary{shapes.geometric,trees, matrix, mindmap}" + System.lineSeparator()
			+ "\\begin{document}"+System.lineSeparator()+"\\begin{landscape}" + System.lineSeparator()
			+ "\\begin{tikzpicture}" + System.lineSeparator();

	public final static String LATEX_FOOTER = "\\end{tikzpicture}" + System.lineSeparator() + "\\end{landscape}" + System.lineSeparator() + "\\end{document}"
			+ System.lineSeparator();
	/**
	 * Converts the shape items to a tikz Code
	 * @param model The model on which the conversion is based
	 * @return the generated code
	 */
	public static String convertToTikz(Model model) {

		// Add default latex header
		String tikzCode = LATEX_HEADER;
		List<CustomShape> shapesList = model.getComponents();

		tikzCode += parseShapeList(shapesList);

		// Add footer
		tikzCode += LATEX_FOOTER;

		return tikzCode;
	}
	/**
	 * Parses the shape items into a string
	 * @param shapesList A list of the existing shapes
	 * @return a string containing the shapes
	 */
	private static String parseShapeList(List<CustomShape> shapesList) {
		String returnString = "";
		for (CustomShape shape : shapesList) {
			returnString += shape.toString();
		}
		return returnString;
	}

}

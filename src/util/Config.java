package util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import model.services.Server;

public class Config {

	static Properties properties = new Properties();
	/**
	 * Load the properties.ini file to define the server configuration.
	 */
	public static void loadProperties() {
		if (properties.isEmpty()) {
			try {
				FileReader fileReader;
				fileReader = new FileReader("properties.ini");
				properties.load(fileReader);
			} catch (IOException e) {
				System.out.println("fichier de configuration properties.ini introuvable");
				System.exit(1);
			}
		}
	}
	
	/**
	 * Gets the property used
	 * @param property current property
	 * @return the property loaded
	 */
	public static String getProperty(String property) {
		loadProperties();

		return properties.getProperty(property);
	}
	
	/**
	 * Saves the properties.ini file
	 */
	public static void save() throws IOException {
		FileOutputStream fos = new FileOutputStream(new File("properties.ini"));
		properties.store(fos, "Modification date: "+new Date().toString());
	}
	/**
	 * Sets the wanted property
	 * @param property current property
	 * @param value wanted property
	 */
	public static void setProperty(String property,String value) throws IOException {
		loadProperties();
		properties.setProperty(property,value);
		save();
	}
	/**
	 * Shows if the server is offline
	 * @return the boolean state of the offline mode
	 */
	public static boolean offlineMode() {
		return Boolean.parseBoolean(getProperty("offline")) || !Server.isOnline();
	}
}

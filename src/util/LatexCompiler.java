package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import model.Project;

public class LatexCompiler extends Thread {
	InputStream is;
	String type;

	public LatexCompiler(InputStream is) {
		super();
		this.is = is;
	}
	/**
	 * Runs the main Reader
	 */
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while (br.readLine() != null) {
				// Flush the stream
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	/**
	 * Compiles the file into a new PDF One
	 * @param proj The project to compile
	 * @return the Pdf File compiled
	 */
	public static String compile(Project proj) throws IOException, InterruptedException {
		String tikzFile = proj.getBaseFolder() + Project.MAIN_FILE;
		String outputFolder = proj.getBaseFolder() + "pdf/";
		String jobName = "main-" + System.currentTimeMillis();

		File directory = new File(outputFolder);
		directory.mkdirs();

		Process p = Runtime.getRuntime()
				.exec(new String[] { "pdflatex", tikzFile, "-output-directory", outputFolder, "-job-name=" + jobName });

		emptyOutput(p);
		p.waitFor();
		deteleAuxFiles(outputFolder + jobName);
		return outputFolder + jobName + ".pdf";
	}

	private static void emptyOutput(Process p) {
		LatexCompiler errorGobbler = new LatexCompiler(p.getErrorStream());
		LatexCompiler outputGobbler = new LatexCompiler(p.getInputStream());
		errorGobbler.start();
		outputGobbler.start();
	}
	/**
	 * Deletes the aux and log files
	 * @param path The path of the files to delete
	 */
	private static void deteleAuxFiles(String path) {
		File aux = new File(path + ".aux");
		File log = new File(path + ".log");
		aux.delete();
		log.delete();
	}
}
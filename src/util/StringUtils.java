package util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class StringUtils {
	/**
	 * Verifies if a given string isn't empty
	 * @param string The string to check
	 * @return the Boolean state for the emptiness of the string
	 */
	public static Boolean isNullOrEmpty(String string) {
		return (string == null || string.isEmpty());
	}
	/**
	 * Puts the datas of a file into a string
	 * @param File The file to read
	 * @return The new string containing the file
	 */
	public static String readStringFromFile(File file) throws IOException {
		return new String(Files.readAllBytes(file.toPath()));
	}
}

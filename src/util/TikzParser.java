package util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import model.Model;
import model.shapes.CustomEllipse;
import model.shapes.CustomLine;
import model.shapes.CustomMatrix;
import model.shapes.CustomMindMap;
import model.shapes.CustomNode;
import model.shapes.CustomRectangle;
import model.shapes.CustomShape;
import model.shapes.CustomTriangle;
import model.styles.ColorStyle;

public class TikzParser {

	public static final int LINE_BEGIN = 6;
	public static List<CustomShape> components;

	public static void parse(String tikzCode, Model baseModel) throws ParseException {
		parse(tikzCode, baseModel, false, true);
	}
	/**
	 * Parses the tikz code into shape items
	 * @param tikzCode the code to parse
	 * @param add a boolean checking if the Model is empty 
	 * @return 
	 */
	public static List<CustomShape> parse(String tikzCode, Model baseModel, Boolean add, Boolean update) throws ParseException {
		components = new ArrayList<CustomShape>();

		while (tikzCode.contains("\\matrix")) {
			String matrice = tikzCode.substring(tikzCode.indexOf("\\matrix"), tikzCode.indexOf("\n};\n") + 2);
			treatMatrix(matrice);
			tikzCode = tikzCode.replaceFirst(Pattern.quote(matrice), "");
		}

		String[] splittedLines = tikzCode.split("\\\\");

		for (int lineIndex = 0; lineIndex < splittedLines.length; lineIndex++) {
			try {
				treatLine(splittedLines[lineIndex]);
			} catch (Exception ex) {

				ex.printStackTrace();
				throw new ParseException(
						"erreur de syntaxe Ã  la ligne " + lineIndex + System.lineSeparator() + splittedLines[lineIndex],
						lineIndex);
			}
		}
		if (add) {
			baseModel.addComponents(components,update);
		} else {
			baseModel.setComponents(components,update);
		}
		return components;
	}
	/**
	 * parses a Line
	 * @param line the line to treat
	 */
	private static void treatLine(String line) {

		line = removeUselessCharacters(line);
		if (line.startsWith("node")) {
			components.add(treatNode(line));
		} else if (line.startsWith("path")) {
			CustomMindMap mindmap = treatPath(line);
			components.add(mindmap);

		} else if (line.startsWith("draw")) {
			components.add(treatShape(line));
		}
	}

	/**
	 * parse a matrix
	 * 
	 * @param line
	 *            a line representing a matrix
	 */
	private static void treatMatrix(String line) {
		CustomMatrix matrix = new CustomMatrix();
		String options = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
		String[] sep = options.split("=|,");
		String colSep = "";
		String rowSep = "";

		String regex = "[\\s+a-zA-Z :]";
		switch (sep[0]) {
		case "column sep":
			colSep = sep[1].replaceAll(regex, "");
			rowSep = sep[3].replaceAll(regex, "");
			break;

		case "row sep":
			rowSep = sep[1].replaceAll(regex, "");
			colSep = sep[3].replaceAll(regex, "");
			break;

		default:
			break;
		}

		matrix.setColSep(Double.parseDouble(colSep));
		matrix.setRowSep(Double.parseDouble(rowSep));

		String toRemove = line.substring(line.indexOf("\\matrix"), line.indexOf(" {\n") + 2);
		String elements = line.replace(toRemove, "");
		elements = elements.replace("};", "");

		String[] splittedLines = elements.split("\\\\\\\\");
		for (int i = 0; i < splittedLines.length; i++) {
			String[] nodes = splittedLines[i].split("&");
			for (int j = 0; j < nodes.length; j++) {
				String currentLine = nodes[j].replace("\\", "");
				currentLine = removeUselessCharacters(currentLine);
				CustomShape shape = null;

				if (currentLine.startsWith("node")) {
					shape = treatNode(currentLine);
				} else if (currentLine.startsWith("draw")) {
					shape = treatShape(currentLine);
				}
				matrix.addComponent(i, j, shape);
			}
		}
		components.add(matrix);
	}
	/**
	 * Parses a tikz code to be used for the MindMap library
	 * @param line The line to treat
	 * @return a MindMap
	 */
	public static CustomMindMap treatPath(String line) {

		CustomMindMap mindMap = null;
		Color backgroundColor = null;
		Color textcolor = null;

		try {
				try {
					String options = line.substring(line.indexOf("["), line.indexOf("]")+1);
					line = line.replace(options, "");
					if (!options.equals("")) {
						try {
							int colorIndex = options.indexOf("color=") + "color=".length();
							int colorend = options.lastIndexOf(",");
							if(colorend < colorIndex)  {
								colorend = options.lastIndexOf("]");
							}
							String bgdcolorString = options.substring(colorIndex, colorend);
							backgroundColor = ColorStyle.findColorByName(bgdcolorString);
						} catch(Exception ex) {
							//No color
						}
						try {
							int textIndex = options.indexOf("text=")+"text+".length();
							int colorend = options.lastIndexOf(",");
							if(colorend<textIndex)  {
								colorend = options.lastIndexOf("]");
							}
							String textColorString = options.substring(textIndex,colorend);
							textcolor = ColorStyle.findColorByName(textColorString);
						} catch(Exception ex) {
							//No text color
						}
					}
				}catch (Exception ex) {
					//No options available
				}
				
			if(line.startsWith("child{")) {
				line = line.substring("child{".length(), line.length());
			}

			String firstNode = getFirstNode(line);
			Integer clock = getClockWiseFromString(firstNode);
			String label = getLabelFromLine(firstNode);
			
			line = line.replace(firstNode, "");
			mindMap = new CustomMindMap(backgroundColor, textcolor, clock, label);

			try {
				String coordString = firstNode.substring(firstNode.indexOf("at(")+"at(".length(),firstNode.indexOf(")"));	
				Double x = Double.parseDouble(coordString.split(",")[0]);
				Double y = Double.parseDouble(coordString.split(",")[1]);
				mindMap.setCoords(x*CustomShape.XSCALE,y*CustomShape.YSCALE);
				
			} catch(Exception ex){
				}
				
			List<String> children = getChildren(line);
			for (String child : children) {
				mindMap.addChild(treatPath(child));
			}

		} catch (IndexOutOfBoundsException | NumberFormatException e) {
			e.printStackTrace();
		}

		return mindMap;
	}
	/**
	 * Gets the first node of the tree
	 * @param line the Line to treat
	 * @return the wanted node
	 */
	private static String getFirstNode(String line) {
		int begin = line.indexOf("node");
		int childIndex = line.indexOf("child");
		if (childIndex < begin)
			childIndex = line.length();
		if (begin == -1)
			begin = 0;
		String firstNode = line.substring(begin, childIndex);
		return firstNode;
	}
	/**
	 * Gets the label from a Line
	 * @return the wanted label
	 */
	private static String getLabelFromLine(String str) {
		return str.substring(str.indexOf("{") + 1, str.lastIndexOf("}")).replace("{", "").replace("}", "");
	}
	/**
	 * Gets the clockWise from a string
	 * @param line The line to treat
	 * @return the clockwise
	 */
	private static Integer getClockWiseFromString(String line) {
		Integer clockWise = null;
		try {
			int start = line.indexOf("=");
			int end = line.lastIndexOf("]");
			if (end == -1)
				end = line.length();
			if (start == -1)
				start = 0;
			clockWise = Integer.parseInt(line.substring(start + 1, end));
		} catch (NumberFormatException e) {
		}
		return clockWise;

	}
	/**
	 * Parses a line into a node
	 * @param line the Line to treat
	 * @return the node
	 */
	private static CustomNode treatNode(String line) {
		CustomNode node = new CustomNode();
		try {
			String options = line.substring(0, line.indexOf("{")).substring(line.indexOf("[") + 1, line.indexOf("]"));
			if (!options.equals("")) {
				parseOptions(node.getFigure(), options);
			}
		} catch (IndexOutOfBoundsException e) {
		}
		try {
			String label = line.substring(line.indexOf("{") + 1, line.indexOf("}"));
			node.setLabel(label);

			try {
				String coordString = line.substring(line.indexOf("at(")+"at(".length(),line.indexOf(")"));	
				Double x = Double.parseDouble(coordString.split(",")[0]);
				Double y = Double.parseDouble(coordString.split(",")[1]);
				node.setCoords(x*CustomShape.XSCALE,y*CustomShape.YSCALE);
			} catch(Exception ex){}
				
			String childrenString = line.substring(line.indexOf("}") + 1);
			List<String> children = getChildren(childrenString);
			for (String child : children) {
				child = child.substring("child{".length(),child.length()-1);
				node.addChild(treatNode(child));
			}
		} catch (IndexOutOfBoundsException e) {
		}

		return node;
	}

	/**
	 * Gets the children of a node
	 * @param line the line to treat
	 * @return list of the children 
	 */
	private static List<String> getChildren(String line) {
		ArrayList<String> children = new ArrayList<String>();
		int start = line.indexOf("{");
		int level = 0;
		if (start == -1) {
			return children;
		}
		
		int end=0;
		for (int index = start; index < line.length(); index++) {
			if (line.charAt(index) == '{') {
				level++;
				if (level == 1) {
					start = index + 1;
					int childIndex =  end + line.substring(end,start).indexOf("child");
					start = childIndex == -1 ? start : childIndex;
				}
			} else if (line.charAt(index) == '}') {
				level--;
				if (level == 0) {
					end = index+1;
					String child = line.substring(start, index + 1);
					children.add(child);
					
				}
			}
		}
		return children;
	}
	/**
	 * parses a line into a shape
	 * @param line the line to treat
	 * @return the shape
	 */
	private static CustomShape treatShape(String line) {
		line = removeUselessCharacters(line);

		if (line.isEmpty()) {
			return null;
		}
		if (isUselessLine(line)) {
			return null;
		}
		String[] optionsAndRequest = extractOptionsAndRequest(line);
		String request = optionsAndRequest[1];
		String options = optionsAndRequest[0];
		CustomShape newShape;
		if (request.contains("cycle")) {
			newShape = parsePolygon(request, options);
		} else if (request.contains("--")) {
			newShape = parseLink(request, options);
		} else if (request.contains("ellipse")) {
			newShape = parseEllipse(request, options);
		} else {
			newShape = parseRectangle(request, options);
		}

		try {
			String label = line.substring(line.indexOf("{") + 1, line.indexOf("}"));
			newShape.setLabel(label);
		} catch (IndexOutOfBoundsException e) { // No label
		}

		return newShape;
	}

	/**
	 * Parses the line of the Options and Requests
	 * @param the line to treat
	 * @return the string contening the Options and Requests
	 */
	private static String[] extractOptionsAndRequest(String line) {
		String returnValue[] = new String[2];
		if (line.contains("[")) {
			String[] splittedCode = line.split("\\[");
			String[] subCode = splittedCode[1].split("]");
			returnValue = subCode;
		} else {
			returnValue[1] = line.substring(line.indexOf('('));
			returnValue[0] = "";
		}
		return returnValue;
	}
	/**
	 * Removes the useless characters of a line
	 * @param line the line to treat
	 * @return th new line
	 */
	private static String removeUselessCharacters(String line) {
		line = line.replaceAll("\\s+", "");
		line = line.replaceAll(";", "");
		return line;
	}
	/**
	 * Checks if the line is "useless" (first heading lines)
	 * @param line the line to Treat
	 * @return
	 */
	public static boolean isUselessLine(String line) {
		return line.contains("\\begin") || line.contains("\\use") || line.contains("\\end")
				|| line.contains("\\documentclass");
	}

	/**
	 * Parses a line into a rectangle
	 * 
	 * @param request
	 * @param options
	 * @return
	 */
	private static CustomShape parseRectangle(String request, String options) {

		Rectangle newRectangle = new Rectangle();
		newRectangle.setStroke(ColorStyle.findColorByName("black"));
		newRectangle.setFill(null);

		parseOptions(newRectangle, options);

		CustomShape rectangle = new CustomRectangle(newRectangle);
		parseCoordinates(request, ",", rectangle);
		rectangle.setId(getNodeId(request));
		return rectangle;
	}

	/**
	 * Parses a line into an ellipse
	 * 
	 * @param request
	 * @param options
	 * @return
	 */
	private static CustomShape parseEllipse(String request, String options) {

		Ellipse newEllipse = new Ellipse();
		newEllipse.setStroke(ColorStyle.findColorByName("black"));
		newEllipse.setFill(null);
		parseOptions(newEllipse, options);

		CustomShape ellipse = new CustomEllipse(newEllipse);
		parseCoordinates(request, "and", ellipse);
		ellipse.setId(getNodeId(request));

		return ellipse;
	}
	/**
	 * Gets the id of a node
	 * @param request
	 * @return
	 */
	private static String getNodeId(String request) {
		String splitRequest[] = request.split("node");
		return (splitRequest.length > 1) ? splitRequest[1].replaceAll("\\(", "").replaceAll("\\)", "") : null;

	}
	/**
	 * Parses a line to get the coordinates 
	 * @param request
	 * @param keyword
	 * @param shape
	 */
	private static void parseCoordinates(String request, String keyword, CustomShape shape) {

		String[] splittedRequest = request.split("\\)");

		String startCoordinatesString = splittedRequest[0].replaceAll("\\(", "");
		String[] startCoords = startCoordinatesString.split(",");

		String endCoordinatesString = splittedRequest[1].split("\\(")[1];
		String[] endCoords = endCoordinatesString.split(keyword);

		Double[] coords = new Double[] { 0.0, 0.0, 0.0, 0.0 };

		if (startCoords.length > 1) {
			coords[0] = Double.parseDouble(startCoords[0])*CustomShape.XSCALE;
			coords[1] = Double.parseDouble(startCoords[1])*CustomShape.YSCALE;
		} else {
			shape.setStartId(startCoordinatesString);
		}

		if (endCoords.length > 1) {
			coords[2] = Double.parseDouble(endCoords[0])*CustomShape.XSCALE;
			coords[3] = Double.parseDouble(endCoords[1])*CustomShape.YSCALE;
		} else {
			shape.setEndId(endCoordinatesString);
		}
		shape.setCoords(coords);

	}

	/**
	 * Parses a line into a link
	 * 
	 * @param request
	 * @param options
	 * @return
	 */
	private static CustomShape parseLink(String request, String options) {

		Line newLine = new Line();
		newLine.setStroke(ColorStyle.findColorByName("black"));
		newLine.setFill(null);
		parseOptions(newLine, options);

		CustomShape newLink = new CustomLine(newLine);
		parseCoordinates(request, ",", newLink);
		newLink.setId(getNodeId(request));
		return newLink;
	}

	/**
	 * Parses a line into a polygon
	 * 
	 * @param request
	 * @param options
	 * @return
	 */
	private static CustomShape parsePolygon(String request, String options) {
		String[] coordinateStrings = request.split("--");
		for (int coordIndex = 0; coordIndex < coordinateStrings.length; coordIndex++) {
			coordinateStrings[coordIndex] = coordinateStrings[coordIndex].replaceAll("\\(", "");
			coordinateStrings[coordIndex] = coordinateStrings[coordIndex].replaceAll("\\)", "");
		}

		int count = coordinateStrings.length - 1;
		Double[] coords = new Double[count * 2];
		for (int index = 0; index < count; index++) {
			String[] coordinates = coordinateStrings[index].split(",");
			coords[2 * index] = Double.parseDouble(coordinates[0])*CustomShape.XSCALE;
			coords[2 * index + 1] = Double.parseDouble(coordinates[1])*CustomShape.YSCALE;
		}

		Polygon polygon = new Polygon();
		polygon.setStroke(ColorStyle.findColorByName("black"));
		polygon.setFill(null);
		if (!options.equals("")) {
			parseOptions(polygon, options);
		}
		CustomShape newPolygon = new CustomTriangle(polygon, coords);
		newPolygon.setId(getNodeId(request));
		return newPolygon;
	}

	/**
	 * Parses a line into options
	 * 
	 * @param newShape
	 * @param options
	 */
	private static void parseOptions(Shape newShape, String options) {
		if (options.isEmpty()) {
			return;
		}

		double numLineWidth = 0;
		double currentStroke = 0;
		String[] splittedOptions = options.split(",");
		for (String option : splittedOptions) {
			if (option.equals("dotted")) {
				currentStroke = 2d;
				if (numLineWidth != 0) {
					newShape.getStrokeDashArray().setAll(numLineWidth * currentStroke);
				}
			} else {
				String[] subOption = option.split("=");

				switch (subOption[0]) {
				case "draw":
					newShape.setStroke(ColorStyle.findColorByName(subOption[1]));
					break;
				case "fill":
					newShape.setFill(ColorStyle.findColorByName(subOption[1]));
					break;
				case "linewidth":
					String lineWidth = subOption[1].replaceAll("pt", "");
					numLineWidth = Double.parseDouble(lineWidth);
					newShape.setStrokeWidth(numLineWidth);
					if (currentStroke != 0) {
						newShape.getStrokeDashArray().setAll(numLineWidth * currentStroke);
					}
					break;
				default:
					break;
				}
			}
		}
	}
}

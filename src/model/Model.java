package model;

import javafx.scene.shape.Shape;
import model.shapes.CustomMatrix;
import model.shapes.CustomShape;

import java.util.*;

/**
 * Created by J on 29/02/2016.
 */
public class Model extends Observable {

	private List<CustomShape> components = new ArrayList<>();
	private String filePath;

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Model() {
	}

	/**
	 * Creates a model with the path of the corresponding tikz file
	 * @param path
     */
	public Model(String path) {
		filePath = path;
	}

	/**
	 * Creates a model based on another one
	 * @param model
     */
	public Model(Model model) {
		this(model.filePath);
		for (CustomShape shape : model.components) {
			CustomShape clone = shape.clone();
			this.addComponent(clone, false);
		}
	}

	/**
	 * Creates a model based on his components
	 * @param components
     */
	public Model(List<CustomShape> components) {
		this.components = components;
	}

	/**
	 * Add a new component to the list and perform an update depending on the update parameter
	 * @param component
	 * @param update
     */
	public void addComponent(CustomShape component, Boolean update) {
		component.setModel(this);
		getComponents().add(component);
		if (update) {
			update();
		}
	}

	/**
	 * add a new Component to the list
	 * @param component
     */
	public void addComponent(CustomShape component) {
		addComponent(component, true);
	}

	/**
	 * Remove a component to the list
	 * @param component
     */
	public void removeComponent(CustomShape component) {
		getComponents().remove(component);
		update();
	}

	/**
	 * Remove a shape from the component list
	 * @param shape
     */
	public void removeShape(Shape shape) {
		CustomShape component = findCustomShape(shape);
		getComponents().remove(component);
		update();
	}

	/**
	 * Gets all the components
	 * @return
     */
	public List<CustomShape> getComponents() {
		return components;
	}

	/**
	 * Add a set of components
	 * @param shapes
     */
	public void addComponents(List<CustomShape> shapes) {
		addComponents(shapes, true);
	}

	/**
	 * Add a new component to the list and perform an update depending on the update parameter
	 * @param shapes
	 * @param update
	 */
	public void addComponents(List<CustomShape> shapes, Boolean update) {
		for (CustomShape shape : shapes) {
			shape.setModel(this);
		}
		components.addAll(shapes);
		if (update) {
			update();
		}
	}

	/**
	 * Sets all the components
	 * @param shapes
     */
	public void setComponents(List<CustomShape> shapes) {
		setComponents(shapes, true);
	}
	public void setComponents(List<CustomShape> shapes, Boolean update) {
		for (CustomShape shape : shapes) {
			shape.setModel(this);
		}
		components = shapes;
		if(update)update();
	}

	public void update() {
		setChanged();
		notifyObservers();
	}

	/**
	 * Find the customshape related to the shape paramater
	 * @param drawing
     * @return
     */
	public CustomShape findCustomShape(Shape drawing) {
		for (CustomShape customShape : components) {
			if (customShape.getFigure().equals(drawing)) {
				return customShape;
			}
		}
		return null;
	}

	/**
	 * Gets the file path of the model
	 * @return
     */
	public String getFilePath() {
		return filePath;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof Model)) {
			return false;
		}

		Model model = (Model) object;

		return this.components.equals(model.getComponents()) && filePath.equals(model.filePath);
	}

	@Override
	public int hashCode() {
		int result = components.hashCode();
		result = 31 * result + filePath.hashCode();
		return result;
	}

	/**
	 * Find and gives a customShape from the id
	 * @param id
	 * @return
	 * @throws NoSuchElementException
     */
	public CustomShape getCustomShapeById(String id) throws NoSuchElementException {
		CustomShape result = getCustomShapeById(id, components);
		if (result == null){
			throw new NoSuchElementException();
		}
		return result;
	}

	/**
	 * Retrieve a customShape from the id
	 * @param id
	 * @param components
	 * @return
	 * @throws NoSuchElementException
     */
	public static CustomShape getCustomShapeById(String id, List<CustomShape> components) throws NoSuchElementException {
		CustomShape result = null;
		for (CustomShape shape : components) {
			if (id.equals(shape.getId())) {
				result = shape;
			} else if (shape instanceof CustomMatrix) {
				result = getCustomShapeById(id, ((CustomMatrix) shape).getAllElements());
			}
			if (result != null) {
				break;
			}
		}
		return result;
	}

	public void addObservers(List<Observer> observers) {
		for (Observer obs : observers) {
			this.addObserver(obs);
		}

	}
}

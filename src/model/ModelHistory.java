package model;

import model.services.Server;

import java.util.ArrayList;
import java.util.Observer;

/**
 * Created by J on 20/04/2016.
 */
public class ModelHistory extends ArrayList<Model> {
	private static final long serialVersionUID = 1L;
	private Model currentModel;
	private int currentModelIndex = -1;
	private Observer obs;
	private boolean comeFromUndo = false;

	/**
	 * Tells whether or not the signal comes from undo
	 * @return
     */
	public boolean getComeFromUndo() {
		return comeFromUndo;
	}

	/**
	 * Defines the state of the
	 * @param comeFromUndo
     */
	public void setComeFromUndo(boolean comeFromUndo) {
		this.comeFromUndo = comeFromUndo;
	}

	/**
	 * performs the redo action and move forward
	 */
	public void redo() {
		comeFromUndo = true;
		if (currentModelIndex < this.size()) {
			currentModelIndex++;
		}
		moveToNewModel();
	}

	/**
	 * Tells if there is a previous state to the current state
	 * @return
     */
	public boolean hasPreviousModel() {
		return (currentModelIndex > 0);
	}

	/**
	 * Tells if there is a next state to the current state
	 * @return
     */
	public boolean hasNextModel() {
		return (currentModelIndex < size() - 1);
	}

	/**
	 * Tells if the current model is set
	 * @return
     */
	public boolean hasCurrentModel() {
		return (currentModel != null);
	}


	public ModelHistory(Observer obs) {
		this.obs = obs;
	}

	/**
	 * performs the redo action and move backward
	 */
	public void undo() {
		comeFromUndo = true;
		if (currentModelIndex > 0) {
			currentModelIndex--;
		}
		moveToNewModel();
	}

	/**
	 * Updates the current model
	 */
	public void updateCurrentModel() {
		currentModel.update();
	}

	/**
	 * Sets the next state of the models
	 */
	private void moveToNewModel() {
		if (currentModel != null) {
			currentModel.deleteObservers();
		}

		currentModel = new Model(this.get(currentModelIndex));
		currentModel.addObserver(obs);
		currentModel.addObserver(Server.getCurrentProject());

		updateCurrentModel();
	}


	/**
	 * Gets the current model
	 * @return
     */
	public Model getCurrentModel() {
		return currentModel;
	}

	/**
	 * Gets the previous models
	 * @return
     */
	public Model getPreviousModel() {
		return get(currentModelIndex);
	}

	/**
	 * Add a new model
	 * @param model
     */
	public void addNewModel(Model model) {
		this.removeRange(currentModelIndex + 1, size());

		this.add(model);
		currentModelIndex = this.indexOf(model);
		moveToNewModel();
	}

}

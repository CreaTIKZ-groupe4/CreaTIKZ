package model.styles;

import java.util.HashMap;
import java.util.Map;

public enum ThicknessStyle {
	ULTRATHIN(1, "ultra thin"), 
	VERYTHIN(2, "very thin"), 
	THIN(4, "thin"), 
	SEMITHICK(6, "semi thick"), 
	THICK(8, "thick"), 
	VERYTHICK(12, "very thick"), 
	ULTRATHICK(16, "ultra thick");

	private final Double thickness;
	public final String name;

	private static final Map<Double, ThicknessStyle> doubleMap = new HashMap<Double, ThicknessStyle>();
	static {
		for (ThicknessStyle thicknessStyle : ThicknessStyle.values()) {
			doubleMap.put(thicknessStyle.getDoubleValue(), thicknessStyle);
		}
	}

	ThicknessStyle(double thickness, String name) {
		this.thickness = thickness;
		this.name = name;
	}

	public double getDoubleValue() {
		return thickness;
	}

	public static ThicknessStyle doubleToThickness(Double thickness) {
		return doubleMap.getOrDefault(thickness, THIN);
	}

	@Override
	public String toString() {
		return name;
	}
}

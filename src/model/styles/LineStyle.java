package model.styles;

public enum LineStyle {
	dotted, normal;
	public final static double DEFAULT_DASH_SIZE = 2d;
	public final static double SOLID = 0;
}

package model.styles;

import model.shapes.CustomEllipse;
import model.shapes.CustomLine;
import model.shapes.CustomRectangle;
import model.shapes.CustomShape;
import model.shapes.CustomTriangle;

public enum ShapeStyle {
	Rectangle("rectangle", CustomRectangle.class), 
	Ellipse("ellipse", CustomEllipse.class), 
	Line("line", CustomLine.class), 
	Link("link", null), 
	Triangle("triangle", CustomTriangle.class), 
	None("none", null);

	private final String name; 
	private final Class<? extends CustomShape> customClass;

	ShapeStyle(String name, Class<? extends CustomShape> customClass) {
		this.name = name;
		this.customClass = customClass;
	}

	public Class<? extends CustomShape> getCustomClass() {
		return customClass;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}

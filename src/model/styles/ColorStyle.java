package model.styles;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.paint.Color;

public enum ColorStyle {
	black(Color.BLACK, "black"), 
	white(Color.WHITE, "white"), 
	red(Color.RED, "red"), 
	green(Color.GREEN, "green"), 
	blue(Color.BLUE, "blue"), 
	cyan(Color.CYAN, "cyan"), 
	magenta(Color.MAGENTA, "magenta"), 
	yellow(Color.YELLOW,"yellow"), 
	gray(Color.GRAY, "gray"), 
	darkgray(Color.DARKGRAY, "darkgray"), 
	darkblue(Color.DARKBLUE, "darkblue"), 
	lightgray(Color.LIGHTGRAY, "lightgray"), 
	brown(Color.rgb(150, 75, 0), "brown"), 
	lime(Color.rgb(191, 255, 0), "lime"), 
	olive(Color.rgb(128, 128, 0), "olive"), 
	orange(Color.ORANGE, "orange"), 
	pink(Color.PINK, "pink"), 
	purple( Color.rgb(128, 0, 128), "purple"), 
	teal(Color.rgb(0, 128, 128), "teal"), 
	violet( Color.rgb(238, 130, 238), "violet"), 
	empty(null, "empty");

	private final Color value;
	private final String name;

	private static final Map<Color, ColorStyle> colorMap = new HashMap<Color, ColorStyle>();
	static {
		for (ColorStyle colorStyle : ColorStyle.values()) {
			colorMap.put(colorStyle.getValue(), colorStyle);
		}
	}

	private static final Map<String, ColorStyle> stringMap = new HashMap<String, ColorStyle>();
	static {
		for (ColorStyle colorStyle : ColorStyle.values()) {
			stringMap.put(colorStyle.toString(), colorStyle);
		}
	}

	ColorStyle(Color value, String name) {
		this.value = value;
		this.name = name;
	}

	public Color getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static Color findColorByName(String colorString) {
		return stringMap.getOrDefault(colorString, empty).getValue();
	}

	public static ColorStyle findColorStyle(Color color) {
		return colorMap.getOrDefault(color, empty);
	}

	@Override
	public String toString() {
		return name;
	}
}

package model;

import model.services.Server;
import org.json.JSONObject;
import util.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class Project implements Observer {
	public final static String TIKZ_EXTENSION = ".tikz";
	public final static String MAIN_FILE = "main" + TIKZ_EXTENSION;
	public final static String PROJECT_BASE = System.getProperty("user.home") + "/CreaTIKZ/";
	public final static String INFO_FILE = ".info";

	private int prj_id;
	private int prj_owner_id;
	private String prj_owner_name;
	private Date prj_timestamp;
	private String prj_name;
	private String prj_data = "";
	private String prj_origin = "";
	private String prj_timestamp_string = "";

	public Project() {
		setPrj_timestamp_now();
	}

	/**
	 * Creates a project with his name
	 * @param prj_name
     */
	public Project(String prj_name) {
		this();
		this.prj_owner_id = Server.getActiveUser().getId();
		this.prj_owner_name = Server.getActiveUser().getUsername();
		this.prj_name = prj_name;
		try {
			readFromDisk();
		} catch (IOException e) {
		}
	}

	/**
	 * Creates a new project
	 * @param file
	 * @param name
	 * @throws IOException
     */
	public Project(File file, String name) throws IOException {
		this(name);
		if (file.getAbsolutePath().endsWith(TIKZ_EXTENSION)) {
			readTikzFromFile(file);
		} else {
			readFromDisk();
		}
		saveToDisk();
	}

	/**
	 * Creates  a project with an existing tikz File
	 * @param tikzFile
	 * @throws IOException
     */
	public Project(File tikzFile) throws IOException {
		this(tikzFile, tikzFile.isDirectory() ? tikzFile.getName() : tikzFile.getParentFile().getName());
	}

	/**
	 * Creates a project
	 * @param projectName
	 * @return
	 * @throws Exception
     */
	public static Project createNewProject(String projectName) throws Exception {
		Project project = new Project(projectName);
		File projectFolder = new File(project.getBaseFolder());
		if(projectFolder.exists()) {
			throw new Exception("A project with this name already exists");
		} else {
			projectFolder.mkdirs();
		}
		if(!Config.offlineMode()) {
			project = Server.createProject(project);
		}
		project.saveToDisk();
		
		return project;
	}

	/**
	 * Creates the model based on the project data
	 * @return
	 * @throws ParseException
     */
	public Model buildModel() throws ParseException {
		Model model = new Model(getBaseFolder() + MAIN_FILE);
		TikzParser.parse(prj_data, model);
		return model;
	}

	/**
	 * Gets the owner's if
	 * @return
     */
	public int getPrj_owner_id() {
		return prj_owner_id;
	}

	/**
	 * Set the owner'id
	 * @param prj_owner_id
     */
	public void setPrj_owner_id(int prj_owner_id) {
		this.prj_owner_id = prj_owner_id;
	}

	/**
	 * Gets the owner's name
	 * @return
     */
	public String getPrj_owner_name() {
		return prj_owner_name;
	}

	/**
	 * Gets the owner's name
	 * @param prj_owner_name
     */
	public void setPrj_owner_name(String prj_owner_name) {
		this.prj_owner_name = prj_owner_name;
	}

	/**
	 * Gets the id of the project
	 * @return
     */
	public int getPrj_id() {
		return prj_id;
	}

	/**
	 * Sets the project's
	 * @param prj_id
     */
	public void setPrj_id(int prj_id) {
		this.prj_id = prj_id;
	}

	/**
	 * Gets the project's name
	 * @return
     */
	public String getPrj_name() {
		return prj_name;
	}

	/**
	 * Sets the project's name
	 * @param prj_name
     */
	public void setPrj_name(String prj_name) {
		this.prj_name = prj_name;
	}

	/**
	 * Gets the project's tikz code
	 * @return
     */
	public String getPrj_data() {
		return prj_data;
	}

	/**
	 * Sets prj_data
	 * @param prj_data the related tikz code
     */
	public void setPrj_data(String prj_data) {
		if (StringUtils.isNullOrEmpty(prj_origin)) {
			prj_origin = prj_data;
		}
		this.prj_data = prj_data;
	}

	/**
	 * Gets the project's origin
	 * @param prj_origin
     */
	public void setPrj_origin(String prj_origin) {
		this.setPrj_timestamp_now();
		this.prj_origin = prj_origin;
	}

	/**
	 * Sets the project's origin
	 * @return prj_origin
	 */
	public String getPrj_origin() {
		return prj_origin;
	}

	@Override
	public String toString() {
		return "Project [prj_id=" + prj_id + ", prj_owner_id=" + prj_owner_id + ", prj_owner_name=" + prj_owner_name
				+ ", prj_name=" + prj_name + ", prj_data=" + prj_data + ", prj_origin=" + prj_origin + "]";
	}

	/**
	 * Gives a Json version of the project
	 * @return
     */
	public String toJSON() {
		JSONObject jsonObject = new JSONObject(this);
		return jsonObject.toString();
	}

	/**
	 * Save the project
	 * @throws IOException
     */
	public void saveToDisk() throws IOException {
		File folder = new File(getBaseFolder());
		folder.mkdirs();

		writeTikzFile();
		writeInfoFile();
	
	}

	/**
	 * Writes the tikz code in the  corresponding file
	 * @throws IOException
     */
	public void writeTikzFile() throws IOException {
		String tikzFilePath = getBaseFolder() + MAIN_FILE;
		File tikzFile = new File(tikzFilePath);
	
		if(!tikzFile.exists() || tikzFile.lastModified()<this.prj_timestamp.getTime()){
		Files.write(tikzFile.toPath(), prj_data.getBytes());
		}
	}

	/**
	 * Sets the info file
	 * @throws IOException
     */
	public void writeInfoFile() throws IOException {
		File info = new File(getBaseFolder() + INFO_FILE);
		
		Files.write(info.toPath(), toInfoString().getBytes());
	}

	/**
	 * Gets the base folder
	 * @return
     */
	public String getBaseFolder() {
		return PROJECT_BASE + Server.getActiveUser().getUsername() + "/" + getPrj_name() + "/";
	}

	/**
	 * Read the tikz and info files from the disk
	 * @throws IOException
     */
	public void readFromDisk() throws IOException {

		File tikzFile = new File(getBaseFolder() + MAIN_FILE);
		readTikzFromFile(tikzFile);
		readInfoFromDisk();
	}

	/**
	 * Read the tikz file
	 * @param file
	 * @throws IOException
     */
	public void readTikzFromFile(File file) throws IOException {
		setPrj_data(StringUtils.readStringFromFile(file));
	}

	/**
	 * Read the info file
	 * @throws IOException
     */
	public void readInfoFromDisk() throws IOException {
		File infoFile = new File(getBaseFolder() + INFO_FILE);
		String infoString = StringUtils.readStringFromFile(infoFile);
		try {
			setInfoFromString(infoString);
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new IOException("Info file content incomplete");
		} catch (NumberFormatException | ParseException ex) {
			throw new IOException("Info file content invalid");
		}
	}

	/**
	 * Set all the info parameters from a string
	 * @param info
	 * @throws ArrayIndexOutOfBoundsException
	 * @throws NumberFormatException
	 * @throws ParseException
     */
	private void setInfoFromString(String info)
			throws ArrayIndexOutOfBoundsException, NumberFormatException, ParseException {
		String[] lines = info.split(System.getProperty("line.separator"));
		prj_id = Integer.parseInt(lines[0]);
		prj_name = lines[1];
		prj_owner_name = lines[2];
		setPrj_timestamp_string(lines[3]);
		prj_owner_id = Integer.parseInt(lines[4]);
	}

	/**
	 * Gives a String version of the project's info
	 * @return
     */
	public String toInfoString() {
		String returnValue = prj_id + System.lineSeparator();
		returnValue += prj_name + System.lineSeparator();
		returnValue += prj_owner_name + System.lineSeparator();
		returnValue += getPrj_timestamp_string()+ System.lineSeparator();
		returnValue += prj_owner_id + System.lineSeparator();

		return returnValue;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof Project)) {
			return false;
		}
		Project other = (Project) object;
		return this.toJSON().equals(other.toJSON());
	}

	@Override
	public int hashCode() {
		return this.toJSON().hashCode();
	}

	@Override
	public void update(Observable obs, Object arg) {
		Model model = (Model) obs;
		setPrj_data(TikzGenerator.convertToTikz(model));
		try {
			setPrj_timestamp_now();
			writeTikzFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Move the project to a new destination
	 * @param destinationName
	 * @throws Exception
     */
	public void move(String destinationName) throws Exception {
		String source = this.getBaseFolder();
		this.setPrj_name(destinationName);
		Files.move(Paths.get(source), Paths.get(this.getBaseFolder()));
		saveToDisk();
		Server.renameProject(this);
	}

	/**
	 * Copy the project
	 * @param destination
	 * @return
	 * @throws Exception
     */
	public Project copy(String destination) throws Exception {
		String source = this.getBaseFolder();
		Project copy = new Project(destination);
		CustomUtil.recursiveCopy(new File(source),new File(copy.getBaseFolder()));
		copy.readFromDisk();
		copy.setPrj_name(destination);		
		copy = Server.createProject(copy);	//Create copy on server
		copy.writeInfoFile();
		return copy;
	}

	/**
	 * Delete the project
	 * @throws Exception
     */
	public void delete() throws Exception {
		CustomUtil.deleteFolderOrFile(new File(getBaseFolder()));
		Server.deleteProject(this);
	}

	/**
	 *
	 * @throws IOException
     */
	public void deleteFromDisk() throws IOException {
		String source = this.getBaseFolder();
		Files.delete(Paths.get(source));
	}

	/**
	 * Gets the date of the last modification
	 * @return
     */
	public Date getPrj_timestamp() {
		return prj_timestamp;
	}

	/**
	 * Sets the date of the last modification
	 * @param prj_timestamp
	 */
	public void setPrj_timestamp(Date prj_timestamp) {
		this.prj_timestamp = prj_timestamp;
	}
	/**
	 * Sets the date of the last modification to right now
	 * @param prj_timestamp
	 */
	public void setPrj_timestamp_now() {
		Calendar cal = Calendar.getInstance();
		this.prj_timestamp = cal.getTime();
		this.prj_timestamp_string = new SimpleDateFormat("yyyyMMddHHmmss").format(this.prj_timestamp);
	}
	/**
	 *
	 * @return String representation of this project's lastUpdate yyyyMMddHHmmss format
	 */
	public String getPrj_timestamp_string() {
		return prj_timestamp_string;
	}

	/**
	 *
	 * @param prj_timestamp_string String representation of this project's lastUpdate yyyyMMddHHmmss format expected
	 * @throws ParseException
	 */
	public void setPrj_timestamp_string(String prj_timestamp_string) throws ParseException {
		this.prj_timestamp_string = prj_timestamp_string;
		this.prj_timestamp = new SimpleDateFormat("yyyyMMddHHmmss").parse(prj_timestamp_string);
	}


}

package model.shapes;

import javafx.scene.shape.Line;

import java.util.NoSuchElementException;

public class CustomLine extends CustomShape {

	public CustomLine() {
		this(0d, 0d, 0d, 0d);
	}

	public CustomLine(Double... coords) {
		this(new Line(), coords);
	}

	public CustomLine(Line line, Double... coords) {
		this.figure = line;
		setCoords(coords);
	}

	public CustomLine(Line line) {
		this(line, line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY());
	}

	/**
	 * if the figure is a link, update the start coordinates according to the linked figure
	 */
	public void updateStartCoordinates() {
		CustomShape startShape = model.getCustomShapeById(startId);
		coords[0] = startShape.getAnchorX();
		coords[1] = startShape.getAnchorY();
	}

	/**
	 * if the figure is a link, update the end coordinates according to the linked figure
	 */
	public void updateEndCoordinates() {
		CustomShape endShape = model.getCustomShapeById(endId);
		coords[2] = endShape.getAnchorX();
		coords[3] = endShape.getAnchorY();
	}

	@Override
	public void updateFigureCoordinates() {

		if (this.figure == null) {
			this.figure = new Line();
		}
		Line line = (Line) figure;
		try {
			updateStartCoordinates();
			updateEndCoordinates();
		} catch (NullPointerException | NoSuchElementException ex) {
		}

		line.setStartX(coords[0]);
		line.setStartY(coords[1]);
		line.setEndX(coords[2]);
		line.setEndY(coords[3]);
	}

	@Override
	public CustomShape clone() {
		CustomLine line = new CustomLine(this.coords.clone());
		line.setStartId(startId);
		line.setEndId(endId);
		line.setId(getId());
		line.setLabel(getLabel());
		line.copyStyleFrom(this);
		return line;
	}

	@Override
	public String generateTikz(String options) {
		String startCoord = (startId != null && !startId.isEmpty()) ? startId : coords[0]/XSCALE + "," + coords[1]/YSCALE;
		String endCoord = (endId != null && !endId.isEmpty()) ? endId : coords[2]/XSCALE + "," + coords[3]/YSCALE;

		return "\\draw" + options + " (" + startCoord + ") -- (" + endCoord + ")" + generateIdString("midway")
				+ System.lineSeparator();
	}

	@Override
	public Double getAnchorX() {
		return (coords[0] + coords[2]) / 2;
	}

	@Override
	public Double getAnchorY() {
		return (coords[1] + coords[3]) / 2;
	}

	@Override
	public void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY) {
		setCoords(startingPointX, startingPointY, endingPointX, endingPointY);

	}

	@Override
	public Double getWidth() {
		return coords[2] - coords[0];
	}

	@Override
	public Double getHeight() {
		return coords[3] - coords[1];
	}
	
	@Override
	public Double getStartX() {
		return coords[0];
	}

	@Override
	public Double getStartY() {
		return coords[1];
	}

}

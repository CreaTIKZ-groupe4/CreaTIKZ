package model.shapes;

import javafx.scene.shape.Ellipse;

public class CustomEllipse extends CustomShape {

	public CustomEllipse() {
		this(0d, 0d, 0d, 0d);
	}

	/**
	 * Create a CustomEllipse based on the coordinates
	 * @param coords
     */
	public CustomEllipse(Double... coords) {
		setCoords(coords);
	}

	/**
	 * Create a CustomEllispe based on an existing Ellipse
	 * @param ellipse
     */
	public CustomEllipse(Ellipse ellipse) {
		this(ellipse, ellipse.getCenterX(), ellipse.getCenterY(), ellipse.getRadiusX(), ellipse.getRadiusY());
	}

	/**
	 * Create a CustomEllipse based on an existing Ellipse and the coordinates
	 * @param ellipse
	 * @param coords
     */
	public CustomEllipse(Ellipse ellipse, Double... coords) {
		this.figure = ellipse;
		setCoords(coords);
	}

	@Override
	public CustomShape clone() {
		CustomEllipse ellipse = new CustomEllipse(this.coords.clone());
		ellipse.setId(getId());
		ellipse.copyStyleFrom(this);
		ellipse.setLabel(getLabel());
		return ellipse;
	}

	@Override
	public void updateFigureCoordinates() {
		if (this.figure == null) {
			this.figure = new Ellipse();
		}
		Ellipse ellipse = (Ellipse) this.figure;
		ellipse.setCenterX(coords[0]);
		ellipse.setCenterY(coords[1]);
		ellipse.setRadiusX(coords[2]);
		ellipse.setRadiusY(coords[3]);
	}

	@Override
	public void translate(double offsetX, double offsetY) {
		this.coords[0] += offsetX;
		this.coords[1] += offsetY;
		updateFigureCoordinates();
	}

	@Override
	public String generateTikz(String options) {
		return "\\draw" + options + " (" + coords[0]/XSCALE + "," + coords[1]/YSCALE + ") ellipse (" + coords[2]/XSCALE + " and " + coords[3]/YSCALE
				+ ")" + generateIdString("") + System.lineSeparator();
	}

	@Override
	public Double getAnchorX() {
		return coords[0];
	}

	@Override
	public Double getAnchorY() {
		return coords[1];
	}

	@Override
	public void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY) {
		double w = Math.abs(endingPointX - startingPointX);
		double h = Math.abs(endingPointY - startingPointY);

		Double[] coords = new Double[] { Math.min(startingPointX, endingPointX) + w / 2,
				Math.min(startingPointY, endingPointY) + h / 2, w / 2, h / 2 };
		setCoords(coords);
	}

	@Override
	public Double getWidth() {
		return coords[2] * 2;
	}

	@Override
	public Double getHeight() {
		return coords[3] * 2;
	}

}

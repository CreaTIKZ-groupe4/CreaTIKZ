package model.shapes;

import java.util.ArrayList;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Line;
import javafx.scene.shape.Ellipse;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.styles.ColorStyle;

public class CustomMindMap extends CustomNode {
	private Color conceptColor;// = Color.WHITE;
	private Color textColor;// = Color.BLACK;
	protected Integer clockWise;
	private CustomMindMap centralNode = null;

	private double nodeWidth = 50.;
	private double nodeHeight = 50.;
	private double radius = 200.;
	private Integer intervalAngle = 60;

	public CustomMindMap() {
		this(0.,0.);
	}

	public CustomMindMap(Double... coords) {
		this.coords = coords;
		updateFigureCoordinates();
	}

	public CustomMindMap(Color conceptColor, Color textColor, Integer clockWise, String label) {
		this(conceptColor,textColor,clockWise,label,0.,0.);
	}

	public CustomMindMap(Color conceptColor, Color textColor, Integer clockWise, String label, Double... coords) {
		this(coords);
		this.conceptColor = conceptColor;
		this.textColor = textColor;
		this.clockWise = clockWise;
		this.label = label;
	}

	/**
	 * Add a sub-node to this node, initialising it with this node's parameters
	 * 
	 * @param child
	 *            the node to be added
	 */
	public void addChild(CustomMindMap child) {
		children.add(child);
		child.setRadius(radius * 0.75);
		child.propagateLevel(level + 1);
		child.setChildNumber(children.size());
		child.setNodeWidth(nodeWidth * 0.75);
		child.setNodeHeight(nodeHeight * 0.75);
		child.setCentralNode(this);
		child.setCoords(this.coords);
		updateIntervalAngle();
	}

	private void updateIntervalAngle() {
		int childMod = (children.size() - 1 < 0 ? 0 : children.size() - 1);
		if (childMod < 5) {
			intervalAngle = 60;
		} else {
			childMod = children.size() - 3;
			intervalAngle = (60 * (int) Math.round(Math.pow(0.75, childMod)));
		}
	}

	@Override
	public CustomShape clone() {
		CustomMindMap node = new CustomMindMap(coords);
		node.setLabel(getLabel());
		node.copyStyleFrom(this);
		node.setLevel(this.level);
		node.conceptColor = this.conceptColor;
		node.textColor = this.textColor;
		node.clockWise = this.clockWise;
		node.setModel(this.model);
		for (CustomNode child : children) {
			node.addChild((CustomMindMap) child.clone());
		}
		return node;
	}

	@Override
	public void updateFigureCoordinates() {
		if (figure == null) {
			figure = new Ellipse();
		}
		Ellipse ell = (Ellipse) figure;
		ell.setCenterX(getAnchorX());
		ell.setCenterY(getAnchorY());
		ell.setRadiusX(nodeWidth);
		ell.setRadiusY(nodeHeight);
		ell.setStroke(conceptColor);
		ell.setFill(conceptColor);
		try {
			for (CustomNode child : children) {
				((CustomMindMap) child).updateFigureCoordinates();
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ArrayList<Shape> getAllFigures() {
		ArrayList<Shape> returnValue = new ArrayList<Shape>();

		Text label = new Text();
		label.setText(getLabel());
		label.setX(getAnchorX());
		label.setY(getAnchorY());
		label.setLayoutX(-label.getLayoutBounds().getWidth() / 2);
		label.setFill(textColor != null ? textColor : Color.BLACK);

		for (CustomNode child : children) {
			Line line = new Line();
			line.setStartX(this.getAnchorX());
			line.setEndX(child.getAnchorX());
			line.setStartY(this.getAnchorY());
			line.setEndY(child.getAnchorY());
			line.setFill(figure.getFill());
			returnValue.add(line);
			returnValue.addAll(child.getAllFigures());
		}
		returnValue.add(getFigure());
		returnValue.add(label);
		return returnValue;
	}

	public Integer getChildAngle() {
		return centralNode.getClockWise() - ((childNumber - 1) * centralNode.intervalAngle);
	}

	public double getNodeWidth() {
		return nodeWidth;
	}

	public void setNodeWidth(double nodeWidth) {
		this.nodeWidth = nodeWidth;
	}

	public double getNodeHeight() {
		return nodeHeight;
	}

	public void setNodeHeight(double nodeHeight) {
		this.nodeHeight = nodeHeight;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Color getConceptColor() {
		return conceptColor;
	}

	public void setConceptColor(Color conceptColor) {
		this.conceptColor = conceptColor;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	public CustomMindMap getCentralNode() {
		return centralNode;
	}

	public void setCentralNode(CustomMindMap centralNode) {
		this.centralNode = centralNode;
	}

	public int getClockWise() {
		return clockWise != null ? clockWise : 0;
	}

	public void setClockWise(int clockWise) {
		this.clockWise = clockWise;
	}

	@Override
	public String generateTikz() {
		String tikzRepr = tikzRepr();
		tikzRepr = tikzRepr + ";";

		return "\\path[mindmap,concept color=" + ColorStyle.findColorStyle(conceptColor) + ",text="
				+ ColorStyle.findColorStyle(textColor) + "]" + tikzRepr + System.lineSeparator();
	}

	@Override
	public String tikzRepr() {

		String at = centralNode == null ? "at ("+getAnchorX()/XSCALE +","+getAnchorY()/YSCALE +") " : "";
		String repr = "node "+at+" [concept] {" + label + "}"
				+ (clockWise != null ? "[clockwise from=" + clockWise + "]" : "");
		for (CustomNode childNode : children) {
			CustomMindMap child = (CustomMindMap)childNode;
			String whitespaces = new String(new char[level]).replace("\0", "  ");
			String colorString = child.getConceptColor() != null ? " color=" + ColorStyle.findColorStyle(((CustomMindMap) child).getConceptColor()) :"";
			String textColorString = child.getTextColor() != null ? "text="+ColorStyle.findColorStyle(child.getTextColor()) : "";
			if(!textColorString.isEmpty()) {
				colorString+= ",";
			}
			repr += System.lineSeparator() + whitespaces + "child [concept"+colorString+ textColorString+ "] { "
					+ ((CustomMindMap) child).tikzRepr() + " }";
		}
		return repr;
	}

	@Override
	public Double getAnchorX() {
		Double drawx = coords[0];
		if (centralNode != null) {
			drawx = centralNode.getAnchorX();
			double nodeStartAngleRadians = Math.toRadians(centralNode.getClockWise());
			double intervalAngleRadians = Math.toRadians(centralNode.intervalAngle);
			double modx = Math.cos(nodeStartAngleRadians - ((childNumber - 1) * intervalAngleRadians));
			drawx += radius * modx;

		}
		return drawx;
	}

	@Override
	public Double getAnchorY() {
		Double drawy = coords[1];
		if (centralNode != null) {
			drawy = centralNode.getAnchorY();
			double nodeStartAngleRadians = Math.toRadians(centralNode.getClockWise());
			double intervalAngleRadians = Math.toRadians(centralNode.intervalAngle);
			double mody = Math.sin(nodeStartAngleRadians - ((childNumber - 1) * intervalAngleRadians));
			drawy -= radius * mody;

		}
		return drawy;
	}

}

package model.shapes;

import java.util.Random;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import model.Model;
import model.styles.ColorStyle;
import model.styles.LineStyle;
import model.styles.ThicknessStyle;

/**
 * etend la classe Shape mais contient d'autres param�tres Created by J on
 * 29/02/2016.
 */
public abstract class CustomShape {

	public final static int YSCALE = -40;
	public final static int XSCALE = 40;

	private final static Random generator = new Random();
	protected Shape figure;
	protected Double[] coords = {0.,0.,0.,0.};
	protected String id;
	protected String startId;
	protected String endId;
	protected Model model;
	String label;

	@Override
	public abstract CustomShape clone();

	public void copyStyleFrom(CustomShape destination) {
		this.figure.setStroke(destination.figure.getStroke());
		this.figure.setStrokeWidth(destination.figure.getStrokeWidth());
		this.figure.setFill(destination.figure.getFill());
		this.figure.getStrokeDashArray().setAll(destination.figure.getStrokeDashArray());
	}

	/**
	 * Set the  figure's stroke color
	 * @param color
     */
	public void setStrokeColor(ColorStyle color) {
		this.figure.setStroke(color.getValue());
	}
	/**
	 * Set the  figure's stroke thickness
	 * @param thickness
	 */
	public void setStrokeWidth(ThicknessStyle thickness) {
		this.figure.setStrokeWidth(thickness.getDoubleValue());
	}

	/**
	 * Set the figure's fill color
	 * @param color
     */
	public void setFillColor(ColorStyle color) {
		this.figure.setFill(color.getValue());
	}

	/**
	 * sett the figure's dash value
	 * @param dashed
     */
	public void setDashed(Boolean dashed) {
		double newLineStyle = (dashed)? LineStyle.DEFAULT_DASH_SIZE : LineStyle.SOLID;
		this.figure.setStrokeDashOffset(newLineStyle);
	}

	/**
	 * Get the figure
	 * @return
     */
	public Shape getFigure() {
		return figure;
	}

	/**
	 * Set the figure
	 * @param figure
     */
	public void setFigure(Shape figure) {
		this.figure = figure;
	}

	/**
	 * Get the related figure 's coordinates
	 * @return
     */
	public Double[] getCoords() {
		return coords;
	}

	/**
	 * Set the coordinates
	 * @param coords
     */
	public void setCoords(Double... coords) {
		if (coords != null && coords.length >= 4) {
			this.coords = coords;
			updateFigureCoordinates();
		}
	}

	/**
	 * Update the coordinates
	 */
	public abstract void updateFigureCoordinates();

	/**
	 * Translate the current figure to a new position
	 * @param offsetX
	 * @param offsetY
     */
	public void translate(double offsetX, double offsetY) {
		for (int i = 0; i < this.coords.length; i += 2) {
			this.coords[i] += offsetX;
			this.coords[i + 1] += offsetY;
		}
		updateFigureCoordinates();
	}
	/**
	 * Scale the current figure
	 * @param multiplyX
	 * @param multiplyY
     */
	public void scale(double multiplyX, double multiplyY,Boolean update) {
		for (int i = 0; i < this.coords.length; i += 2) {
			this.coords[i] *= multiplyX;
			this.coords[i + 1] *= multiplyY;
		}
		if(update)updateFigureCoordinates();
	}
	/**
	 * Generate the associated tikz code
	 * @param options
	 * @return
     */
	public abstract String generateTikz(String options);

	/**
	 * Generate an id if the shape doesn't have one
	 * @param anchorPoint
     * @return
     */
	protected String generateIdString(String anchorPoint) {
		return (hasId() || hasLabel()) ? " node(" + getId() + ") [" + anchorPoint + "]{" + getLabel() + "};" : ";";
	}

	/**
	 * Gives a string view of all the options
	 * @return
     */
	public String getOptionString() {
		String options = "[";

		options += "draw=" + ColorStyle.findColorStyle((Color) figure.getStroke());

		if (figure.getFill() != null) {
			options += ", fill=" + ColorStyle.findColorStyle((Color) figure.getFill());
		}

		options += ", line width=" + figure.getStrokeWidth();

		if (figure.getStrokeDashArray().size() != LineStyle.SOLID) {
			options += ", dotted";
		}

		options += "]";
		return options;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!CustomShape.class.isAssignableFrom(obj.getClass())) {
			return false;
		}

		final CustomShape other = (CustomShape) obj;

		return other.toString().equals(this.toString());
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	@Override
	public String toString() {
		return generateTikz(getOptionString());
	}

	/**
	 * Get the id
	 * @return
     */
	public String getId() {
		return hasId() ? id : "";
	}

	public Boolean hasId() {
		return !(id == null || id.isEmpty());
	}

	/**
	 * Set the id
	 * @param id
     */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the endId
	 * @return
     */
	public String getEndId() {
		return endId;
	}

	/**
	 * Set the endId
	 * @param endId
     */
	public void setEndId(String endId) {
		this.endId = endId;
	}

	/**
	 * Get the start Id
	 * @return
     */
	public String getStartId() {
		return startId;
	}

	/**
	 * Set the startId
	 * @param startId
     */
	public void setStartId(String startId) {
		this.startId = startId;
	}

	/**
	 * Get the related model
	 * @return
     */
	public Model getModel() {
		return model;
	}

	/**
	 * Set the model
	 * @param model
     */
	public void setModel(Model model) {
		this.model = model;
	}

	/**
	 * Get the anchor point x coordinate
	 * @return
     */
	public abstract Double getAnchorX();

	/**
	 * 
	 * @return the vertical center of the shape
	 */
	public abstract Double getAnchorY();

	/**
	 * 
	 * @return the width of the shape
	 */
	public abstract Double getWidth();

	/**
	 * 
	 * @return the height of the shape
	 */
	public abstract Double getHeight();

	public String getOrGenerateId() {
		if (!hasId()) {
			this.id = "shape" + generator.nextInt(Integer.MAX_VALUE);
		}
		return this.id;
	}

	/**
	 * Get the label
	 * @return
     */
	public String getLabel() {
		return hasLabel() ? label : "";

	}

	/**
	 * Set the label
	 * @param value
     */
	public void setLabel(String value) {
		this.label = value;
	}

	/**
	 * Check whether or not a label exists
	 * @return
     */
	public boolean hasLabel() {
		return this.label != null && !this.label.isEmpty();
	}

	/**
	 * Set the coordindates according to 2 points
	 * @param startingPointX
	 * @param startingPointY
	 * @param endingPointX
	 * @param endingPointY
     */
	public abstract void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY);

	/**
	 * 
	 * @return the horizontal starting point of the shape
	 */
	public Double getStartX() {
		return getCenterX() - getWidth()/2;
	}

	/**
	 * 
	 * @return the vertical starting point of the shape
	 */
	public Double getStartY() {
		return getCenterY() - getHeight()/2;
	}

	/**
	 * Get the center point x coordinate
	 * @return
     */
	public Double getCenterX() {
		return getAnchorX();
	}

	/**
	 * Get the center point y coordinate
	 * @return
	 */
	public Double getCenterY() {
		return getAnchorY();
	}

}
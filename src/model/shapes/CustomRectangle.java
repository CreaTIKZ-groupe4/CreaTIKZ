package model.shapes;

import javafx.scene.shape.Rectangle;

public class CustomRectangle extends CustomShape {

	public CustomRectangle() {
		this(0d, 0d, 0d, 0d);
	}

	/**
	 * Creates a CustomRectangle based on the coordinates
	 * @param coords
     */
	public CustomRectangle(Double... coords) {
		this(null, coords);
	}

	/**
	 * Creates a CustomRectangle based on an existing rectangle
	 * @param rectangle
     */
	public CustomRectangle(Rectangle rectangle) {
		this(rectangle, rectangle.getX(), rectangle.getY(), rectangle.getX() + rectangle.getWidth(),
				rectangle.getY() + rectangle.getHeight());
	}

	/**
	 * Creates a CustomRectangle based on an existing rectangle and the coordinates
	 * @param rectangle
	 * @param coords
     */
	public CustomRectangle(Rectangle rectangle, Double... coords) {
		this.figure = rectangle;
		setCoords(coords);
	}

	@Override
	public CustomShape clone() {
		CustomRectangle clone = new CustomRectangle(this.coords.clone());
		clone.setId(getId());
		clone.copyStyleFrom(this);
		clone.setLabel(getLabel());
		return clone;
	}

	@Override
	public void updateFigureCoordinates() {
		if (this.figure == null) {
			this.figure = new Rectangle();
		}
		Rectangle rec = (Rectangle) figure;
		rec.setX(coords[0]);
		rec.setY(coords[1]);
		rec.setWidth(coords[2] - coords[0]);
		rec.setHeight(coords[3] - coords[1]);

		if (rec.getWidth() < 0) {
			rec.setWidth(-rec.getWidth());
			rec.setX(rec.getX() - rec.getWidth());
		}
		if (rec.getHeight() < 0) {
			rec.setHeight(-rec.getHeight());
			rec.setY(rec.getY() - rec.getHeight());
		}
	}

	@Override
	public String generateTikz(String options) {
		return "\\draw" + options + " (" + coords[0]/XSCALE + "," + coords[1]/YSCALE + ") rectangle (" + coords[2]/XSCALE + "," + coords[3]/YSCALE
				+ ")" + generateIdString("midway") + System.lineSeparator();
	}

	@Override
	public Double getAnchorX() {
		return (coords[0] + coords[2]) / 2;
	}

	@Override
	public Double getAnchorY() {
		return (coords[1] + coords[3]) / 2;
	}

	@Override
	public void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY) {

		setCoords(startingPointX, startingPointY, endingPointX, endingPointY);

	}

	@Override
	public Double getWidth() {
		return coords[2] - coords[0];
	}

	@Override
	public Double getHeight() {
		return coords[3] - coords[1];
	}
	
	@Override
	public Double getStartX() {
		return coords[0];
	}

	@Override
	public Double getStartY() {
		return coords[1];
	}


}

package model.shapes;

import java.util.ArrayList;

import model.Model;

public class CustomMatrix extends CustomShape {
	private ArrayList<ArrayList<CustomShape>> rows = new ArrayList<ArrayList<CustomShape>>();
	private Double colSep = 1.;
	private Double rowSep = 1.;

	/**
	 * 
	 * @return the matrix rows
	 */
	public ArrayList<ArrayList<CustomShape>> getRows() {
		return rows;
	}
	/**
	 * 
	 * @return all the elements of the matrix
	 */
	public ArrayList<CustomShape> getAllElements() {
		ArrayList<CustomShape> elements = new ArrayList<>();
		for (ArrayList<CustomShape> row : getRows()) {
			elements.addAll(row);
		}
		return elements;
	}


	/**
	 * 
	 * @return the separation between the columns
	 */
	public Double getColSep() {
		return colSep;
	}

	/**
	 * 
	 * @param colSep the new separation between the columns
	 */
	public void setColSep(Double colSep) {
		this.colSep = colSep;
	}

	/**
	 * 
	 * @return the separation between the rows
	 */
	public Double getRowSep() {
		return rowSep;
	}

	/**
	 * 
	 * @param rowSep the new separation between the rows
	 */
	public void setRowSep(Double rowSep) {
		this.rowSep = rowSep;
	}

	/**
	 * 
	 * @param i the index of the column
	 * @return the size of the column
	 */
	public int getColSize(int i) {
		try {
			return rows.get(i).size();
		} catch (NullPointerException | IndexOutOfBoundsException exception) {
			return 0;
		}
	}

	/**
	 * 
	 * @return the number of rows
	 */
	public int getRowSize() {
		return rows.size();
	}

	/**
	 * Add a component in the matrix
	 * @param row the row index
	 * @param col the column index
	 * @param shape the componenent to add
	 */
	public void addComponent(int row, int col, CustomShape shape) {
		while (this.getRowSize() <= row) {
			rows.add(new ArrayList<CustomShape>());
		}
		rows.get(row).add(col, shape);
	}

	@Override
	public CustomShape clone() {
		CustomMatrix matrix = new CustomMatrix();
		matrix.setColSep(getColSep());
		matrix.setRowSep(getRowSep());
		for (int i = 0; i < this.getRowSize(); i++) {
			for (int j = 0; j < rows.get(i).size(); j++) {
				if (get(i, j) != null) {
					matrix.addComponent(i, j, get(i, j).clone());
				}
			}
		}
		return matrix;
	}

	@Override
	public void updateFigureCoordinates() {
		double startX = getStartX();
		double currentY = getStartY();

		for (int i = 0; i < this.getRowSize(); i++) {
			double currentX = startX;
			double rowHeight = getRowHeight(i);
			for (int j = 0; j < this.getColSize(i); j++) {
				CustomShape currentShape = get(i, j);

				double colWidth = getColWidth(j);
				if (currentShape != null) {
					double adjustX = currentX - currentShape.getStartX();
					double adjustY = currentY - currentShape.getStartY();
					currentShape.translate(adjustX, adjustY);
				}

				currentX += colWidth + colSep;
			}
			currentY += rowHeight + rowSep;
		}
	}

	/**
	 * get the component of the matrix
	 * @param i the row index
	 * @param j the column index
	 * @return the component
	 */
	private CustomShape get(int i, int j) {
		CustomShape customShape = null;
		try {
			customShape = rows.get(i).get(j);
		} catch (IndexOutOfBoundsException e) {
		}
		return customShape;
	}

	@Override
	public String generateTikz(String options) {

		String at = " at ("+getAnchorX()/XSCALE +","+getAnchorY()/YSCALE +") ";
		String result = "\\matrix" + this.getOptionString() + " "+at+"{\n";
		for (int i = 0; i < this.getRowSize(); i++) {

			int colSize = this.getColSize(i);
			for (int j = 0; j < colSize; j++) {
				CustomShape currentShape = get(i, j);
				if (currentShape != null) {
					String shapeTikz = currentShape.toString().replace(System.lineSeparator(), "");
					result += shapeTikz;
				}
				result += (j + 1 == colSize) ? " \\\\" : " &";
				result += System.lineSeparator();
			}
		}
		return result + "};\n";
	}

	/**
	 * 
	 * @param col the column index
	 * @return the width of the column 
	 */
	private double getColWidth(int col) {
		double left = Double.MAX_VALUE;
		double right = 0;
		for (int i = 0; i < rows.size(); i++) {
			try {
				left = Math.min(left, get(i, col).getCenterX() - get(i, col).getWidth() / 2);
				right = Math.max(right, get(i, col).getCenterX() + get(i, col).getWidth() / 2);
			} catch (NullPointerException | IndexOutOfBoundsException exception) {
			}
		}

		return right - left;
	}

	/**
	 * 
	 * @param row the row index
	 * @return the height of the row 
	 */
	private double getRowHeight(int row) {
		double bot = Double.MAX_VALUE;
		double top = 0;

		for (int i = 0; i < rows.get(row).size(); i++) {
			try {
				bot = Math.min(bot, get(row, i).getCenterY() - get(row, i).getHeight() / 2);
				top = Math.max(top, get(row, i).getCenterY() + get(row, i).getHeight() / 2);
			} catch (NullPointerException | IndexOutOfBoundsException exception) {
			}
		}
		return top - bot;

	}

	@Override
	public String toString() {
		return generateTikz(this.getOptionString());
	}

	@Override
	public String getOptionString() {
		return "[column sep=" + colSep + "mm, row sep=" + rowSep + "mm]";
	}

	@Override
	public Double getAnchorX() throws NullPointerException {
		return getStartX() + getWidth() / 2;
	}

	@Override
	public Double getAnchorY() throws NullPointerException {
		return getStartY() + getHeight() / 2;
	}

	@Override
	public void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY) {
	}

	@Override
	public Double getWidth() {
		double width = 0;

		for (int i = 0; i < rows.size(); i++) {
			double rowWidth = 0;
			for (int j = 0; j < rows.get(i).size(); j++) {
				rowWidth += this.getColWidth(j);
			}
			width = Math.max(width, rowWidth);
		}
		return width;
	}

	@Override
	public Double getHeight() {
		double height = 0;

		for (int i = 0; i < rows.size(); i++) {
			height += this.getRowHeight(i);
		}
		return height;
	}

	/**
	 * 
	 * @return horizontal start point of the matrix (top left point)
	 * @throws NullPointerException if there is no element in the matrix
	 */
	public Double getStartX() throws NullPointerException {
		return get(0, 0).getStartX();
	}

	/**
	 * 
	 * @return vertical start point of the matrix (top left point)
	 * @throws NullPointerException if there is no element in the matrix
	 */
	public Double getStartY() throws NullPointerException {
		return get(0, 0).getStartY();
	}

	@Override
	public void setModel(Model model) {
		for (ArrayList<CustomShape> row : this.rows) {
			for (CustomShape shape : row) {
				if (shape != null) {
					shape.setModel(model);
				}
			}
		}
	}

}

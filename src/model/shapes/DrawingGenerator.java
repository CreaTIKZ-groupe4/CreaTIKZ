package model.shapes;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import model.DrawOptions;
import model.Model;
import model.styles.ColorStyle;
import model.styles.ThicknessStyle;
import util.TikzParser;

import java.awt.List;
import java.text.ParseException;
import java.util.ArrayList;

public class DrawingGenerator {

	public static final double DEFAULT_CIRCLE_RADIUS = 16;
	public static final double DEFAULT_SHAPE_RADIUS = 64;

	/**
	 * Add a circle on a circle (like a necklace)
	 * @param x Center of the composed shape
	 * @param y Center of the composed shape
	 * @param shapeRadius Radius of the composed shape
	 * @param circleRadius Radius of the circles in the composed shape
	 * @param rotation Starting rotation of the circles
	 * @param itemCount How many circles on the "necklace"
	 * @param model Model in which to add the shapes
     * @param updateModel True will update the model (avoid if you are adding more than one shape)
     * @return List of added circles
     */
	public static ArrayList<CustomShape> addCirclesInCircle(double x, double y, double shapeRadius, double circleRadius,
			double rotation, int itemCount, Model model, Boolean updateModel) {
		ArrayList<CustomShape> circles = new ArrayList<CustomShape>();

		for (int i = 0; i < itemCount; i++) {
			Double[] coords = { x + Math.cos(rotation + Math.PI * 2 / itemCount * i) * shapeRadius,
					y - Math.sin(rotation + Math.PI * 2 / itemCount * i) * shapeRadius, circleRadius, circleRadius };

			CustomShape ellipse = new CustomEllipse(coords);
			ellipse.setFillColor(ColorStyle.white);
			ellipse.setStrokeColor(ColorStyle.black);
			circles.add(ellipse);
		}
		model.addComponents(circles, updateModel);
		return circles;
	}

	/**
	 * Add links as a complete graph to a list of given shapes (does not update)
	 * @param shapesList List of shapes that will get linked together
	 * @param tikzModel
     */
	public static void addLinksCompleteGraph(ArrayList<CustomShape> shapesList, Model tikzModel) {

		for (int i = 0; i < shapesList.size(); i++) {
			for (int j = i + 1; j < shapesList.size(); j++) {
				CustomShape shapeA = shapesList.get(i);
				CustomShape shapeB = shapesList.get(j);

				CustomLine link = new CustomLine();
				link.setStrokeWidth(ThicknessStyle.THIN);
				link.setStartId(shapeA.getOrGenerateId());
				link.setEndId(shapeB.getOrGenerateId());
				tikzModel.addComponent(link, false);
			}
		}
	}
	/**
	 * Create a complete graph with its nodes in circle around a center
	 * @param x Center of the composed shape
	 * @param y Center of the composed shape
	 * @param shapeRadius Radius of the composed shape
	 * @param circleRadius Radius of the circles in the composed shape
	 * @param rotation Starting rotation of the circles
	 * @param nodeCount How many nodes in the graph
	 * @param model
	 * @param updateModel True will update the model (avoid if you are adding more than one shape)
	 */
	public static void addCompleteLinkedGraph(double x, double y, double shapeRadius, double circleRadius,
			double rotation, int nodeCount, Model model, Boolean updateModel) {
		ArrayList<CustomShape> circles = addCirclesInCircle(x, y, shapeRadius, circleRadius, rotation, nodeCount, model,
				false);
		addLinksCompleteGraph(circles, model);

		if (updateModel) {
			model.update();
		}

	}
	/**
	 * Create and adds a complete pentagonal graph to given model
	 * @param x Center of the composed shape
	 * @param y Center of the composed shape
	 * @param tikzModel
	 * @param update True will update the model (avoid if you are adding more than one shape)
	 */
	public static void addPentagon(double x, double y, Model tikzModel, Boolean update) {
		addCompleteLinkedGraph(x, y, DEFAULT_SHAPE_RADIUS, DEFAULT_CIRCLE_RADIUS, Math.PI / 2, 5, tikzModel, update);
	}

	/**
	 * Create and adds a complete diamond-shaped graph to given model
	 * @param x Center of the composed shape
	 * @param y Center of the composed shape
	 * @param tikzModel
	 * @param update True will update the model (avoid if you are adding more than one shape)
	 */
	public static void addDiamond(double x, double y, Model tikzModel, Boolean update) {
		addCompleteLinkedGraph(x, y, DEFAULT_SHAPE_RADIUS, DEFAULT_CIRCLE_RADIUS, Math.PI / 2, 4, tikzModel, update);
	}

	/**
	 * Create and adds an empty circle with smaller circles on it to given model
	 * @param x Center of the composed shape
	 * @param y Center of the composed shape
	 * @param shapeRadius Radius of the composed shape
	 * @param circleRadius Radius of the circles in the composed shape
	 * @param rotation Starting rotation of the circles
	 * @param items How many smaller circles there are
	 * @param tikzModel
	 * @param update True will update the model (avoid if you are adding more than one shape)
	 */
	public static void addCycleDrawing(double x, double y, double shapeRadius, double circleRadius, double rotation,
			int items, Model tikzModel, boolean update) {
		CustomShape newCustomShape = new CustomEllipse(x, y, shapeRadius, shapeRadius);
		newCustomShape.setStrokeColor(ColorStyle.black);
		newCustomShape.setFillColor(ColorStyle.empty);
		newCustomShape.setStrokeWidth(ThicknessStyle.THIN);
		tikzModel.addComponent(newCustomShape, false);
		DrawingGenerator.addCirclesInCircle(x, y, shapeRadius, circleRadius, rotation, items, tikzModel, true);
	}

	/**
	 * Create an adds a matrix with an ellipse, a rectangle, a triangle and a line in it
	 * @param x Starting point of the matrix
	 * @param y Starting point of the matrix
	 * @param tikzModel
	 * @param drawOptions Color and style options for the matrix elements
	 */
	public static void addMatrixDrawing(double x, double y, Model tikzModel, DrawOptions drawOptions) {
		CustomMatrix matrix = new CustomMatrix();
		CustomEllipse ellipse = new CustomEllipse(x, y, 60.0, 60.0);
		CustomRectangle rectangle = new CustomRectangle(x - 50, y - 40, x + 50.0, y + 40.0);
		CustomTriangle triangle = new CustomTriangle(x, y - 30, x - 40, y + 30, x + 40, y + 30);
		CustomLine line = new CustomLine(x - 100, y, x + 100, y);
		drawOptions.applyStyleToShape(ellipse.getFigure());
		drawOptions.applyStyleToShape(rectangle.getFigure());
		drawOptions.applyStyleToShape(triangle.getFigure());
		drawOptions.applyStyleToShape(line.getFigure());
		matrix.addComponent(0, 0, ellipse);
		matrix.addComponent(0, 1, rectangle);
		matrix.addComponent(1, 0, triangle);
		matrix.addComponent(1, 1, line);
		tikzModel.addComponent(matrix, true);
	}
	
	/**
	 * Create and adds a simple three elements tree to the given model
	 * @param x Starting point of the tree
	 * @param y Starting point of the tree
	 * @param tikzModel
	 * @param drawOptions
	 */
	public static void addTreeDrawing(double x, double y, Model tikzModel, DrawOptions drawOptions) {
		CustomNode root = new CustomNode(x,y);
		root.setLabel("father");
		CustomNode node_1 = new CustomNode();
		node_1.setLabel("left");
		CustomNode node_2 = new CustomNode();
		node_2.setLabel("right");
		root.addChild(node_1);
		root.addChild(node_2);
		for (Shape node : root.getAllFigures()) {
			drawOptions.applyStyleToShape(node);
		}
		tikzModel.addComponent(root, true);
	}
	
	/**
	 * Create and adds a mindmap drawing with one center and five nodes to the given model
	 * @param x Starting point of the mindmap
	 * @param y Starting point of the mindmap
	 * @param tikzModel
	 * @param drawOptions
	 */
	public static void addMindMapDrawing(double x, double y, Model tikzModel, DrawOptions drawOptions) {
		CustomMindMap mindmap = new CustomMindMap(Color.YELLOW, Color.WHITE, 0, "Hello!",x,y);
		CustomMindMap node_1 = new CustomMindMap(Color.RED, Color.WHITE, null, "Hello!");
		CustomMindMap node_2 = new CustomMindMap(Color.GREEN, Color.WHITE, null, "Hello!");
		CustomMindMap node_3 = new CustomMindMap(Color.BLUE, Color.WHITE, null, "Hello!");
		CustomMindMap node_4 = new CustomMindMap(Color.GRAY, Color.WHITE, null, "Hello!");
		CustomMindMap node_5 = new CustomMindMap(Color.ORANGE, Color.WHITE, null, "Hello!");
		mindmap.addChild(node_1);
		mindmap.addChild(node_2);
		mindmap.addChild(node_3);
		mindmap.addChild(node_4);
		mindmap.addChild(node_5);
		for (Shape node : mindmap.getAllFigures()) {
			drawOptions.applyStyleToShape(node);
		}
		tikzModel.addComponent(mindmap, true);
	}

	/**
	 * Inserts a cake
	 * @param x
	 * @param y
	 * @param tikzModel
	 */
	public static void addCakeDrawing(double x, double y, Model tikzModel) {
		try {
			String oldcake = "\\draw[draw=brown, fill=orange, line width=4.0] (380.0,452.5) ellipse (140.0 and 51.5);\n\\draw[draw=orange, fill=orange, line width=4.0] (249.0,344.0) rectangle (514.0,457.0);\n\\draw[draw=brown, fill=white, line width=4.0] (381.0,356.5) ellipse (139.0 and 49.5);\n\\draw[draw=cyan, fill=white, line width=1.0] (427.5,351.0) ellipse (16.5 and 12.0);\n\\draw[draw=cyan, fill=white, line width=1.0] (334.5,353.0) ellipse (16.5 and 12.0);\n\\draw[draw=purple, fill=blue, line width=4.0] (325.0,281.0) rectangle (344.0,355.0);\n\\draw[draw=purple, fill=blue, line width=4.0] (418.0,280.0) rectangle (437.0,354.0);\n\\draw[draw=red, fill=red, line width=1.0] (336.0,269.5) ellipse (23.0 and 22.5);\n\\draw[draw=red, fill=red, line width=1.0] (426.0,268.5) ellipse (23.0 and 22.5);\n\\draw[draw=red, fill=red, line width=1.0] (336.0,209.0) -- (314.0,263.0) -- (358.0,263.0) -- cycle;\n\\draw[draw=red, fill=red, line width=1.0] (426.0,209.0) -- (404.0,263.0) -- (448.0,263.0) -- cycle;\n\\draw[draw=red, fill=orange, line width=1.0] (425.0,271.0) ellipse (15.0 and 15.0);\n\\draw[draw=red, fill=orange, line width=1.0] (336.0,271.0) ellipse (15.0 and 15.0);\n\\draw[draw=orange, fill=yellow, line width=1.0] (335.5,271.5) ellipse (8.5 and 8.5);\n\\draw[draw=orange, fill=yellow, line width=1.0] (426.5,273.5) ellipse (8.5 and 8.5);\n\\draw[draw=brown, fill=red, line width=4.0] (376.0,274.0) -- (357.0,322.0) -- (395.0,322.0) -- cycle;\n\\draw[draw=brown, fill=red, line width=4.0] (435.0,342.0) -- (416.0,390.0) -- (454.0,390.0) -- cycle;\n\\draw[draw=brown, fill=red, line width=4.0] (317.0,343.0) -- (298.0,391.0) -- (336.0,391.0) -- cycle;\n\\draw[draw=brown, fill=red, line width=4.0] (475.0,294.0) -- (456.0,342.0) -- (494.0,342.0) -- cycle;\n\\draw[draw=brown, fill=red, line width=4.0] (280.0,296.0) -- (261.0,344.0) -- (299.0,344.0) -- cycle;\n\\draw[draw=brown, fill=brown, line width=1.0] (359.0,422.0) rectangle (368.0,467.0);\n\\draw[draw=brown, fill=brown, line width=1.0] (359.0,458.0) rectangle (406.0,468.0);\n\\draw[draw=brown, fill=brown, line width=1.0] (385.0,426.0) rectangle (397.0,490.0);";
					
			java.util.List<CustomShape> tell = 
			TikzParser.parse(oldcake,
					tikzModel, true, false);
			
			for(CustomShape shape : tell){
				shape.scale(0.01,-0.01,false);
				shape.translate(x-100, y-100);
			}
			tikzModel.update();
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}

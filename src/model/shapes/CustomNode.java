package model.shapes;

import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import model.Model;
import util.Config;

import java.util.ArrayList;

public class CustomNode extends CustomShape {
	public static final Double WIDTH = Double.parseDouble(Config.getProperty("treewidth"));
	private static final Double childrensWidth = 200.;
	private static final Double nodeWidth = 60.;
	private static final Double nodeHeight = 25.;

	
	protected ArrayList<CustomNode> children = new ArrayList<CustomNode>();
	protected int level = 0;
	protected int childNumber = 0;
	
	private CustomNode parent;

	public CustomNode() {
		this(0.,0.);
	}
	
	public CustomNode(Double...coords) {
		this.coords = coords;
		updateFigureCoordinates();
	}

	public void propagateLevel(int level) {
		if (this.level != level) {
			this.level = level;
			for (CustomNode node : children) {
				node.propagateLevel(level + 1);
			}
		}
	}

	/**
	 * Add a child to the current node
	 * @param child
     */
	public void addChild(CustomNode child) {
		children.add(child);
		child.propagateLevel(level + 1);
		child.setChildNumber(children.size());
		child.setParent(this);
		child.setCoords(this.coords);
		child.setModel(this.model);
	}

	/**
	 * Get the number of children
	 * @return
     */
	public int getChildrenCount() {
		return children.size();
	}

	/**
	 * Get the children
	 * @return
     */
	public ArrayList<CustomNode> getChildren() {
		return children;
	}

	/**
	 * Set the children
	 * @param children
     */
	public void setChildren(ArrayList<CustomNode> children) {
		this.children = children;
	}

	/**
	 * Get the level of the node
	 * @return
     */
	public int getLevel() {
		return level;
	}

	/**
	 * Set the node's level
	 * @param level
     */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * The position of the children  in relation to the father
	 * @return
     */
	public int getChildNumber() {
		return childNumber;
	}

	/**
	 * Set the positon of the children
	 * @param childNumber
     */
	public void setChildNumber(int childNumber) {
		this.childNumber = childNumber;
	}

	public Double[] getCoords() {
		return coords;
	}
	
	public void setCoords(Double...coords) {
		this.coords = coords;
	}

	@Override
	public CustomShape clone() {
		CustomNode node = new CustomNode(coords);
		node.setLabel(getLabel());
		node.copyStyleFrom(this);
		node.setLevel(this.level);
		node.setModel(this.model);
		for (CustomNode child : children) {
			node.addChild((CustomNode) child.clone());
		}
		return node;
	}

	@Override
	public void updateFigureCoordinates() {
		if (figure == null) {
			figure = new Rectangle();
			figure.setFill(null);
		}
		Rectangle rec = (Rectangle) figure;
		rec.setX(getAnchorX());
		rec.setY(getAnchorY());
		rec.setWidth(nodeWidth);
		rec.setHeight(nodeHeight);
		for (CustomNode child : children) {
			child.updateFigureCoordinates();
		}
	}

	/**
	 * Get the figures related to the node
	 * @return
     */
	public ArrayList<Shape> getAllFigures() {
		ArrayList<Shape> returnValue = new ArrayList<Shape>();
		returnValue.add(getFigure());

		Text label = new Text();
		label.setText(getLabel());
		label.setX(getAnchorX() + nodeWidth / 2);
		label.setY(getAnchorY() + nodeHeight / 2);
		label.setLayoutX(-label.getLayoutBounds().getWidth() / 2);
		returnValue.add(label);
		
		for (CustomNode child : children) {
			Line line = new Line();
			line.setStartX(this.getAnchorX() + nodeWidth / 2);
			line.setEndX(child.getAnchorX() + nodeWidth/2);
			line.setStartY(this.getAnchorY() + nodeHeight);
			line.setEndY(child.getAnchorY());
			returnValue.add(line);
			returnValue.addAll(child.getAllFigures());
		}
		return returnValue;
	}

	@Override
	public String generateTikz(String options) {
		return generateTikz();
	}

	public String generateTikz() {
		return "\\" + tikzRepr() + ";"+System.lineSeparator();
	}

	/**
	 * Get the the string representation of the node
	 * @return
     */
	public String tikzRepr() {
		String at = parent == null ? "at ("+getAnchorX()/XSCALE +","+getAnchorY()/YSCALE +") " : "";
		String repr = "node "+at + getOptionString() + "{" + label + "}";
		for (CustomNode child : children) {
			String whitespaces = new String(new char[level]).replace("\0", "  ");
			repr += System.lineSeparator() + whitespaces + "child { " + child.tikzRepr() + " }";

		}
		return repr;

	}

	@Override
	public Double getAnchorX() {
		Double drawx = coords[0] + WIDTH / 2;
		if (parent != null) {
			drawx = parent.getAnchorX();
			
			if (parent.getChildrenCount() != 1) {
				Double offsetX = (childrensWidth / (parent.getChildrenCount() - 1) * (childNumber - 1))
						- childrensWidth / 2;

				drawx += offsetX;
			}

		}
		return drawx;
	}

	/**
	 * Get the bottom point y coordinate of the node
	 * @return
     */
	public double getBottomY() {
		return getAnchorY() + nodeHeight;
	}

	/**
	 * Get the middle point  x coordinate of the node
	 * @return
	 */
	public double getMiddleX() {
		return getAnchorX() + nodeWidth / 2;
	}

	@Override
	public Double getAnchorY() {
		return coords[1] + level * 75.;
	}

	@Override
	public void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return generateTikz();
	}

	/**
	 * Gives a string version of the node
	 * @return
     */
	public String showNode() {
		String returnValue = "";
		returnValue += " " + level + " " + ":" + childNumber + "/" + getSiblingCount() + label + figure.toString()
				+ System.lineSeparator();
		for (CustomNode node : children) {
			returnValue += node.showNode();
		}
		return returnValue;
	}

	/**
	 * Get the number of parent's children
	 * @return
     */
	public int getSiblingCount() {
		return (parent != null) ? parent.getChildrenCount() : 1;
	}

	@Override
	public Double getWidth() {
		return nodeWidth;
	}

	@Override
	public Double getHeight() {
		return nodeHeight;
	}
	
	@Override
	public void setModel(Model model) {
		for (CustomNode node : children) {
			node.setModel(model);
		}
		this.model = model;
	}

	/**
	 * Gets the parent
	 * @return
     */
	public CustomNode getParent() {
		return parent;
	}

	/**
	 * Sets the parent
	 * @param parent
     */
	public void setParent(CustomNode parent) {
		this.parent = parent;
	}


}

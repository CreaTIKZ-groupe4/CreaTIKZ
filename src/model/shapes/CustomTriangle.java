package model.shapes;

import javafx.scene.shape.Polygon;
import util.CustomUtil;

public class CustomTriangle extends CustomShape {

	public CustomTriangle() {
		this(0d, 0d, 0d, 0d, 0d, 0d);
	}

	/**
	 * Create a triangle based on an existing triangle
	 * @param polygon
     */
	public CustomTriangle(Polygon polygon) {
		this(polygon, polygon.getPoints().toArray(new Double[] {}));
	}

	/**
	 * Creates a triangle based on an existing triangle and the coordinates
	 * @param polygon
	 * @param coords
     */
	public CustomTriangle(Polygon polygon, Double[] coords) {
		this.figure = polygon;
		setCoords(coords);
	}

	/**
	 * Creates a triangle based on the coordinates
	 * @param coords
     */
	public CustomTriangle(Double... coords) {
		this(null, coords);
	}

	@Override
	public void updateFigureCoordinates() {
		if (this.figure == null) {
			this.figure = new Polygon();
		}
		Polygon pol = (Polygon) figure;
		pol.getPoints().setAll(coords);
	}

	@Override
	public CustomShape clone() {
		CustomTriangle triangle = new CustomTriangle(this.coords.clone());
		triangle.setId(getId());
		triangle.copyStyleFrom(this);
		triangle.setLabel(getLabel());
		return triangle;
	}

	@Override
	public String generateTikz(String options) {
		String coordString = "";
		boolean isXCoord = true;
		for (double point : coords) {
			coordString += (isXCoord) ? " (" + point/XSCALE + "," : point/YSCALE + ") --";
			isXCoord = !isXCoord;
		}
		return "\\draw" + options + coordString + " cycle" + generateIdString("at start, above left")
				+ System.lineSeparator();
	}

	@Override
	public Double getAnchorX() {
		return coords[4];
	}

	@Override
	public Double getAnchorY() {
		return coords[5];
	}
	
	@Override
	public Double getCenterX() {
		return getCenter(coords[0], coords[2], coords[4]);
	}

	@Override
	public Double getCenterY() {
		return getCenter(coords[1], coords[3], coords[5]);
	}

	private Double getCenter(Double... points) {
		double min = CustomUtil.getMin(points);
		double max = CustomUtil.getMax(points);
		return min + (max - min)/2;
	}

	@Override
	public void setCoordsFromTwoPoints(double startingPointX, double startingPointY, double endingPointX,
			double endingPointY) {
		setCoords((startingPointX + endingPointX) / 2, startingPointY, startingPointX, endingPointY, endingPointX,
				endingPointY);

	}

	@Override
	public Double getWidth() {
		return getDifference(coords[0], coords[2], coords[4]);
	}

	@Override
	public Double getHeight() {
		return getDifference(coords[1], coords[3], coords[5]);
	}

	/**
	 * Gets the difference between the current corrdinates and others
	 * @param points
	 * @return
     */
	private Double getDifference(Double... points) {
		double min = CustomUtil.getMin(points);
		double max = CustomUtil.getMax(points);
		return max - min;
	}

}

package model.services;

import controller.LoginController;
import model.Project;
import util.Config;
import util.CustomUtil;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.net.ConnectException;
import java.util.List;

public class Server {

	private static User activeUser = new User("offline", "offline", "offline", "offline", "offline");

	private static Project currentProject;

	public static final String BASE_URI = Config.getProperty("server_base_uri");
	private static Client client = ClientBuilder.newClient();
	private static WebTarget target = client.target(BASE_URI);

	/**
	 * Registers this user on the server
	 * @param user the user to register
	 * @throws ConnectException If the server is unreachable
	 */
	public static void register(User user) throws ConnectException{
		target.path("User/post").request().post(Entity.json(user.toJSON()), Boolean.class);
	};

	/**
	 * Attempt to connect to server using this user 
	 * @param user to be authenticated
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception If an error occurs during authentication
	 */
	public static void authenticate(User user) throws ConnectException, Exception {
		activeUser = target.path("User/auth").request().post(Entity.json(user.toJSON()), User.class);
		if (activeUser == null) {
			throw new Exception("Erreur d'authentification");
		}
	}
	
	/**
	 * Updates this user's information on the server
	 * @param user to be authenticated
	 * @param newPwd the user's new password
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception If an error occurs when the user is being updated
	 */
	public static void update(User user, String newPwd) throws ConnectException, Exception {
		User result = target.path("User/update/" + newPwd).request().post(Entity.json(user.toJSON()), User.class);
		activeUser = result;
		
	}

	/**
	 * Creates a new project on the server
	 * @param project the project to be created
	 * @return the created project
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception If an error occurs when creating the project on the server
	 */
	public static Project createProject(Project project) throws ConnectException, Exception  {
		currentProject = target.path("Project/Create").request().post(Entity.json(project.toJSON()), Project.class);
		currentProject.writeInfoFile();
		return currentProject;
	}

	/**
	 * Updates a project on the server and sets this project as the current project
	 * @param project The project to be updated
	 * @return Conflict which contains the result of the merge attempt. 
	 * If the merge attempt failed, user action is required to solve the conflict.
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception If an error occurs when updating the project on the server
	 */
	public static Conflict updateProject(Project project) throws ConnectException, Exception  {
		Conflict result = target.path("Project/Update").request().post(Entity.json(project.toJSON()), Conflict.class);
		currentProject = project;
		return result;
	}

	/**
	 * Shares a project with all other users
	 * @param project the project to be shared
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception if an error occurs while sharing the project
	 */
	public static void shareProject(Project project) throws ConnectException, Exception {
		try {
			target.path("Project/Share").request().post(Entity.json(project.toJSON()), Boolean.class);
		} catch (ProcessingException ex) {
			throw new Exception("Connexion au serveur impossible");
		} catch(Exception ex) {
			throw new Exception("Erreur au partage");
		}
	}
	/**
	 * Checks if a project is shared
	 * @param project the project to check
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception if an error occurs while sharing the project
	 * @return Boolean if the project is shared
	 */
	public static boolean isSharedProject(Project proj)  throws ConnectException, Exception {
		try {
			String result = target.path("Project/isshared/"+proj.getPrj_id()).request().get(String.class);
			return result.equals("true");
		} catch (ProcessingException ex) {
			throw new Exception("Connexion au serveur impossible");
		}
	}

	/**
	 * Returns the projects to which this user has access and have been modified since the last request
	 * @param user the User for whom the projects have to be downloaded
	 * @return this user's projects (shared projects included)
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception if an error occurs while getting the projects
	 */
	public static List<Project> getProjects(User user) throws ConnectException, Exception  {
		List<Project> projects = target.path("Project/Get").request().post(Entity.json(user.toJSON()),
				new GenericType<List<Project>>() {});
		return projects;
	}

	/**
	 * Solves the conflict by applying the user's decision
	 * @param conflict the conflict to be solved
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception if an error occurs while applying the decision server-side
	 */
	public static void solveConflict(Conflict conflict) throws ConnectException, Exception {
		target.path("Project/Solve").request().post(Entity.json(conflict.toJSON()));
	}

	/**
	 * Renames the project on the server
	 * @param project the project to be renamed
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception if an error occurs while renaming the project server-side
	 */
	public static void renameProject(Project project) throws ConnectException, Exception {
		target.path("Project/Rename").request().post(Entity.json(project.toJSON()), Boolean.class);
	}

	/**
	 * Deletes the project on the server
	 * @param project the project to be deleted
	 * @throws ConnectException If the server is unreachable
	 * @throws Exception if an error occurs while deleting the project server-side
	 */
	public static void deleteProject(Project project) throws ConnectException, Exception {
		target.path("Project/Delete").request().post(Entity.json(project.toJSON()), Boolean.class);
	}

	/**
	 * Send a request to the server to copy a file or a directory
	 * @param sourceFile
	 * @param targetFile
	 */
	public static void copyFile(String sourceFile, String targetFile) {
		request("Copy", sourceFile, targetFile);
	}

	/**
	 * Send a request to the server to delete a file or a directory
	 * @param sourceFile
     */
	public static void deleteFileRequest(String sourceFile) {
		request("Delete", sourceFile, null);
	}

	/**
	 * Send a request to move a file or a directory
	 * @param sourceFile
	 * @param targetFile
     */
	public static void moveFileRequest(String sourceFile, String targetFile) {
		request("Move", sourceFile, targetFile);
	}

	/**
	 * Send a request to rename a file or a directory
	 * @param sourceFile
	 * @param targetFile
     */
	public static void renameFileRequest(String sourceFile, String targetFile) {
		request("Rename", sourceFile, targetFile);
	}

	/**
	 * Send a request to the server
	 * @param request
	 * @param sourceFile
	 * @param targetFile
     */
	private static void request(String request, String sourceFile, String targetFile) {
		if (activeUser != null) {
			String workingDir = CustomUtil.getWorkingDirPath();
			MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
			formData.add("id", String.valueOf(activeUser.getId()));
			switch (request) {
			case "Copy":
			case "Move":
				formData.add("target", targetFile.replace(workingDir,""));
			case "Delete":
				formData.add("source", sourceFile.replace(workingDir,""));
				break;
			case "Rename":
				formData.add("oldName", sourceFile.replace(workingDir,""));
				formData.add("newName", targetFile.replace(workingDir,""));
				break;
			default:
				break;
			}
			target.path("FileRequest/" + request).request().post(Entity.form(formData));
		}
	}

	/**
	 * Test if a user is connected
	 * @return True if a user is connected, False otherwise
	 */
	public static Boolean isConnected() {
		return activeUser != null;
	}

	/**
	 * Check if the server is reachable
	 * @return True if it is, False otherwise
	 */
	public static Boolean isOnline() {
		Boolean online = false;
		try {
			String result = Server.target.path("User/test").request().get(String.class);
			if (result != null) {
				online = result.equals("ok");
			}
		} catch (Exception ex) {
			System.out.println("Connexion avec le serveur impossible");
		}
		return online;
	}

	/**
	 * @return Project the currently opened project
	 */
	public static Project getCurrentProject() {
		return currentProject;
	}

	/**
	 * @param currentProject the currently open project
	 */
	public static void setCurrentProject(Project currentProject) {
		Server.currentProject = currentProject;
	}

	/**
	 * 
	 * @return the currently connected User
	 */
	public static User getActiveUser() {
		return activeUser;
	}

	/**
	 * 
	 * @param activeUser the currently connected User
	 */
	public static void setActiveUser(User activeUser) {
		Server.activeUser = activeUser;
	}

	/**
	 * Log the user out and open the login dialog
	 * @throws IOException if an error occurs while loading up the login dialog
	 */
	public static void logOut() throws IOException {
		activeUser = null;
		LoginController.setUp();
	}
}

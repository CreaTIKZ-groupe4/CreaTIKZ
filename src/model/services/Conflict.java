package model.services;

import org.json.JSONObject;


public class Conflict {

	private int projectId;
	private String mine;
	private String theirs;
	private String origin;
	private int result;
	private int decision;
	public static final int MINE = 1;
	public static final int THEIRS = 2;
	public static final int ORIGIN = 0;

	
	/**
	 * Default constructor necessary for server communication
	 */
	public Conflict() {
	}


	/**
	 * 
	 * @return concerned project id
	 */
	public int getProjectId() {
		return projectId;
	}

	/**
	 * 
	 * @param projectId project id
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	
	/**
	 * 
	 * @param mine user's modified code
	 */
	public void setMine(String mine) {
		this.mine = mine;
	}

	/**
	 * 
	 * @return current user's modified code
	 */
	public String getMine() {
		return mine;
	}
	
	/**
	 * 
	 * @return other user's modified code
	 */
	public String getTheirs() {
		return theirs;
	}

	/**
	 * 
	 * @param theirs user's modified code
	 */
	public void setTheirs(String theirs) {
		this.theirs = theirs;
	}

	/**
	 * 
	 * @return Source code as it was when the user started editing
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * 
	 * @param origin code as it was when the user started editing
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * Returns the result of a conflict detection
	 * @return Either NO_CONFLICT if no conflict was detected
	 * 			or CONFLICT if a conflict was detected
	 */
	public int getResult() {
		return result;
	}

	/**
	 * Sets the value of conflict detection
	 * @param result NO_CONFLICT if no conflict was detected
	 * 			or CONFLICT if a conflict was detected
	 */
	public void setResult(int result) {
		this.result = result;
	}

	/**
	 * Gets the user's choice on which version to keep
	 * @return Either MINE to keep the active user's changes, 
	 * 			THEIRS to keep the changes made by other users 
	 * 			or ORIGIN to go back to the code before modification
	 */
	public int getDecision() {
		return decision;
	}
	
	/**
	 * Gets the user's choice on which version to keep
	 * @param decision MINE to keep the active user's changes, \n
	 * 			THEIRS to keep the changes made by other users \n
	 * 			or ORIGIN to go back to the code before modification\n
	 */
	public void setDecision(int decision) {
		this.decision = decision;
	}

	/**
	 * 
	 * @return True if a conflict is detected, False otherwise
	 */
	public boolean isDetected() {
		return result >= 0;
	}


	/**
	 * Instantiates a new conflict 
	 * @param mine Active user's modified code
	 * @param theirs Other user's modified code
	 * @param origin State of the code before intervention by the active user
	 * @param result Result of conflict detection
	 * @param projectId Concerned project's id
	 */
	public Conflict(String mine, String theirs, String origin, int result, int projectId) {
		super();
		this.mine = mine;
		this.theirs = theirs;
		this.origin = origin;
		this.result = result;
		this.projectId = projectId;
	}

	/**
	 * 
	 * @return a JSON representation of this object
	 */
	public String toJSON() {
		JSONObject jsonO = new JSONObject(this);
		return jsonO.toString();
	}

	/**
	 * Apply the selected decision and save on server
	 * @param decision Either MINE to keep the active user's changes, \n
	 * 			THEIRS to keep the changes made by other users \n
	 * 			or ORIGIN to go back to the code before modification\n
	 * @throws Exception 
	 */
	public void choose(int decision) throws Exception {
		this.decision = decision;
		Server.solveConflict(this);
		String definitiveVersion = getMine();
		switch (decision) {
		case ORIGIN:
			definitiveVersion = getOrigin();
			break;
		case MINE:
			definitiveVersion = getMine();
			break;
		case THEIRS:
			definitiveVersion = getTheirs();
			break;
		}

		Server.getCurrentProject().setPrj_data(definitiveVersion);
		Server.getCurrentProject().setPrj_origin(definitiveVersion);
		Server.getCurrentProject().saveToDisk();
	}

	@Override
	public String toString() {
		return "Conflict [projectId=" + projectId + ", mine=" + mine + ", theirs=" + theirs + ", origin=" + origin
				+ ", result=" + result + ", decision=" + decision + "]";
	}
}

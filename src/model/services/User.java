package model.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONObject;

import model.Project;

public class User {

	private int usr_id;
	private String usr_username = "";
	private String usr_first_name = "";
	private String usr_last_name = "";
	private String usr_email = "";
	private String usr_password = "";
	private Date usr_last_update = new Date(Long.MIN_VALUE);
	private String usr_last_update_string = "";
	
	public User(String usr_username, String usr_first_name, String usr_last_name, String usr_email,
			String usr_password) {
		super();
		this.usr_id = 0;
		this.usr_username = usr_username;
		this.usr_first_name = usr_first_name;
		this.usr_last_name = usr_last_name;
		this.usr_email = usr_email;
		this.usr_password = usr_password;
	}


	public User() {
	}

	/**
	 * 
	 * @return this user's id
	 */
	public int getId() {
		return usr_id;
	}

	/**
	 * 
	 * @return the user's username
	 */
	public String getUsername() {
		return usr_username;
	}

	/**
	 * 
	 * @return the user's first name
	 */
	public String getFirst_name() {
		return usr_first_name;
	}

	/**
	 * 
	 * @return the user's last name
	 */
	public String getLast_name() {
		return usr_last_name;
	}

	/**
	 * 
	 * @return the user's email address
	 */
	public String getEmail() {
		return usr_email;
	}

	/**
	 * 
	 * @return the user's password
	 */
	public String getPassword() {
		return usr_password;
	}

	/**
	 * 
	 * @param id the user's id
	 */
	public void setId(int id) {
		this.usr_id = id;
	}

	/**
	 * 
	 * @param username the user's username
	 */
	public void setUsername(String username) {
		this.usr_username = username;
	}

	/**
	 * 
	 * @param first_name the user's first name
	 */
	public void setFirst_name(String first_name) {
		this.usr_first_name = first_name;
	}

	/**
	 * 
	 * @param last_name the user's last name
	 */
	public void setLast_name(String last_name) {
		this.usr_last_name = last_name;
	}

	/**
	 * 
	 * @param email the user's email
	 */
	public void setEmail(String email) {
		this.usr_email = email;
	}

	/**
	 * 
	 * @param password the user's password
	 */
	public void setPassword(String password) {
		this.usr_password = password;
	}

	/**
	 * 
	 * @return the last time this user's projects were downloaded from the server
	 */
	public Date getUsr_last_update() {
		return usr_last_update;
	}
	
	/**
	 * 
	 * @param lastUpdate the last time this user's projects were downloaded from the server
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.usr_last_update = lastUpdate;
		this.usr_last_update_string = new SimpleDateFormat("yyyyMMddHHmmss").format(lastUpdate);
	}
	
	/**
	 * 
	 * @return String representation of this user's lastUpdate yyyyMMddHHmmss format
	 */
	public String getUsr_last_update_string() {
		return usr_last_update_string;
	}

	/**
	 * 
	 * @param usr_last_update_string String representation of this user's lastUpdate yyyyMMddHHmmss format expected
	 * @throws ParseException 
	 */
	public void setUsr_last_update_string(String usr_last_update_string) throws ParseException {
		this.usr_last_update_string = usr_last_update_string;
		this.usr_last_update = new SimpleDateFormat("yyyyMMddHHmmss").parse(usr_last_update_string);
	}
	
	@Override
	public String toString() {
		return "User [usr_id=" + usr_id + ", usr_username=" + usr_username + ", usr_first_name=" + usr_first_name
				+ ", usr_last_name=" + usr_last_name + ", usr_email=" + usr_email + ", usr_password=" + usr_password
				+ ", usr_last_update=" + usr_last_update + ", usr_last_update_string=" + usr_last_update_string + "]";
	}

	/**
	 * 
	 * @return JSON String representation of this object
	 */
	public String toJSON() {
		JSONObject jsonObject = new JSONObject(this);
		return jsonObject.toString();
	}

	/**
	 * 
	 * @return Get the user's working directory
	 */
	public String getPersonalDir() {
		return System.getProperty("user.home") + "/CreaTIKZ/" + getUsername();
	}
	
	/**
	 * Download the user's projects form the server. Only projects modified after this user's lastUpdate property will be downloaded again.
	 * @throws Exception if an error occurs while downloading the projects from the server
	 */
	public void downloadProjects() throws Exception {
		ArrayList<Project> projects = (ArrayList<Project>) Server.getProjects(this);
		for (Project proj : projects) {
			try {
				proj.saveToDisk();
			} catch (Exception ex) {
				ex.printStackTrace();
				System.err.println("Erreur lors de la sauvegarde d'un projet");
			}
		}
		this.setLastUpdate(new Date());
	}

}

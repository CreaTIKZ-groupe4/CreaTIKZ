package model;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import model.styles.ColorStyle;
import model.styles.LineStyle;
import model.styles.ShapeStyle;
import model.styles.ThicknessStyle;

import java.util.Observable;

public class DrawOptions extends Observable {
	private ColorStyle fillColorStyle = ColorStyle.white;
	private ColorStyle lineColorStyle = ColorStyle.black;;
	private ShapeStyle shapeStyle = ShapeStyle.Rectangle;
	private ThicknessStyle thickness = ThicknessStyle.THIN;
	private LineStyle lineStyle = LineStyle.normal;
	private Double dashSize = LineStyle.DEFAULT_DASH_SIZE;

	/**
	 * Default Constructor
	 */
	public DrawOptions() {
		super();
	}

	/**
	 * Gives a string version of shape's type
	 * @return
     */
	public String getShapeStyleString() {
		return shapeStyle.name();
	}

	/**
	 * Says whether or not the stroke is dasehd
	 * @return
     */
	public boolean isDashed() {
		return this.lineStyle == LineStyle.dotted;
	}

	/**
	 * Gives a string version of stroke's thickness
	 * @return
	 */
	public String getThicknessString() {
		return thickness.name();
	}

	/**
	 * Gives a double version of stroke's thickness
	 * @return
     */
	public double getThicknessValue() {
		return thickness.getDoubleValue();
	}

	/**
	 * Gets the color used to fill the shape
	 * @return
     */
	public ColorStyle getFillColorStyle() {
		return fillColorStyle;
	}

	/**
	 * Fills the shape with a color style (enum)
	 * @param fillColorStyle
     */
	public void setFillColorStyle(ColorStyle fillColorStyle) {
		System.out.println(fillColorStyle);
		if (this.fillColorStyle != fillColorStyle) {
			this.fillColorStyle = fillColorStyle;
			update();
		}
	}

	/**
	 * Gives the stroke's color
	 * @return
     */
	public ColorStyle getLineColorStyle() {
		return lineColorStyle;
	}

	/**
	 * Defines the colorStyle (enum) of the stroke
	 * @param lineColorStyle
     */
	public void setLineColorStyle(ColorStyle lineColorStyle) {
		if (this.lineColorStyle != lineColorStyle) {
			this.lineColorStyle = lineColorStyle;
			update();
		}
	}

	/**
	 * Gets the style of the shape ex: Rectangle, Ellipse ...
	 * @return
     */
	public ShapeStyle getShapeStyle() {
		return shapeStyle;
	}

	/**
	 * Defines the style of the shape. ex: Rectangle, Ellipse ...
	 * @param drawMode
     */
	public void setShapeStyle(ShapeStyle drawMode) {
		if (this.shapeStyle != drawMode) {
			this.shapeStyle = drawMode;
			update();
		}
	}

	/**
	 * Gets the thickness of the stroke
	 * @return
     */
	public ThicknessStyle getThickness() {
		return thickness;
	}

	/**
	 * Defines the stroke's thickness
	 * @param thickness
     */
	public void setThickness(ThicknessStyle thickness) {
		if (this.thickness != thickness) {
			this.thickness = thickness;
			update();
		}
	}

	/**
	 * Gets a string version of the color
	 * @return
     */
	public String getColorString() {
		return lineColorStyle.name();
	}

	/**
	 * Defines the stroke's color
	 * @param color
     */
	public void setlineColor(ColorStyle color) {
		if (this.lineColorStyle != color) {
			this.lineColorStyle = color;
			update();
		}
	}

	/**
	 * Get the color of the stroke
	 * @return
     */
	public Color getLineColor() {
		return getLineColorStyle().getValue();
	}

	/**
	 * Gets the color use to fill the shape
	 * @return
     */
	public Color getFillColor() {
		return getFillColorStyle().getValue();
	}

	/**
	 * Get the style of the shape
	 * @return
     */
	public LineStyle getLineStyle() {
		return lineStyle;
	}

	/**
	 * Defines the style of the shape
	 * @param lineStyle
     */
	public void setLineStyle(LineStyle lineStyle) {
		if (this.lineStyle != lineStyle) {
			this.lineStyle = lineStyle;
			update();
		}
	}

	/**
	 * Defines whether or not the stroke is dashed
	 * @param dashed
     */
	public void setDashed(boolean dashed) {
		LineStyle newLine = dashed ? LineStyle.dotted : LineStyle.normal;
		setLineStyle(newLine);
	}

	/**
	 * Gets the value of the dashed stroke
	 * @return
     */
	public double getDashSize() {
		return dashSize * thickness.getDoubleValue();
	}

	/**
	 * Sets the size of the dashed stroke
	 * @param dashSize
     */
	public void setDashSize(Double dashSize) {
		if (dashSize != this.dashSize) {
			this.dashSize = dashSize;
			update();
		}
	}

	/**
	 * Update and notify observers
	 */
	private void update() {
		setChanged();
		notifyObservers();
	}

	/**
	 * Gets the shape's style and sets the current options
	 * @param shape
     */
	public void applyStyleToShape(Shape shape) {
		shape.setStroke(getLineColor());
		shape.setStrokeWidth(getThicknessValue());
		shape.setFill(getFillColor());

		if (isDashed()) {
			shape.getStrokeDashArray().setAll(getDashSize());
		} else {
			shape.getStrokeDashArray().clear();
		}

	}

	/**
	 * Apply the shape's style to this DrawOptions instance and update once everything has been applied
	 * @param shape the shape from which to get the style
	 */
	public void setStyleFromShape(Shape shape) {
		
		lineColorStyle = ColorStyle.findColorStyle((Color) shape.getStroke());
		thickness = ThicknessStyle.doubleToThickness(shape.getStrokeWidth());
		fillColorStyle = ColorStyle.findColorStyle((Color)shape.getFill());
		boolean dashed = (shape.getStrokeDashOffset() != 0 || shape.getStrokeDashArray().size() != 0);
		lineStyle = dashed ? LineStyle.dotted : LineStyle.normal;
		update();
	}

}
package test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.shapes.CustomEllipse;
import model.shapes.CustomLine;
import model.shapes.CustomMatrix;
import model.shapes.CustomRectangle;
import model.shapes.CustomTriangle;

public class TestMatrix {

	CustomMatrix matrix;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		matrix = new CustomMatrix();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDefault() {
		assertEquals(0, matrix.getRowSize());
		assertEquals(0, matrix.getColSize(0));
		assertEquals(1., matrix.getColSep(), 0.01);
		assertEquals(1., matrix.getRowSep(), 0.01);
		assertEquals(matrix, matrix.clone());
		assertEquals("[column sep=1.0mm, row sep=1.0mm, every cell/.style={ yscale=-0.04,xscale=0.04}]", matrix.getOptionString());
		assertEquals(0.0, matrix.getWidth(), 0.01);
		assertEquals(0.0, matrix.getHeight(), 0.01);
	}
	
	@Test(expected=NullPointerException.class)
	public void testDefaultAnchorXException() {
		matrix.getAnchorX();
	}
	
	@Test(expected=NullPointerException.class)
	public void testDefaultAnchorYException() {
		matrix.getAnchorY();
	}
	
	@Test(expected=NullPointerException.class)
	public void testDefaultStartXException() {
		matrix.getStartX();
	}
	
	@Test(expected=NullPointerException.class)
	public void testDefaultStartYException() {
		matrix.getStartY();
	}
	
	@Test
	public void testWithComponent() {
		CustomRectangle rect = new CustomRectangle();
		CustomTriangle tri = new CustomTriangle();
		CustomEllipse ell = new CustomEllipse();
		CustomLine lin = new CustomLine();
		matrix.addComponent(0, 0, rect);
		matrix.addComponent(0, 1, tri);
		matrix.addComponent(1, 0, ell);
		matrix.addComponent(1, 1, lin);
		
		String expected = "\\matrix[column sep=1.0mm, row sep=1.0mm, every cell/.style={ yscale=-0.04,xscale=0.04}] {\n";
		expected += rect.toString().replace(System.lineSeparator(), "") + " &" + System.lineSeparator();
		expected += tri.toString().replace(System.lineSeparator(), "") + " \\\\" + System.lineSeparator();
		expected += ell.toString().replace(System.lineSeparator(), "") + " &" + System.lineSeparator();
		expected += lin.toString().replace(System.lineSeparator(), "") + " \\\\" + System.lineSeparator();
		expected += "};\n";

		assertEquals(expected, matrix.toString());
		
		assertEquals(0.0, matrix.getWidth(), 0.01);
		assertEquals(0.0, matrix.getHeight(), 0.01);
		
		assertEquals(0.0, matrix.getStartX(), 0.01);
		assertEquals(0.0, matrix.getStartY(), 0.01);

		assertEquals(0.0, matrix.getAnchorX(), 0.01);
		assertEquals(0.0, matrix.getAnchorY(), 0.01);
		
		assertEquals(0.0, matrix.getCenterX(), 0.01);
		assertEquals(0.0, matrix.getCenterY(), 0.01);
	}

}

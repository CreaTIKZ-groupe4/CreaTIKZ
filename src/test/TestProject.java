package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.ConnectException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javafx.scene.shape.Rectangle;
import model.Project;
import model.services.Server;
import model.shapes.CustomRectangle;
import model.styles.ColorStyle;

public class TestProject {

	Project project;

	@Before
	public void setUp() {
		Rectangle rec = new Rectangle();
		rec.setStroke(ColorStyle.findColorByName("black"));
		rec.setFill(ColorStyle.findColorByName("white"));
		rec.setStrokeWidth(4.0);
		CustomRectangle rectangle = new CustomRectangle(rec, 20.0, 320.0, 80.0, 80.0);
		this.project = new Project("testproject");
		project.setPrj_data(rectangle.toString());
		try {
			project.saveToDisk();
			project = Server.createProject(project);
		} catch (ConnectException e) {
			e.printStackTrace();
			fail();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testCopyAndSaveFile() {
		try {
			Project otherProject = new Project("testproject");
			otherProject.readFromDisk();
			assertEquals(project, otherProject);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}

	}
	
	@After
	public void tearDown() {
		try {
			project.delete();
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}
	}

}

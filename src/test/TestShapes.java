package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.shapes.CustomEllipse;
import model.shapes.CustomLine;
import model.shapes.CustomRectangle;
import model.shapes.CustomShape;
import model.shapes.CustomTriangle;

public class TestShapes {

	private static final Double PRECISION = 0.01;

	@Test
	public void testRectangleCenterCoords() {
		CustomRectangle rec = new CustomRectangle(0.0, 0.0, 10.0, 10.0);
		assertEquals(rec.getAnchorX(), 5.0, PRECISION);
		assertEquals(rec.getAnchorY(), 5.0, PRECISION);
	}

	@Test
	public void testEllipseCenterCoords() {
		CustomEllipse ellipse = new CustomEllipse(0.0, 0.0, 10.0, 10.0);
		assertEquals(ellipse.getAnchorX(), 0.0, PRECISION);
		assertEquals(ellipse.getAnchorY(), 0.0, PRECISION);
	}

	@Test
	public void testLineCenter() {
		CustomLine line = new CustomLine(0.0, 0.0, 10.0, 10.0);
		assertEquals(line.getAnchorX(), 5.0, PRECISION);
		assertEquals(line.getAnchorY(), 5.0, PRECISION);
	}

	@Test
	public void testTriangleCenter() {
		CustomTriangle triangle = new CustomTriangle(0.0, 0.0, 10.0, 0.0, 5.0, 10.0);
		assertEquals(triangle.getAnchorX(), 5.0, PRECISION);
		assertEquals(triangle.getAnchorY(), 10.0, PRECISION);
	}

	@Test
	public void testRectangleClone() {
		CustomShape rec = new CustomRectangle(0.0, 0.0, 10.0, 10.0);
		rec.setId("rectangle");
		assertEquals(rec, rec.clone());
	}

	@Test
	public void testEllipseClone() {
		CustomShape ellipse = new CustomEllipse(0.0, 0.0, 10.0, 10.0);
		ellipse.setId("ellipse");
		assertEquals(ellipse, ellipse.clone());
	}

	@Test
	public void testTriangleClone() {
		CustomShape triangle = new CustomTriangle(0.0, 0.0, 10.0, 10.0, 20.0, 20.0);
		triangle.setId("triangle");
		assertEquals(triangle, triangle.clone());
	}

	@Test
	public void testLineClone() {
		CustomShape line = new CustomLine(0.0, 0.0, 10.0, 10.0);
		line.setId("line");
		line.setStartId("start");
		line.setEndId("end");
		assertEquals(line, line.clone());
	}
	
	@Test
	public void testRectangleDimentions() {
		CustomShape rec = new CustomRectangle(40.0, 50.0, 60.0, 70.0);
		assertEquals(40.0, rec.getStartX(), PRECISION);
		assertEquals(50.0, rec.getStartY(), PRECISION);
		
		assertEquals(50.0, rec.getCenterX(), PRECISION);
		assertEquals(60.0, rec.getCenterY(), PRECISION);

		assertEquals(20.0, rec.getWidth(), PRECISION);
		assertEquals(20.0, rec.getHeight(), PRECISION);
	}
	
	@Test
	public void testEllipseDimentions() {
		CustomShape ell = new CustomEllipse(40.0, 50.0, 30.0, 20.0);
		assertEquals(10.0, ell.getStartX(), PRECISION);
		assertEquals(30.0, ell.getStartY(), PRECISION);
		
		assertEquals(40.0, ell.getCenterX(), PRECISION);
		assertEquals(50.0, ell.getCenterY(), PRECISION);

		assertEquals(60.0, ell.getWidth(), PRECISION);
		assertEquals(40.0, ell.getHeight(), PRECISION);
	}

	@Test
	public void testLineDimentions() {
		CustomShape lin = new CustomLine(40.0, 50.0, 60.0, 70.0);
		assertEquals(40.0, lin.getStartX(), PRECISION);
		assertEquals(50.0, lin.getStartY(), PRECISION);
		
		assertEquals(50.0, lin.getCenterX(), PRECISION);
		assertEquals(60.0, lin.getCenterY(), PRECISION);

		assertEquals(20.0, lin.getWidth(), PRECISION);
		assertEquals(20.0, lin.getHeight(), PRECISION);
	}

	@Test
	public void testTriangleDimentions() {
		CustomShape tri = new CustomTriangle(10.0, 20.0, 30.0, 40.0, 50.0, 20.0);
		assertEquals(10.0, tri.getStartX(), PRECISION);
		assertEquals(20.0, tri.getStartY(), PRECISION);
		
		assertEquals(30.0, tri.getCenterX(), PRECISION);
		assertEquals(30.0, tri.getCenterY(), PRECISION);

		assertEquals(40.0, tri.getWidth(), PRECISION);
		assertEquals(20.0, tri.getHeight(), PRECISION);
	}

}

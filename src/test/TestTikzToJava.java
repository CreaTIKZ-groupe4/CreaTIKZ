package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import model.Model;
import model.shapes.CustomEllipse;
import model.shapes.CustomLine;
import model.shapes.CustomMindMap;
import model.shapes.CustomNode;
import model.shapes.CustomRectangle;
import model.shapes.CustomShape;
import model.shapes.CustomTriangle;
import model.styles.ColorStyle;
import util.StringUtils;
import util.TikzParser;

public class TestTikzToJava {

	private List<CustomShape> shapes;
	private String tikzCode;
	private Model model;

	@Before
	public void setUp() {
		tikzCode = "";
		shapes = new ArrayList<CustomShape>();
		model = new Model(new ArrayList<CustomShape>());
	}

	@After
	public void tearDown() {
	}

	
	@Test 
	public void testMindmap() {
		String mindMapString = " \\path[mindmap,concept color=black,text=white]   node at(9,9) [concept] {Computer Science}  [clockwise from=0] child[concept color=white]{      node[concept] {practical}      [clockwise from=90]      child { node[concept] {algorithms} }    child[concept] { node[concept] {theoretical}  }};";
		try {
			TikzParser.parse(mindMapString, model);
			CustomMindMap mindMap = new CustomMindMap(Color.BLACK,Color.WHITE, 0, "ComputerScience",360.,-360.);
			CustomMindMap practical = new CustomMindMap(Color.WHITE,null,90,"practical");
			mindMap.addChild(practical);
			CustomMindMap algorithms = new CustomMindMap(null,null,null,"algorithms");
			practical.addChild(algorithms);
			CustomMindMap theoretical = new CustomMindMap(null,null,null,"theoretical");
			practical.addChild(theoretical);
			assertEquals(mindMap,model.getComponents().get(0));

		} catch (ParseException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void testTikzToJavaEmpty() {
		try {
			TikzParser.parse(tikzCode, model);
		} catch (ParseException e) {
			fail();
		}
		assertTrue(model.getComponents().isEmpty());
	}

	@Test
	public void testTikzToJavaCircle() {
		CustomShape ellipse = generateCircle();
		assertEquals(ellipse.getId(), "circle");
		addShape(ellipse);
	}

	@Test
	public void testTikzToJavaRectangle() {
		CustomShape rec = generateRectangle();
		assertEquals(rec.getId(), "rectangle");
		addShape(rec);
	}

	@Test
	public void testTikzToJavaPolygon() {
		CustomShape pol = generatePolygon();

		assertEquals(pol.getId(), "polygon");
		addShape(pol);

	}

	@Test
	public void testTikzToJavaEdge() {
		CustomShape edge = generateEdge();
		assertEquals(edge.getId(), "line");
		addShape(edge);
	}

	@Test
	public void testTikzToJavaNode() throws IOException {
		CustomShape node = generateNode();
		addShape(node);
	}

	private void addShape(CustomShape shape) {
		shapes.add(shape);
		try {
			TikzParser.parse(tikzCode, model);
		} catch (ParseException e) {
			fail();
		}
		assertEquals(model.getComponents(), shapes);
	}

	@Test
	public void testTikzToJavaMixed() {
		CustomShape polygon = generatePolygon();
		addShape(polygon);

		CustomShape edge = generateEdge();
		addShape(edge);

		CustomShape circle = generateCircle();
		addShape(circle);

		CustomShape rectangle = generateRectangle();
		addShape(rectangle);

		try {
			TikzParser.parse(tikzCode, model);
		} catch (ParseException e) {
			fail();
		}
		assertSame(model.getComponents().size(), 4);
	}

	/**
	 * G�n�re un polygone(triangle) et ajoute le code tikz
	 * 
	 * @param tikzCode
	 * @return
	 */
	private CustomShape generatePolygon() {
		tikzCode += System.lineSeparator()
				+ "\\draw [draw=green,line width=1pt] (0,0) -- (1,0) -- (0.5,1) -- cycle node(polygon)[at start, above left]{};";
		Polygon triangle = new Polygon();
		triangle.setStroke(Color.GREEN);
		triangle.setStrokeWidth(1);
		triangle.setFill(null);

		Double coords[] = new Double[] { 0.0, -0.0, 40.0, -0.0, 20., -40.0 };
		CustomTriangle custTriangle = new CustomTriangle(triangle, coords);
		custTriangle.setId("polygon");
		return custTriangle;
	}

	/**
	 * G�n�re un rectangle et ajoute le code tikz
	 * 
	 * @param tikzCode
	 * @return
	 */
	private CustomShape generateRectangle() {

		tikzCode += System.lineSeparator()
				+ "\\draw [draw=black,line width=2pt] (-4,5) rectangle (0,0) node(rectangle)[midway]{};";
		Rectangle rectangle = new Rectangle();
		rectangle.setStroke(Color.BLACK);
		rectangle.setStrokeWidth(2);
		rectangle.setFill(null);

		rectangle.setId("rectangle");
		Double coords[] = new Double[] { -160.0, -200.0, 0.0, -0.0 };
		CustomRectangle custRectangle = new CustomRectangle(rectangle, coords);
		custRectangle.setId("rectangle");
		return custRectangle;
	}

	/**
	 * G�n�re un cercle et ajoute le code tikz
	 * 
	 * @param tikzCode
	 * @return
	 */
	private CustomShape generateCircle() {
		tikzCode += System.lineSeparator()
				+ "\\draw [draw=blue,line width=2pt] (-4,5) ellipse (1 and 1) node(circle)[midway]{};";
		Ellipse ellipse = new Ellipse();
		ellipse.setStroke(Color.BLUE);
		ellipse.setStrokeWidth(2);
		ellipse.setFill(null);

		ellipse.setId("circle");
		Double coords[] = new Double[] { -160.0, -200.0, 40.0, -40.0 };
		CustomEllipse custEllipse = new CustomEllipse(ellipse, coords);
		custEllipse.setId("circle");
		return custEllipse;
	}

	/**
	 * G�n�re une ar�te et ajoute le code tikz
	 * 
	 * @param tikzCode
	 * @return
	 */
	private CustomShape generateEdge() {
		tikzCode += System.lineSeparator() + "\\draw[draw=white,line width=3pt] (test) -- (-4,6) node(line)[midway]{};";
		Line line = new Line();
		line.setStroke(Color.WHITE);
		line.setStrokeWidth(3);
		line.setFill(null);

		line.setId("line");
		Double coords[] = new Double[] { -280.0, -320.0, -160.0, -240.0 };
		CustomLine custEdge = new CustomLine(line, coords);
		custEdge.setStartId("test");
		custEdge.setId("line");
		return custEdge;
	}

	private CustomShape generateNode() throws IOException {
		File file = new File("src/resources/nodesTikz.txt");

		tikzCode += StringUtils.readStringFromFile(file);

		CustomNode root = new CustomNode(360.,-360.);
		CustomNode Left = new CustomNode();
		CustomNode Right = new CustomNode();
		CustomNode mid = new CustomNode();
		CustomNode left = new CustomNode();
		CustomNode center = new CustomNode();
		CustomNode right = new CustomNode();

		root.addChild(Left);
		root.addChild(Right);
		Right.addChild(mid);
		mid.addChild(left);
		mid.addChild(center);
		mid.addChild(right);

		root.setLabel("Node1");
		Left.setLabel("Left");
		Right.setLabel("Right");
		mid.setLabel("mid");
		left.setLabel("left");
		center.setLabel("center");
		right.setLabel("right");

		Left.setStrokeColor(ColorStyle.red);
		Left.setFillColor(ColorStyle.orange);
		root.setStrokeColor(ColorStyle.red);
		root.setFillColor(ColorStyle.orange);
		return root;

	}
}

package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import util.CustomUtil;

public class TestLoginTools {

	@Before
	public void setUp() {
	}

	@Test
	public void testMailValidityFail() {
		String mail = "test";
		assertFalse(CustomUtil.isValidEmail(mail));
		mail = "test@test";
		assertFalse(CustomUtil.isValidEmail(mail));
	}

	@Test
	public void testMailValiditySuccess() {
		String mail = "test@test.com";
		assertTrue(CustomUtil.isValidEmail(mail));
	}

}

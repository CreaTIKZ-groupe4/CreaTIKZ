package test;

import static org.junit.Assert.*;

import java.net.ConnectException;

import org.junit.Before;
import org.junit.Test;

import model.services.Server;
import model.services.User;

public class TestServer {

	User user;

	@Before
	public void setUp() throws Exception {
		user = new User("test23", "prenom", "nom", "nom@nom2", "mdp");
	}

	@Test
	public void testRegister() {
		try {
			Server.register(user);
		} catch(ConnectException ex) {
			fail();
		}
	}

}


package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

import org.junit.Test;

import model.DrawOptions;
import model.styles.ColorStyle;
import model.styles.LineStyle;
import model.styles.ShapeStyle;
import model.styles.ThicknessStyle;

public class DrawOptionsConfigPanelValuesTest {

	@Test
	public void testDefaultValues() {
		DrawOptions testPanel = new DrawOptions();
		assertEquals(testPanel.getFillColorStyle(), ColorStyle.white);
		assertEquals(testPanel.getLineColorStyle(), ColorStyle.black);

		assertEquals(testPanel.getShapeStyle(), ShapeStyle.Rectangle);
		assertEquals(testPanel.getShapeStyleString(), "Rectangle");
		assertEquals(testPanel.getThickness(), ThicknessStyle.THIN);
		assertEquals(testPanel.getThicknessString(), "THIN");
		assertEquals(testPanel.getThicknessValue(), 4.0, 0.001);

		assertEquals(testPanel.getLineStyle(), LineStyle.normal);

		double dashSize = LineStyle.DEFAULT_DASH_SIZE * testPanel.getThicknessValue();
		assertEquals(testPanel.getDashSize(), dashSize, 0.001);

		assertEquals(testPanel.getColorString(), "black");

	}

	@Test
	public void testSettingValues() {
		DrawOptions testPanel = new DrawOptions();

		testPanel.setShapeStyle(ShapeStyle.Rectangle);
		assertEquals(testPanel.getShapeStyle(), ShapeStyle.Rectangle);

		for (ShapeStyle style : ShapeStyle.values()) {

			testPanel.setShapeStyle(style);
			assertEquals(testPanel.getShapeStyle(), style);
		}

		testPanel.setlineColor(ColorStyle.red);
		assertEquals(testPanel.getLineColorStyle(), ColorStyle.red);
		assertNotEquals(testPanel.getFillColorStyle(), ColorStyle.red);

		testPanel.setLineColorStyle(ColorStyle.orange);
		assertEquals(testPanel.getLineColorStyle(), ColorStyle.orange);
		assertNotEquals(testPanel.getFillColorStyle(), ColorStyle.orange);

		testPanel.setFillColorStyle(ColorStyle.blue);
		assertNotEquals(testPanel.getLineColorStyle(), ColorStyle.blue);
		assertEquals(testPanel.getFillColorStyle(), ColorStyle.blue);

	}

}

package test;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import model.services.User;

public class TestUserObject {

	private User user;

	@Before
	public void setUp() throws Exception {
		user = new User("test", "prenom", "nom", "nom@nom", "mdp");
		Calendar date = Calendar.getInstance();
		date.set(2016,1,1,10,0,0); //1 fevrier 2016 à 10:00:00
		user.setLastUpdate(date.getTime());
		
	}

	@Test
	public void testToJSONString() {
		String expected = "{\"password\":\"mdp\",\"usr_last_update_string\":\"201602011000\",\"usr_last_update\":\"Mon Feb 01 10:00:00 CET 2016\",\"personalDir\":\""+ System.getProperty("user.home")+"/CreaTIKZ/test\",\"last_name\":\"nom\",\"id\":0,\"first_name\":\"prenom\",\"email\":\"nom@nom\",\"username\":\"test\"}";
		String actual = user.toJSON();
		assertEquals(expected, actual);
	}

}
